<?php
require_once("database.php");

class Job extends DatabaseObject {

	protected static $table_name="job";
	protected static $db_fields = array('id', 'user_id', 'job_title', 'job_description_brief', 'job_description_detailed', 'category', 'location', 'country', 'employment_type', 'requirements', 'qualifications', 'additional_info', 'max_salary', 'min_salary', 'currency', 'posted_date', 'start_date', 'end_date', 'total_application', 'short_listed', 'status', 'image');

	public $id;
	public $user_id;
	public $job_title;
	public $job_description_brief;
	public $job_description_detailed;
	public $category;
	public $location;
	public $country;
	public $employment_type;
	public $requirements;
	public $qualifications;
	public $additional_info;
	public $max_salary;
	public $min_salary;
	public $currency;
	public $posted_date;
	public $start_date;
	public $end_date;
	public $total_application;
	public $short_listed;
	public $status;
	public $image;

	public static function make($id, $uid, $jtitle, $j_des_brief, $j_des_det, $category, $location, $country, $employment_type, $requirements, $qualifications, $additional_info, $max_salary, $min_salary, $currency, $start_date, $end_date, $image) {
			$job = new Job();

		    $job->id = $id;
		    $job->user_id = $uid;
		    $job->job_title = $jtitle;
		    $job->job_description_brief = $j_des_brief;
		    $job->job_description_detailed = $j_des_det;
		    $job->category = $category;
		    $job->location = $location;
		    $job->country = $country;
		    $job->employment_type = $employment_type;
		    $job->requirements = $requirements;
		    $job->qualifications = $qualifications;
		    $job->additional_info = $additional_info;
		    $job->max_salary = $max_salary;
		    $job->min_salary = $min_salary;
		    $job->currency = $currency;
		    $job->posted_date = datetime();
		    $job->start_date = $start_date;
		    $job->end_date = $end_date;
		    $job->image = $image;
		    $job->total_application = (INT) 0;
		    $job->short_listed = (INT) 0;
		    $job->short_listed = (INT) 0;
		    $job->status = "";

			return $job;
	}

	public function find_by_jid($id="") {
		global $database;

		$result_array = static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE job_id = '".$database->escape_value($id)."' LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}

	public function find_available_jobs($id) {
	 	global $database;

	 	$dt =time();
	 	$sql = "SELECT * FROM ".static::$table_name." WHERE user_id=".$id;
		return static::find_by_sql($sql);
	}

	public function find_expired_jobs($id) {
	 	global $database;

	 	$dt =strftime("%Y-%m-%d", time());
	 	$sql = "SELECT * FROM ".static::$table_name." WHERE end_date < ".$dt." AND user_id=".$id;
	 	return static::find_by_sql($sql);
	}

	public function find_active_jobs($id) {
	 	global $database;

	 	$dt =strftime("%Y-%m-%d", time());
	 	$sql = "SELECT * FROM ".static::$table_name." WHERE end_date > ".$dt." AND user_id=".$id;
	 	return static::find_by_sql($sql);
	}

	public function find_by_date_posted ($date) {
		global $database;

		$dt =strftime("%Y-%m-%d", time());
		$sql = "SELECT * FROM ".static::$table_name." WHERE posted_date = ".$date;
		return static::find_by_sql($sql);
	}

	public function find_by_date_expiring ($date) {
		global $database;

		$dt =strftime("%Y-%m-%d", time());
		$sql = "SELECT * FROM ".static::$table_name." WHERE end_date = ".$date;
		return static::find_by_sql($sql);
	}

	public function find_by_field ($field) {
		global $database;

		$sql = "SELECT * FROM ".static::$table_name." WHERE category = ".$field;
		return static::find_by_sql($sql);
	}

	public function find_by_region ($location) {
		global $database;

		$sql = "SELECT * FROM ".static::$table_name." WHERE location = ".$location;
		return static::find_by_sql($sql);
	}

	public function find_by_status ($status) {
		global $database;

		$sql = "SELECT * FROM ".static::$table_name." WHERE status = ".$status;
		return static::find_by_sql($sql);
	}

	public function find_job_by ($status, $date_p, $date_e, $field, $location) {
		global $database;

		$sql = "SELECT * FROM ".static::$table_name;
		($status || $date_p || $date_e || $field || $location) ? $sql .= " WHERE " : false;
		!empty($status) ? $sql .= "status = '".$status."'" : false ;
		!empty($status && $date_p) ? $sql .= " AND ":
		!empty($date_p) ? $sql .= " posted_date = '".$date_p."'" : false ;
		!empty(($status || $date_p) && $date_e) ? $sql .= " AND ":
		!empty($date_e) ? $sql .= "end_date = '".$date_e."'" : false ;
		!empty(($status || $date_p || $date_e) && $field) ? $sql .= " AND ":
		!empty($field) ? $sql .= "category = '".$field."'" : false ;
		!empty(($status || $date_p || $date_e || $field) && $location) ? $sql .= " AND ":
		!empty($location) ? $sql .= "location = '".$location."'" : false;

		$result = $database->query($sql); // This is used to get the count

		$search_results = array(
			'count' => $database->num_rows($result),
			'result' => static::find_by_sql($sql),
			);

		return $search_results;
	}

}



?>
