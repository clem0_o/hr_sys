<?php
require_once("initialize.php");

class Payment extends DatabaseObject {

	protected static $table_name="payment";
	protected static $db_fields = array('id', 'user_id', 'transaction_code', 'amount', 'payee_name', 'date_posted', 'date_paid');

	public $id;
	public $user_id;
	public $transaction_code;
	public $amount;
	public $payee_name;
	public $date_posted;
	public $date_paid;

	public static function make($uid, $transaction_code, $amount, $payee_name, $date_posted, $date_paid) {
			$payment = new Payment();

			$payment->id = 0;
			$payment->user_id = $uid;
			$payment->transaction_code = $transaction_code;
			$payment->amount = $amount;
			$payment->payee_name = $payee_name;
			$payment->date_posted = $date_posted;
			$payment->date_paid = $date_paid;

			return $payment;
	}

	public static function total_payment () {
		$payments = static::find_all();


		$total=0;

		foreach ($payments as $payment) {
			$total += $payment->amount;
		}
		echo $total;
	}

	public static function find_by_trans_id ($trans_id) {
		global $database;
		return static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE transaction_code = '".$database->escape_value($trans_id)."'");
	}


}
?>