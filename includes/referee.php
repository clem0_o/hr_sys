<?php
require_once("database.php");

class Referee extends DatabaseObject {

	protected static $table_name="referee";
	protected static $db_fields = array('id', 'user_id', 'name', 'job_title', 'org_name', 'email', 'phone_number');

	public $id;
	public $user_id;
	public $name;
	public $job_title;
	public $org_name;
	public $email;
	public $phone_number;

	public static function make ($userid, $name, $job_title, $org_name, $email, $phone_number ) {
		$ref = new Referee;

		$ref->id =(INT) 0;
		$ref->user_id = $userid;
		$ref->name = $name;
		$ref->job_title = $job_title;
		$ref->org_name = $org_name;
		$ref->email = $email;
		$ref->phone_number = $phone_number;

  		return $ref;
	}



}



?>