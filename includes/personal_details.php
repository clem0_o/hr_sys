<?php
require_once("initialize.php");

class Personal_details extends DatabaseObject {

	protected static $table_name="personal_details";
	protected static $db_fields = array('id', 'user_id', 'base_cv_id', 'field', 'education_level', 'work_expirience', 'place_of_birth', 'date_of_birth', 'id_number', 'nationality', 'district', 'location', 'sub_location', 'currently_living', 'marital_status', 'spouses_name', 'netx_of_keen', 'keens_contact', 'pay_status');

	public $id;
	public $user_id;
	public $base_cv_id;
	public $field;
	public $education_level;
	public $work_expirience;
	public $place_of_birth;
	public $date_of_birth;
	public $id_number;
	public $nationality;
	public $district;
	public $location;
	public $sub_location;
	public $currently_living;
	public $marital_status;
	public $spouses_name;
	public $netx_of_keen;
	public $keens_contact;
	public $pay_status;

	public static function make($uid, $cv_id, $field, $edu_level, $work_exp, $pod, $dob, $id_no, $nationality, $district, $location, $sub_location, $currently_living, $marital_status, $spouses_name, $netx_of_keen, $knscontact, $acc_status) {
		if (!empty($uid) && !empty($cv_id) && !empty($field) && !empty($edu_level) && !empty($work_exp) && !empty($pod) && !empty($dob) && !empty($id_no) && !empty($nationality) && !empty($district) && !empty($location) && !empty($sub_location) && !empty($currently_living) && !empty($marital_status) && !empty($spouses_name) && !empty($netx_of_keen) && !empty($knscontact) && !empty($acc_status)) {

			$personal_details = new Personal_details;

			$personal_details->id =(INT) 0;
			$personal_details->user_id =(INT) $uid;
			$personal_details->base_cv_id =(INT) $cv_id;
			$personal_details->field = $field;
			$personal_details->education_level = $edu_level;
			$personal_details->work_expirience = $work_exp;
			$personal_details->place_of_birth = $pod;
			$personal_details->date_of_birth = $dob;
			$personal_details->id_number =(INT) $id_no;
			$personal_details->nationality = $nationality;
			$personal_details->district = $district;
			$personal_details->location = $location;
			$personal_details->sub_location = $sub_location;
			$personal_details->currently_living = $currently_living;
			$personal_details->marital_status = $marital_status;
			$personal_details->spouses_name = $spouses_name;
			$personal_details->netx_of_keen = $netx_of_keen;
			$personal_details->keens_contact =(INT) $knscontact;
			$personal_details->pay_status = $acc_status;

			return $personal_details;
		} else {
			return false;
		}
	}

	public static function initial_status() {
		
	}

	public static function activate_account($user_id) {
		global $database;

		$sql  = "UPDATE personal_details ";
		$sql .= "SET pay_status='Active' ";
		$sql .= "WHERE user_id = '{$user_id}' ";
		$database->query($sql);

		return ($database->affected_rows() == 1) ? true : false;
	}

	public function find_personal_details_by ($name, $pay_status, $edu_level, $field, $location) {
		global $database;

		$sql = "SELECT * FROM ".static::$table_name;

		($pay_status || $edu_level || $field || $location) ? $sql .= " WHERE " : false;

		!empty($pay_status) ? $sql .= "pay_status = '".$pay_status."'" : false ;
		!empty($pay_status && $edu_level) ? $sql .= " AND ":
		!empty($edu_level) ? $sql .= " education_level = '".$edu_level."'" : false ;
		!empty(($pay_status || $edu_level) && $field) ? $sql .= " AND ":
		!empty($field) ? $sql .= " field = '".$field."'" : false ;
		!empty(($pay_status || $edu_level || $field) && $location) ? $sql .= " AND ":
		!empty($location) ? $sql .= "currently_living = '".$location."'" : false ;

		$result = $database->query($sql);

		$search_results = array(
			'count' => $database->num_rows($result),
			'result' => static::find_by_sql($sql),
			);

		return $search_results;
	}	
}



?>