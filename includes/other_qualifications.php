<?php
require_once("database.php");

class OtherQualifications extends DatabaseObject {

	protected static $table_name="other_qualifications";
	protected static $db_fields = array('id', 'user_id', 'name', 'description', 'type', 'where_attained');

	public $id;
	public $user_id;
	public $name;
	public $description;
	public $type;
	public $where_attained;

	public static function make ($userid, $name, $where_attained, $type, $description) {
		$other_qualification = new OtherQualifications;

		$other_qualification->id =(INT) 0;
		$other_qualification->user_id = $userid;
		$other_qualification->name = $name;
		$other_qualification->description = $description;
		$other_qualification->type = $type;
		$other_qualification->where_attained = $where_attained;

  		return $other_qualification;
	}



}



?>