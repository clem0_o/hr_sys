<?php
require_once("database.php");

class Application extends DatabaseObject {

	protected static $table_name="application";
	protected static $db_fields = array('id', 'user_id', 'cv_id', 'job_id', 'aplication_date', 'additional_info', 'app_status', 'pay_status', 'status');

	public $id;
	public $user_id;
	public $cv_id;
	public $job_id;
	public $aplication_date;
	public $additional_info;
	public $app_status;
	public $pay_status;
	public $status;


	public function number_of_app($reff) {
		global $database;

		$app_number = $database->query("SELECT * FROM ".static::$table_name." WHERE  job_id = '".$reff."'");

		return mysqli_num_rows($app_number);
	}

	public function find_app_by_job_id ($job_id) {
		global $database;

		return static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE job_id='".$database->escape_value($job_id)."'");
	}

	public function find_application_by ($pay_status, $date_applied, $app_status, $field, $location) {
		global $database;

		$sql = "SELECT * FROM ".static::$table_name;
		($pay_status || $date_applied || $app_status || $field || $location) ? $sql .= " WHERE " : false;
		!empty($pay_status) ? $sql .= " app_status = '".$pay_status."'" : false ;
		!empty($pay_status && $date_applied) ? $sql .= " AND ":
		!empty($date_applied) ? $sql .= " posted_date <= '".$date_applied."'" : false ;
		!empty(($pay_status || $date_applied) && $app_status) ? $sql .= " AND ":
		!empty($app_status) ? $sql .= "pay_status = '".$app_status."'" : false ;
		!empty(($pay_status || $date_applied || $app_status) && $field) ? $sql .= " AND ":
		!empty($field) ? $sql .= "category = '".$field."'" : false ;
		!empty(($pay_status || $date_applied || $app_status || $field) && $location) ? $sql .= " AND ":
		!empty($location) ? $sql .= "location = '".$location."'" : false;

		$result = $result_set = $database->query($sql);

		$search_results = array(
			'count' => $database->num_rows($result),
			'result' => static::find_by_sql($sql),
			);

		return $search_results;
	}
	
	public static function check_app ($uid, $jid) {
		global $database;
		$result_array = static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE user_id = ".$database->escape_value($uid)." AND job_id = ".$jid." LIMIT 1");
		return !empty($result_array) ? 'disabled': false;
	}


	// public function find_by_user_id($id=0) {
	// 	global $database;
	// 	$result_array = static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE user_id = ".$database->escape_value($id)." LIMIT 1");
	// 	return !empty($result_array) ? array_shift($result_array) : false;
	// }

	// public static function find_all1_user_id($user_id=0) {
	// 	global $database;
	// 	return static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE user_id=".$database->escape_value($user_id));
	// }

}
