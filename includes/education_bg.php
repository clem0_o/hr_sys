<?php
require_once("database.php");

class Education_bg extends DatabaseObject {

	protected static $table_name="education_bg";
	protected static $db_fields = array('id', 'user_id', 'cert_id', 'institution', 'course_taken', 'level', 'major', 'start_date', 'end_date');

	public $id;
	public $user_id;
	public $cert_id;
	public $institution;
	public $course_taken;
	public $level;
	public $major;
	public $start_date;
	public $end_date;

	public function last_date() {
		if ($this->end_date==0) {
			$this->end_date = "Present";
		 }
		 return $this->end_date;
	}

	public function summary() {
		return "In ".$this->institution." for ".$this->course_taken." during ".$this->start_date." - ".$this->end_date." period.";
	}

	public static function make ($userid, $cert_id, $institution, $cos_taken, $level, $major, $start_date, $end_date) {
		$edu = new Education_bg;

		$edu->id =(INT) 0;
		$edu->user_id = $userid;
		$edu->cert_id =(INT) $cert_id;
		$edu->institution = $institution;
		$edu->course_taken = $cos_taken;
		$edu->level = $level;
  		$edu->major = $major;
  		$edu->start_date = $start_date;
  		$edu->end_date = $end_date;

  		return $edu;
	}



}



?>