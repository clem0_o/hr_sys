<?php
require_once("database.php");

class Employer extends DatabaseObject {

	protected static $table_name="employer";
	protected static $db_fields = array('id', 'user_id', 'company_name', 'location', 'industry');

	public $id;
	public $user_id;
	public $company_name;
	public $location;
	public $industry;


	public function save() {
		return isset($this->user_id) ? $this->update() : $this->create() ;
	}



	public function update() {
		global $database;
		//Don't forget your SQL syntax and good habits:
		//-UPDATE table SET key='value', key='value' WHERE condition
		//single-quotes around all values
		//escape all values to prevent SQL injection
		$attributes = $this->sanitized_attributes();
		$attribute_pairs = array();
 		foreach ($attributes as $key => $value) {
			$attribute_pairs[] = "{$key}='{$value}'";
		}

		$sql = "UPDATE ".static::$table_name." SET ";
		$sql .= join(", ", $attribute_pairs);
		$sql .= " WHERE user_id=".$database->escape_value($this->user_id);
		$database->query($sql);
		return ($database->affected_rows() == 1) ? true : false;
	}

}

?>