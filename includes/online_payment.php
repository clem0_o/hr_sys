<?php
require_once("initialize.php");

class OnlinePayment extends DatabaseObject {

	protected static $table_name="online_payment";
	protected static $db_fields = array('id','transaction_code', 'amount', 'payee_name','time', 'payment_type', 'transaction_method', 'transaction_merchant_reference', 'transaction_mobile');

	public $id;
	public $transaction_code;
	public $amount;
	public $payee_name;
	public $time;
	public $payment_type;
	public $transaction_method;
	public $transaction_merchant_reference;
	public $transaction_mobile;

	public static function make($transaction_code, $amount, $payee_name, $time, $type, $method, $merchant_reference, $mobile) {
			$payment = new OnlinePayment();

			$payment->id = 0;
			$payment->transaction_code = $transaction_code;
			$payment->amount = $amount;
			$payment->payee_name = $payee_name;
			$payment->time = $time;
			$payment->transaction_type = $type;
			$payment->transaction_method = $method;
			$payment->transaction_merchant_reference = $merchant_reference;
			$payment->transaction_mobile = $mobile;

			return $payment;
	}

	public static function total_payment () {
		$payments = static::find_all();


		$total=0;

		foreach ($payments as $payment) {
			$total += $payment->amount;
		}
		echo $total;
	}

	public static function find_by_trans_id ($trans_id) {
		global $database;
		return static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE transaction_code = '".$database->escape_value($trans_id)."'");
	}


}
?>