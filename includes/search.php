<?php
require_once("initialize.php"); //this brings the other classes cloder for proccessing

//A class to help work with Searches
//This will be used by both the admin and the employers to sort through 
//the list of jobs and applications
 
class Search {


	function __construct() {

	}

	public function search_jobs ($submited) {
		$job = new Job;
		
		
		$status = $submited["status"];
		$date_p = $submited["date_posted"];
		$date_e = $submited["date_expiring"];
		$field = $submited["field"];
		$location = $submited["region"];

		return $job->find_job_by ($status, $date_p, $date_e, $field, $location);
	}

	public function search_aplication ($submited) {
		$application = new Application;

		$pay_status = $submited["pay_status"];
		$date_applied = $submited["date_applied"];
		$app_status = $submited["app_status"];
		$field = $submited["field"];
		$location = $submited["region"];

		return $application->find_application_by ($pay_status, $date_applied, $app_status, $field, $location);

	}

	public function search_cvs ($submited) {
		$personal_details = new Personal_details;

		$name = $submited["name"];
		$pay_status = $submited["pay_status"];
		$edu_level = $submited["edu_level"];
		$field = $submited["field"];
		$location = $submited["region"];


		return $personal_details->find_personal_details_by ($name, $pay_status, $edu_level, $field, $location);

	}

}

?>
