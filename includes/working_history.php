<?php
require_once("database.php");

class WorkingHistory extends DatabaseObject {

	protected static $table_name="work_history";
	protected static $db_fields = array('id', 'user_id', 'organization_name', 'position_held', 'job_description', 'start_date', 'end_date', 'reason_for_leaving');

	public $id;
	public $user_id;
	public $organization_name;
	public $position_held;
	public $job_description;
	public $start_date;
	public $end_date;
	public $email;
	public $reason_for_leaving;

	public function last_date() {
		if ($this->end_date==0) {
			return $this->stop_date = "Present";
		 }
	}

	public static function make ($id, $user_id, $orgname, $posheld, $jobdes, $startdate, $enddate, $reasfleaving) {
		
		$work = new WorkingHistory;

		$work->id = $id;
		$work->user_id = $user_id;
		$work->organization_name = $orgname;
		$work->position_held = $posheld;
		$work->job_description = $jobdes;
  		$work->start_date = $startdate;
  		$work->end_date = $enddate;
  		$work->reason_for_leaving = $reasfleaving;

  		return $work;
	}



}



?>