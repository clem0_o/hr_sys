<?php
require_once("database.php");

class User_payment extends DatabaseObject {

	protected static $table_name="user_payment";
	protected static $db_fields = array('id', 'user_id', 'transaction_code', 'name', 'date');

	public $id;
	public $user_id;
	public $transaction_code;
	public $name;
	public $date;

	public static function make($uid, $transaction_code, $name, $date) {
			$user_payment = new User_payment();

			$user_payment->id =(INT) 0;
			$user_payment->user_id = $uid;
			$user_payment->transaction_code = $transaction_code;
			$user_payment->name = $name;
			$user_payment->date = $date;

			return $user_payment;
	}

	public static function check_payment($transaction_code) {
		global $database;

		$sql  = "SELECT * FROM user_payment ";
		$sql .= "WHERE transaction_code = '{$transaction_code}' ";
		$sql .= "LIMIT 1";
		$result_array = self::find_by_sql($sql);
		return !empty($result_array) ? array_shift($result_array) : false;
	}

	public static function find_by_trans_id ($trans_id) {
		global $database;
		return static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE transaction_code = '".$database->escape_value($trans_id)."'");
	}


}
?>