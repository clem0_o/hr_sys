<?php
require_once("database.php");

class Casual extends DatabaseObject {

	protected static $table_name="casual";
	protected static $db_fields = array('id', 'user_id', 'date_of_birth', 'place_of_birth', 'marital_status', 'specialisation', 'area');

	public $id;
	public $user_id;
	public $date_of_birth;
	public $place_of_birth;
	public $marital_status;
	public $specialisation;
	public $area;

}
?>