<?php
require_once(LIB_PATH.DS."config.php");
//require_once("../includes/initialize.php");

class Logins extends DatabaseObject {

	protected static $table_name="logins";
	protected static $db_fields = array('id', 'user_id', 'date', 'ip_address', 'browser', 'status', 'referred');

	public $id;
	public $user_id;
	public $date;
	public $ip_address;
	public $browser;
	public $status;
	public $referred;

	public static function clog($status="", $uid=0) {
			$log = new Logins();

			$log->id =(INT) 0;
			$log->user_id = $uid;
			$log->date = strftime("%Y-%m-%d %H:%M:%S", $_SERVER['REQUEST_TIME']);
			$log->ip_address = $_SERVER['SERVER_ADDR'];
			$log->browser = $_SERVER['HTTP_USER_AGENT'];
			$log->referred = $_SERVER['HTTP_REFERER'];
			$log->status = $status;

			$log->create();

			return true;
	}// End of make


	public static function find_all_ord() {
		try {
			return static::find_by_sql('SELECT * FROM '.static::$table_name.' ORDER BY date DESC' );
		} catch (Exception $e) {
			echo "Something went wrong".$e;
		}
		
	}


} //End of class
// echo "Sever_Name: ". $_SERVER['SERVER_NAME']."<br/>";
// echo "Sever_ADDR: ". $_SERVER['SERVER_ADDR']."<br/>";
// echo "Sever_PORT: ". $_SERVER['SERVER_PORT']."<br/>";


// echo "REMOTE_ADDR: ". $_SERVER['REMOTE_ADDR']."<br/>";
// echo "REMOTE_PORT: ". $_SERVER['REMOTE_PORT']."<br/>";
// echo "REQUEST_URI: ". $_SERVER['REQUEST_URI']."<br/>";
// echo "QUERY_STRING: ". $_SERVER['QUERY_STRING']."<br/>";
// echo "REQUEST_METHOD: ". $_SERVER['REQUEST_METHOD']."<br/>";
// echo "REQUEST_TIME: ". $_SERVER['REQUEST_TIME']."<br/>";
// echo "HTTP_REFERER: ". $_SERVER['HTTP_REFERER']."<br/>";
// echo "HTTP_USER_AGENT: ". $_SERVER['HTTP_USER_AGENT']."<br/>";
 ?>