<?php
require_once("database.php");

class Employee extends DatabaseObject {

	protected static $table_name="employee";
	protected static $db_fields = array('id', 'user_id', 'date_of_birth', 'place_of_birth', 'marital_status', 'year_completed_highschool', 'base_cv');

	public $id;
	public $user_id;
	public $date_of_birth;
	public $place_of_birth;
	public $marital_status;
	public $year_completed_highschool;
	public $base_cv;

	public static function make($uid, $dob, $pob, $mstatus, $ycomphighschool, $base_cv="") {
		if (!empty($uid) && !empty($dob) && !empty($pob) && !empty($mstatus) && !empty($ycomphighschool) /*&& !empty($base_cv)*/) {
			$employee = new Employee();

			$employee->user_id = $uid;
			$employee->date_of_birth = $dob;
			$employee->place_of_birth = $pob;
			$employee->marital_status = $mstatus;
			$employee->year_completed_highschool = $ycomphighschool;
			$employee->base_cv = $base_cv;

			return $employee;
		} else {
			return false;
		}
	}

}
?>