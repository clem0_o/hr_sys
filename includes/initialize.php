<?php

// Define the core paths
// Define them as absolute paths to make sure that require_once works as expected

// DIRECTORY_SEPARATOR is a PHP pre-defined constant
// (\ for Windows, / for Unix)
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

defined('SITE_ROOT') ? null :
//home1/insigie1/public_html/jobs/includes
	//define('SITE_ROOT', DS.'home1'.DS.'insigie1'.DS.'public_html'.DS.'jobs');
	define('SITE_ROOT', DS.'var'.DS.'www'.DS.'html'.DS.'hr_sys');

defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'includes');

// load config file first
require_once(LIB_PATH.DS.'config.php');

// load basic functions next so that everything after can use them
require_once(LIB_PATH.DS.'functions.php');

// load core objects
require_once(LIB_PATH.DS.'session.php');
require_once(LIB_PATH.DS.'pagination.php');
require_once(LIB_PATH.DS.'search.php');
require_once(LIB_PATH.DS.'databases_objects.php');
require_once(LIB_PATH.DS.'database.php');
require_once(LIB_PATH.DS.'input.php');

// load database-related classes
require_once(LIB_PATH.DS.'user.php');
require_once(LIB_PATH.DS.'working_history.php');
require_once(LIB_PATH.DS.'education_bg.php');
require_once(LIB_PATH.DS.'other_qualifications.php');
require_once(LIB_PATH.DS.'employer.php');
require_once(LIB_PATH.DS.'employee.php');
require_once(LIB_PATH.DS.'job.php');
require_once(LIB_PATH.DS.'formvalidator.php');
require_once(LIB_PATH.DS.'application.php');
require_once(LIB_PATH.DS.'cv.php');
require_once(LIB_PATH.DS.'certs.php');
require_once(LIB_PATH.DS.'photograph.php');
require_once(LIB_PATH.DS.'payment.php');
require_once(LIB_PATH.DS.'personal_details.php');
require_once(LIB_PATH.DS.'user_payment.php');
require_once(LIB_PATH.DS.'online_payment.php');
require_once(LIB_PATH.DS.'logins.php');
require_once(LIB_PATH.DS.'referee.php');
require_once(LIB_PATH.DS.'token.php');
?>
