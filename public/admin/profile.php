<?php
require_once("../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
// This brings in the user details
$user = User::find_by_id($session->user_id);

//
$empl = Employer::find_by_user_id($user->id);

$work_history = WorkingHistory::find_all_user_id($user->id);
$jobs_history = new WorkingHistory;

$education_history = Education_bg::find_all_user_id($user->id);
$education_history_obj = new Education_bg;

$other_qualifications = OtherQualifications::find_all_user_id($user->id);
?>
<?php include_layout_template('adminHeader.php'); ?>

    
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <!--<form role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
        </form>-->
        <ul class="nav menu">
            <li class="active"><a href="profile.php"> Profile </a></li>
            <li><a href="manage_user.php"> Manage Users</a></li>
            <li><a href="visitor.php"> Visitors</a></li>
            <li><a href="payments.php"> Payments</a></li>
        </ul>   
    </div><!--/.sidebar-->
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"class="glyphicon glyphicon-home"></a></li>
                <li class="active">Profile (<?php echo $user->full_name(); ?>)</li>
            </ol>
        </div><!--/.row -Bread crumbs-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Profile</h1>
            </div>
        </div><!--/.row -Page header-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Personal Details
                    </div>
                    <div class="panel-body">
                        <p><label>Name: <?php echo $user->full_name(); ?></label></p>
                        <p><label>Phone No: <?php echo $user->phone_number; ?> </label></p>
                        <p><label>Email: <?php echo $user->email; ?></label></p>
                        <p><label>Residence: <?php echo $user->region; ?></label></p>
                        <p><label>Account Status: <?php echo $user->payment; ?></label></p>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
            
        <div class="row">
            
        </div><!--/.row-->
                                
        <div class="row">
            
        </div><!--/.row-->
    </div>  <!--/.main-->
    

<?php include_layout_template('adminFooter.php'); ?>