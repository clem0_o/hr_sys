<?php
require_once("../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$user = User::find_by_id($_GET['user']);
?>
<?php
$message = "";
$error = false;


//form has been submited
if (isset($_POST['submit'])) {
	if (Token::check($_POST['token'])) {
		//read the input data
		$id = trim($_GET['user']);
		$uname = trim($_POST['username']);
		$pword = trim($_POST['password']);
		$utype = trim($_POST['type']);
		$fname = trim($_POST['first_name']);
		$lname = trim($_POST['last_name']);
		$pnumber = trim($_POST['phone_number']);
		$email = trim($_POST['email']);
		$uregion = trim($_POST['region']);
		
		if (empty($uname)) {
			$fname_error = 'user name is empty, Please fill';
			$error = true;
		}
		
		if (empty($fname)) {
			$fname_error = 'First name is empty, Please fill';
			$error = true;
		}
	
		if (empty($lname)) {
			$lname_error = 'Last name is empty, Please fill';
			$error = true;
		}
	
		if (empty($uname)) {
			$uname_error = 'User name is empty, Please fill';
			$error = true;
		}
	
		if (empty($pnumber)) {
			$pnumber_error = 'Phone number is empty, Please fill';
			$error = true;
		} else if (!(strlen((string)$pnumber)==9)) {
			$pnumber_error2 = 'The number provided is not valid, please correct.';
			$error = true;
		}
	
		if (empty($email)) {
			$email_error = 'Email is empty, Please fill';
			$error = true;
		} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$email_error = 'Email is not valid';
			$error = true;
		}
	
		if (empty($uregion)) {
			$uregion_error = 'Region is empty, Please fill';
			$error = true;
		}
	
		if (empty($utype)) {
			$utype_error = 'Type is empty, Please fill';
			$error = true;
		}
	
		if (empty($utype)) {
			$utype_error = 'Type is empty, Please fill';
			$error = true;
		}
		
	
		if ($error == false) {
			//call the user make function
			
			//Update details in the user object
			$user->username = $uname;
			//empty($pword) ? $user->password : $user->password = password_hash($pword, PASSWORD_DEFAULT);
			if (empty($pword)) {
				$user->password;
			} else {
				$user->password = password_hash($pword, PASSWORD_DEFAULT);
			}
			$user->usertype = $utype;
			$user->first_name = $fname;
			$user->last_name = $lname;
			$user->phone_number = $pnumber;
			$user->email = $email;
			$user->region = $uregion;
			

			//Enter the data in the database
			if($user->update()){
				$session->message = "Details successfully updated!!";
				redirect_to('manage_user.php');
			}
		}
	}
}
?>

<?php include_layout_template('adminHeader.php'); ?>

    
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <!--<form role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
        </form>-->
        <ul class="nav menu">
            <li class="active"><a href="profile.php"> Profile </a></li>
            <li><a href="manage_user.php"> Manage Users</a></li>
            <li><a href="visitor.php"> Visitors</a></li>
            <li><a href="payments.php"> Payments</a></li>
        </ul>   
    </div><!--/.sidebar-->
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"class="glyphicon glyphicon-home"></a></li>
                <li class="active">Edit user details</li>
            </ol>
        </div><!--/.row -Bread crumbs-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit user details</h1>
            </div>
        </div><!--/.row -Page header-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Edit user details
                    </div>
                    <div class="panel-body">
                        <form action="edit_user.php?user=<?php echo $user->id; ?>"  method="post">
							<fieldset>
	                    		<div class="row">
	                    			<div class="col-sm-4">
	                    				<p class="lead">
                    						<div class="form-group">
	                        					<label>Username: </label>
	                        					<input class="form-control" name="username" value="<?php echo $user->username; ?>">
	                        					<p class="help-block">Edit the username.</p>
												<span class="error"><?php echo isset($uname_error) ? $uname_error : false; ?></span>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the username -->
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Password: </label>
	                        						<input class="form-control" name="password" value="">
	                        						<p class="help-block">Edit the password.</p>
													<span class="error"><?php echo isset($pword_error) ? $pword_error : false; ?></span>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the password -->
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Type: </label>
	                        						<select name="type" class="form-control" id="type">
	                        							<option>admin</option>
        												<option>Employer</option>
												        <option>Employee</option>
												        <option>Casual</option>
												      </select>
	                        						<p class="help-block">Edit the type.</p>
													<span class="error"><?php echo isset($utype_error) ? $utype_error : false; ?></span>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the password -->
	                    			</div>
	                    			<!-- end of row -->

	                    			<div class="row">
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>First Name: </label>
	                        						<input class="form-control" name="first_name" value="<?php echo $user->first_name; ?>">
	                        						<p class="help-block">Edit the first name.</p>
													<span class="error"><?php echo isset($fname_error) ? $fname_error : false; ?></span>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the username -->
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Last Name: </label>
	                        						<input class="form-control" name="last_name" value="<?php echo $user->last_name; ?>">
	                        						<p class="help-block">Edit the last name.</p>
													<span class="error"><?php echo isset($lname_error) ? $lname_error : false; ?></span>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the password -->
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Phone number: </label>
	                        						<input class="form-control" type="text" name="phone_number" value="<?php echo $user->phone_number; ?>"> 
	                        						<p class="help-block">Edit the phone number.</p>
													<span class="error"><?php echo isset($pnumber_error) ? $pnumber_error : false; ?></span>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the password -->
	                    			</div>
	                    			<!-- end of row -->

	                    			<div class="row">
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Emai: </label>
	                        						<input class="form-control" name="email" value="<?php echo $user->email; ?>">
	                        						<p class="help-block">Edit the email.</p>
													<span class="error"><?php echo isset($email_error) ? $email_error : false; ?></span>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the username -->
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Residence: </label>
	                        						<input class="form-control" name="region" value="<?php echo $user->region; ?>">
	                        						<p class="help-block">Edit the region.</p>
													<span class="error"><?php echo isset($uregion_error) ? $uregion_error : false; ?></span>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the password -->
	                    				<!--<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Type: </label>
	                        						<input class="form-control" value="">
	                        						<p class="help-block">Edit the type.</p>
                      							</div> 
                    						</p>
	                    				</div>-->
	                    				<!-- end of the password -->
	                    			</div>
	                    			<!-- end of row -->
									<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
	                    			<button type="submit" name="submit" class="btn btn-primary">Edit User</button>
								</fieldset>
	                    		</form>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
            
        <div class="row">
            
        </div><!--/.row-->
                                
        <div class="row">
            
        </div><!--/.row-->
    </div>  <!--/.main-->
    

<?php include_layout_template('adminFooter.php'); ?>
