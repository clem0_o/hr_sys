<?php
require_once("../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php

$users = User::find_all();


?>
<?php include_layout_template('adminHeader.php'); ?>

    
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <!--<form role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
        </form>-->
        <ul class="nav menu">
            <li class="active"><a href="profile.php"> Profile </a></li>
            <li><a href="manage_user.php"> Manage Users</a></li>
            <li><a href="visitor.php"> Visitors</a></li>
            <li><a href="payments.php"> Payments</a></li>
        </ul>   
    </div><!--/.sidebar-->
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"class="glyphicon glyphicon-home"></a></li>
                <li class="active"> Manage users</li>
            </ol>
        </div><!--/.row -Bread crumbs-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> Manage users </h1>
            </div>
        </div><!--/.row -Page header-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        List of Users
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>User Name</th>
                                        <th>Full Name</th>
                                        <th>Type</th>
                                        <th>Edit0][</th>
                                    </tr>
                                </thead>
                                <tbody><?php $counter=1; foreach ($users as $user) : ?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo $user->username; ?></td>
                                        <td><?php echo $user->first_name." ".$user->last_name; ?></td>
                                        <td><?php echo $user->usertype; ?></td>
                                        <td><?php echo "<a href=\"edit_user.php?user=".$user->id."\" class=\"btn btn-link\">Edit</a>" ?></td>
                                    </tr><?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
            
        <div class="row">
            
        </div><!--/.row-->
                                
        <div class="row">
            
        </div><!--/.row-->
    </div>  <!--/.main-->
    

<?php include_layout_template('adminFooter.php'); ?>