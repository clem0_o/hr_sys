<?php
require_once("../../includes/initialize.php");
//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
}
?>
<?php
$message = "";
//form has been submited
if (isset($_POST['submit'])) {

  $name1 = trim($_POST['first_name']);
  $name2 = trim($_POST['last_name']);

  $uname = trim($_POST['username']);
  $pword = trim($_POST['password']);

  $pnumber =(INT) trim($_POST['phone_number']);
  $email = trim($_POST['email']);
  $region = trim($_POST['region']);

  $utype = trim($_POST['type']);

  //Validation
  $error = false;

  empty($name1) ? $name1_error='Please fill in the first name field.' : $error = true ;
  empty($name2) ? $name2_error='Please fill in the last name field.' : $error = true ;
  empty($uname) ? $uname_error='Please fill in the username field.' : $error = true;
  empty($pword) ? $pword_error='Please fill in the Password field.' : $error = true;
  empty($pnumber) ? $pnumber_error='Please fill in the phone number field.' : $error = true;
  empty($email) ? $email_error='Please fill in the email field.' : $error = true;
  empty($region) ? $region_error='Please fill in the region field.' : $error = true;
  empty($utype) ? $utype_error='Please fill in the Looking for field.' : $error = true;


  if ($error === true) {

    $new_user = User::make($name1, $name2,$uname, $pword, $pnumber, $email, $region, $utype);

        //confirm there are no previous entries
    $user = new User();

    if($user->username_exists($uname)) {
      $message = "try another username, this is already taken!";
    } else if ($user->email_exists($email)) {
      $message = "try another email id, this is already taken !";
    } else {
      if ($new_user && $new_user->create()) {
        $session->message = "User successfully created.";  
        redirect_to('manage_user.php');     
      }
    }
  }
} else {
  $name1 = "";
  $name2 = "";
  $uname = "";
  $pword = "";
  $pnumber = "";
  $email = "";
  $region = "";
  $utype = "";
}
?>

<?php include_layout_template('header_forms.php'); ?>

<p>
  Create A new user:
</p>
</div>
</div>
</div>
<div class="row">
  <div class="col-sm-6 col-sm-offset-3 form-box">
    <div class="form-top">
      <div class="form-top-left">
        <h3>Create A new user:</h3>
        <div class="text-danger bg-danger">
          <span class="error_message"><?php echo output_message($message); ?></span>
        </div>
        <p>Please fill the fields below</p>
      </div>
      <div class="form-top-right">
        <span><img src="../img/ico/logo1.png" /></span>
      </div>
    </div>
    <div class="form-bottom">
<form action=""  method="post">
  <div class="row">
    <label for="" class="control-label">Name: </label>
    <p>
    <div class="col-xs-6">
    <label for="first_name" class="sr-only"> First Name: </label>
      <input type="text" name="first_name" class="form-control" id="first_name" placeholder="first" value="<?php echo $name1; ?>" />
      <span class="error"><?php echo isset($name1_error) ? $name1_error : false; ?></span>
    </div>
    <div class="col-xs-6">
    <label for="last_name" class="sr-only">Last Name: </label>
      <input type="text" name="last_name" class="form-control" id="last_name" placeholder="last" value="<?php echo $name2; ?>"  />
      <span class="error"><?php echo isset($name2_error) ? $name2_error : false; ?></span>
    </div>
    </p>
  </div>

  <div class="row">
    <label for="" class="control-label">Login: </label>
    <p>
    <div class="col-xs-6">
    <label for="username" class="sr-only">User Name: </label>
      <input type="text" name="username" class="form-control" id="username" placeholder="username..." value="<?php echo $uname; ?>" />
      <span class="error"><?php echo isset($uname_error) ? $uname_error : false; ?></span>
    </div>
    <div class="col-xs-6">
    <label for="password" class="sr-only">Password: </label>
      <input type="password" name="password" class="form-control" id="password" placeholder="Password..." />
      <span class="error"><?php echo isset($pword_error) ? $pword_error : false; ?></span>
    </div>
    </p>
  </div>

  <div class="row">
    <label for="" class="control-label">Contact: </label>
    <p>
    <div class="col-xs-4">
    <label for="phone_number" class="sr-only">Phone number: </label>
      <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="phone_number..." value="<?php echo $pnumber; ?>"  />
      <span class="error"><?php echo isset($pnumber_error) ? $pnumber_error : false; ?></span>
    </div>
    <div class="col-xs-4">
    <label for="email" class="sr-only">Email: </label>
      <input type="text" name="email" class="form-control" id="email" placeholder="email..." value="<?php echo $email; ?>"  />
      <span class="error"><?php echo isset($email_error) ? $email_error : false; ?></span>
    </div>
    <div class="col-xs-4">
    <label for="region" class="sr-only">Name: </label>
      <input type="text" name="region" class="form-control" id="region" placeholder="region..." value="<?php echo $region; ?>"  />
      <span class="error"><?php echo isset($region_error) ? $region_error : false; ?></span>
    </div>
    </p>
  </div>

  <div class="row">
    <label for="" class="control-label">Looking for? </label>
    <p>
    <div class="col-sm-4">
    <label for="type" class="sr-only">Name: </label>
      <select name="type" class="form-control centred" id="type">
        <option value="Admin">Admin</option>
        <option value="Employee">Job</option>
        <option value="Employer">Employee</option>
      </select>
      <span class="error"><?php echo isset($utype_error) ? $utype_error : false; ?></span>
    </div>
    </p><br/>
  </div>
  <button type="submit" name="submit" class="btn btn-primary">Create User!</button>
</form>

    </div>
  </div>
 </div>

<?php include_layout_template('footer_forms.php'); ?>                      