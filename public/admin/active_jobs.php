<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$jobs = new Job;
$listed_jobs = Job::find_all();


?>
<?php include_layout_template('header_admin.php'); ?>

	<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            All Jobs
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Tables
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <!-- List of jobs Available -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Jobs Available</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Ref Number</th>
                                        <th>Job title</th>
                                        <th>Remaining time</th>
                                        <th>More Details</th>
                                    </tr>
                                </thead>
                                <tbody><?php $counter=1; foreach ($listed_jobs as $jobs_listed) : ?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo $jobs_listed->reff_number; ?></td>
                                        <td><?php echo $jobs_listed->job_title; ?></td>
                                        <td><?php echo $jobs_listed->reff_number; ?></td>
                                        <td><?php echo "<a href=\"apply_job.php?ref_number=".$jobs_listed->reff_number."\" class=\"btn btn-link\">apply</a>" ?></td>
                                    </tr><?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include_layout_template('footer_employee.php'); ?>