<?php
require_once("../../includes/initialize.php");
//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
  // if (Cv::find_by_user_id($session->user_id)) {
  //   redirect_to('employees/profile.php');
  // }
}
?>

<?php
$message = '';
if (isset ($_POST['submit'])) {
  if (Token::check($_POST['token'])) {
    $uid = $session->user_id;
    $edu_level = trim($_POST['edu_level']);
    $field = trim($_POST['field']);
    $work_exp = trim($_POST['work_exp']);
    $pod = trim($_POST['pod']);
    $dob = mysql_date (trim($_POST['rdob']));
    $id_no = trim($_POST['nid']);
    $nationality = trim($_POST['nationality']);
    $district = trim($_POST['hdistrict']);
    $location = trim($_POST['location']);
    $sub_location = trim($_POST['sub_loc']);
    $village = trim($_POST['village']);
    $marital_status = trim($_POST['mstatus']);
    $spouses_name = trim($_POST['spname']);
    $netx_of_keen = trim($_POST['nok']);
    $knscontact = trim($_POST['keencontact']);
    $acc_status = "Waiting";

    $error = false;
    $allowed_types = array(
      'doc', 'Doc', 'docx', 'Docx', 'pdf', 'PDF', 'odt', 'rtf', 'txt'
    );
    $filename = $_FILES['cv']['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    if (empty ($_FILES['cv'])) {
      $uploaded_error = 'There is no file attached';
      $error = true;
    } elseif ($_FILES['cv']['size'] > 6000000) {
      $uploaded_error = 'The attached is too big, please make sure its less than 6Mb.';
      $error = true;
    } elseif (!in_array($ext, $allowed_types)) {
      $uploaded_error = 'The attached file is not of permited type, please make sure its a .Doc, .Docx, .pdf .rtf or .odt.';
      $error = true;
    }


    if (empty($edu_level)) {
      $edu_level_error = 'Please chooose an education level.';
      $error = true;
    }
  
    if (empty($field)) {
      $field_error = 'Please chooose your Professional field.';
      $error = true;
    }
  
    if (empty($work_exp)) {
      $work_exp_error = 'Please enter your Professional expirience.';
      $error = true;
    }
  
    if (empty($pod)) {
      $pod_error = 'Please enter your Place of birth.';
      $error = true;
    }
  
    if (empty($dob)) {
      $dob_error = 'Please enter your date of birth.';
      $error = true;
    }
  
    if (empty($id_no)) {
      $id_no_error = 'Please enter your id number.';
      $error = true;
    }
  
    if (empty($nationality)) {
      $nationality_error = 'Please enter your Nationality.';
      $error = true;
    }
  
    if (empty($district)) {
      $district_error = 'Please enter your District.';
      $error = true;
    }
  
    if (empty($location)) {
      $location_error = 'Please enter your Location.';
      $error = true;
    }
  
    if (empty($sub_location)) {
      $sub_location_error = 'Please enter your Sub location.';
      $error = true;
    }
  
    if (empty($village)) {
      $village_error = 'Please enter Where you carrently live.';
      $error = true;
    }
  
    if (empty($marital_status)) {
      $marital_status_error = 'Please enter your Marital status.';
      $error = true;
    }
  
    if (empty($spouses_name)) {
      $spouses_name_error = 'Please enter your Spouses name.';
      $error = true;
    }
  
    if (empty($netx_of_keen)) {
      $netx_of_keen_error = 'Please enter your netx of keen.';
      $error = true;
    }
  
    if ($error === false) {
      $cv = new Cv;
      $user = User::find_by_id($session->user_id);
  
  
      $cv->id = (INT) 0;
      $cv->user_id = $session->user_id;
      $cv->reff_number = "base_cv";
      $cv->date_posted = datetime();
      $cv->attach_file($_FILES['cv'], $user->cvname());
      $cv_id = $cv->save();
      
      $add_personal_details = Personal_details::make($uid, $cv_id, $field, $edu_level, $work_exp, $pod, $dob, $id_no, $nationality, $district, $location, $sub_location, $village, $marital_status, $spouses_name, $netx_of_keen, $knscontact, $acc_status);

      if ($add_personal_details && $add_personal_details->create()) {
        redirect_to("make_payment.php");     
      } else {
        $message = 'Something went wrong with the uploading of data';
      }//Make entry to the db and redirect if successfull
    }//End of if there is no error proccess
  }//End of the token if
} else {
  $uid = "";
  $cv = "";
  $field = "";
  $edu_level = "";
  $work_exp = "";
  $pod = "";
  $dob = "";
  $id_no = "";
  $nationality = "";
  $district = "";
  $location = "";
  $sub_location = "";
  $village = "";
  $marital_status = "";
  $spouses_name = "";
  $netx_of_keen = "";
  $knscontact = "";
  $acc_status = "";
}
?>

<?php include_layout_template('form_header.php'); ?>

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3>
						<span class="glyphicon glyphicon-plus-sign"> Personal Details </span>
					</h3>
				</div>
				<div class="panel-body">
          <?php
            if (!empty($message)) {$error_out = "<div class=\"alert alert-danger\" role=\"alert\">";
              $error_out .= "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>";
              $error_out .= "<span class=\"sr-only\">Error:</span>";
              $error_out .=  output_message($message); 
            $error_out .= "</div>";
            echo $error_out;
            }?><!--End of Error section -->
					<p>Fill this form to register with us:</p>
					<hr/>
					<form action="" enctype="multipart/form-data" method="post">
						<fieldset>
							<div class="form-group">
								<div class="control-label">
									<label for="" class="control-label"> Education Overview: </label>
								</div>
								<div class="col-xs-6">
                  <label for="edu_level" class="sr-only">Education Level </label>
                  <select name="edu_level" class="form-control" id="edu_level">
                    <option value="">Choose Education level</option>
                    <option value="O level">Form 4</option>
                    <option value="Certificate">Certificate</option>
                    <option value="Diploma">Diploma</option>
                    <option value="Degree">Degree</option>
                    <option value="Degree">Masters</option>
                    <option value="Degree">PHD</option>
                  </select>
                  <p class="help-text small">Choose the highest education you have attained.</p>
                  <span class="error"><?php echo isset($edu_level_error) ? $edu_level_error : false; ?></span>
                </div><!-- End of education level -->
								<div class="col-xs-6">
                  <label for="work_exp" class="sr-only">Experience </label>
                  <?php echo experience_yrs (); ?>
                  <p class="help-text small">How many years of expirience do you have? Choose above.</p>
                  <span class="error"><?php echo isset($work_exp_error) ? $work_exp_error : false; ?></span>
                </div><!-- End of experience -->
							</div><!-- End of education information-->
							  
							<div class="form-group">
								<div class="control-label">
									<label for="" class="control-label"> Birth Information: </label>
								</div>
									<div class="col-xs-6">
                    <label for="pod" class="sr-only">Place Of Birth</label>
                    <input type="text" name="pod" class="form-control" id="first_name" placeholder="Place of birth" value="<?php echo !empty($pod) ? $pod : false; ?>">
                    <p class="help-text small">Enter your place of birthday.</p>
                    <span class="error"><?php echo isset($pod_error) ? $pod_error : false; ?></span>
                  </div>
									<div class="col-xs-6">
                    <label for="dob" class="sr-only">Date Of Birth</label>
                    <p>
                      <label for="dob" class="sr-only">Date Of Birth</label>
                      <input type="date" name="rdob" class="form-control" id="rdob" placeholder="Date of birth" value="">
                      <p class="help-text small">Enter the date that you were born.</p>
                      <span class="error"><?php echo isset($dob_error) ? $dob_error : false; ?></span>
                    </p>
                    <span class="error"><?php echo isset($dob_error) ? $dob_error : false; ?></span>
                  </div>
								</div><!--End of birth information -->
							  
								<div class="form-group">
									<div class="control-label">
										<label for="" class="control-label"> Nationality Infomation: </label>
									</div>
									<div class="col-xs-6">
                    <label for="nid" class="sr-only">National Id Number</label>
                    <input type="text" name="nid" class="form-control" id="nid" placeholder="National Id number" value="<?php echo !empty($id_no) ? $id_no : false; ?>" >
                    <p class="help-text small">Enter your national id number.</p>
                    <span class="error"><?php echo isset($nid_error) ? $nid_error : false; ?></span>
                  </div><!-- /.col-lg-6 -->
									<div class="col-xs-6">
                    <label for="nationality" class="sr-only">Nationality</label>
                    <?php echo nationality (); ?>
                    <p class="help-text small">Choose your nationality.</p>
                    <span class="error"><?php echo isset($nationality_error) ? $nationality_error : false; ?></span>
                  </div>
								</div><!-- End of nationality information-->
							   
								<div class="form-group">
									<label for="" class="control-label"> Physical Address: </label>
									<p>
										<div class="col-xs-6">
                      <label for="hdistrict" class="sr-only">District </label>
                      <input type="text" name="hdistrict" class="form-control" id="hdistrict" placeholder="Home district" value="<?php echo !empty($district) ? $district : false; ?>">
                      <p class="help-text small">Enter your home district.</p>
                      <span class="error"><?php echo isset($district_error) ? $district_error : false; ?></span>
                    </div>
										<div class="col-xs-6">
                      <label for="location" class="sr-only">Location</label>
                      <input type="text" name="location" class="form-control" id="location" placeholder="Location" value="<?php echo !empty($location) ? $location : false; ?>" >
                      <p class="help-text small">Enter your home location.</p>
                      <span class="error"><?php echo isset($location_error) ? $location_error : false; ?></span>
                    </div>
									</p>
                  <br/><br/>
                  <p>
                    <div class="col-xs-6">
                      <label for="sub_loc" class="sr-only">Sub Location</label>
                      <input type="text" name="sub_loc" class="form-control" id="sub_loc" placeholder="Sub-location" value="<?php echo !empty($sub_location) ? $sub_location : false; ?>">
                      <p class="help-text small">Enter yor home sub location.</p>
                      <span class="error"><?php echo isset($sub_location_error) ? $sub_location_error : false; ?></span>
                    </div>
                    <div class="col-xs-6">
                      <label for="village" class="sr-only">Place Currently Living</label>
                      <input type="text" name="village" class="form-control" id="village" placeholder="Place you live now... " value="<?php echo !empty($village) ? $village : false; ?>" >
                      <p class="help-text small">Enter the recidence you live now.</p>
                      <span class="error"><?php echo isset($village_error) ? $village_error : false; ?></span>
                    </div>
                  </p>
								</div><!--End of the physical location-->
                
                <div class="form-group">
									<div class="control-label">
										<label for="" class="control-label"> Marital Status: </label>
									</div>
									<div class="col-xs-6">
                    <label for="mstatus" class="sr-only">Marital Status: </label>
                      <select name="mstatus" id="mstatus" class="form-control">
                        <option value="">Marital Status</option>
                        <option value="Single">Single</option>
                        <option value="Married">Married</option>
                      </select>
                      <p class="help-text small">Choose your marital status.</p>
                      <span class="error"><?php echo isset($marital_status_error) ? $marital_status_error : false; ?></span>
                  </div><!-- /.col-lg-6 -->
									<div class="col-xs-6">
                    <label for="spname" class="sr-only">Spouse Name: </label>
                    <input type="text" name="spname" class="form-control" id="spname" placeholder="Spouse's name..." value="<?php echo !empty($spouses_name) ? $spouses_name : false; ?>">
                    <p class="help-text small">Enter the spouse name is present.</p>
                    <span class="error"><?php echo isset($spouses_name_error) ? $spouses_name_error : false; ?></span>
                  </div><!-- /.col-lg-6 -->
								</div><!-- End of marrital information-->
                
                <div class="form-group">
									<div class="control-label">
										<label for="" class="control-label"> Next of Keen: </label>
									</div>
									<div class="col-xs-6">
                    <label for="" class="sr-only">Next of Keen Name: </label>
                    <input type="text" name="nok" class="form-control" id="nok" placeholder="Next of keen..." value="<?php echo !empty($netx_of_keen) ? $netx_of_keen : false; ?>" >
                    <p class="help-text small">Enter the name of next of keen.</p>
                    <span class="error"><?php echo isset($netx_of_keen_error) ? $netx_of_keen_error : false; ?></span>
                  </div><!-- /.col-lg-6 -->
									<div class="col-xs-6">
                    <label for="keencontact" class="sr-only">Next of Keen Contact number: </label>
                    <input type="text" name="keencontact" class="form-control" id="keencontact" placeholder="Next of keen contact..." value="<?php echo !empty($knscontact) ? $knscontact : false; ?>" >
                    <p class="help-text small">Enter the keens contact.</p>
                    <span class="error"><?php echo isset($knscontact_error) ? $knscontact_error : false; ?></span>
                  </div>
								</div><!-- End of Next of keen information-->
                
                <div class="form-group">
                  <div class="control-label">
                    <label for="" class="control-label"> CV Information: </label>
                  </div>
                  <p>
                    <div class="col-xs-6">
                    <label class="sr-only">Upload your CV: </label>
                      <input type="hidden" name="MAX_FILE_SIZE" value="10000000" >
                      <input type="file" name="cv" id="cv">
                      <span class="file-custom"></span>
                      <p class="help-text small">Upload your CV.</p>
                      <span class="error"><?php echo isset($uploaded_error) ? $uploaded_error : false; ?></span>
                    </div>
                    <div class="col-xs-6">
                      <label class="sr-only">field interested: </label>
                        <?php echo list_industries_fields(); ?>
                        <p class="help-text small">Choose the field that you are seeking employment in.</p>
                        <span class="error"><?php echo isset($field_error) ? $field_error : false; ?></span>
                    </div>
                  </p>
                </div>
								
								<div class="form-group">
									<div class="col-sm-6">
										<br/>
										<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
										<input type="submit" name="submit" class="btn btn-success" value="Register">
									</div>
								</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.MAIN row -->	


<?php include_layout_template('form_footer.php'); ?> 