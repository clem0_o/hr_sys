<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insight Management</title>

<link href="../css/bootstrap.min.css" rel="stylesheet">
<style>
    background-color: #004003;
	
	body {
		background-color: #f5fdfa;
		margin: 0;
		height: 100%;
	}
	
    .container {
		background-color: #f5fdfa;
        width: 100%;
    }
        
    .container header nav .container-fluid {
        background-color: #000d01;
    }
       
    .container header nav .navbar-header a {
        color: #fff;
    }
        
    .container header nav .navbar-header a span{
        color: #00BF0A;
    }
    
    .container header nav .log-dits a {
        background-color: #004003;
		border: #004003;
		margin: 10px;
		color: #fff;
    }
        
    .container header nav .log-dits a:hover {
        background-color: #007F07;
		border: #007F07;
		margin: 10px;
		color: #fff;
    }
	
	.main-content {
		margin-top: 3.8em;
		margin-bottom: 0;
		min-height: 100%;
        height: auto !important;
        height: 100%;
        /* Negative indent footer by it's height */
        margin: auto -60px;
	}
	
	#push,
      #footer {
        height: 60px;
      }
      #footer {
        background-color: #f5f5f5;
      }
	
	
</style>

</head>

<body>
	
    <div class="container">
        <header>
            <nav class="navbar navbar-inverse navbar-fixed-top top" role="navigation">
                <div class="container-fluid">
                    <div class="row">
						<div class="col-xs-6">
							<div class="navbar-header">
								<a class="navbar-brand" href="#"><span>Insight</span> Management</a>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="log-dits pull-right">
								<a href="#" class="btn btn-primary">login</a>
							</div>
						</div>
					</div>
                    
                </div><!-- /.container-fluid -->
            </nav>
        </header>
	
		<div class="main-content">
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
					<div class="login-panel panel panel-default">
						<div class="panel-heading">Log in</div>
						<div class="panel-body">
							<form role="form">
								<fieldset>
									<div class="form-group">
										<input class="form-control" placeholder="E-mail" name="email" type="email" autofocus="">
									</div>
									<div class="form-group">
										<input class="form-control" placeholder="Password" name="password" type="password" value="">
									</div>
									<div class="checkbox">
										<label>
											<input name="remember" type="checkbox" value="Remember Me">Remember Me
										</label>
									</div>
									<a href="index.html" class="btn btn-primary">Login</a>
								</fieldset>
							</form>
						</div>
					</div>
				</div><!-- /.col-->
			</div><!-- /.row -->
		</div><!-- /.main-content -->
		
		<div id="push"></div>
		</div>
		<div id="footer">
			<div class="container">
				<p class="muted credit">Example courtesy <a href="http://martinbean.co.uk">Martin Bean</a> and <a href="http://ryanfait.com/sticky-footer/">Ryan Fait</a>.</p>
			</div>
		</div>
    </div><!-- /.Container -->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>	
</body>

</html>