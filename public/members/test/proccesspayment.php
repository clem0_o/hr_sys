<?php
require_once("../../../includes/initialize.php");
// Check if Lipisha ITN server has made a callback
if(isset($_POST)) 
{
    // Your API details for authentication
    $api_key       = "2165810f68e10142dfdcb1c39fcde901";        // # Check under Settings->API in dashboard
    $api_signature = "dlYEwDf8LxMo5KZW/OWjCx0ZoVSX5NTlTGBNO70jWhVY2MuVmirCWVc7tniY44sz6tjfoZl7inTKg/3+F7GGgJRMnxgA0Z/bNOrOhKY/M11edjFr57g2GjftfMNuwGPC2agds3/NNe3KEZJs7CClIOL3Iq/i9S/c/HpzSG4wOSc=";  // # Check under Settings->API in dashboard

    // Confirm if authentication details are genuine else log a fraud attempt
    if (($_POST["api_key"] == $api_key) && ($_POST["api_signature"] == $api_signature)) 
    {        
        // Process Initiate
        if($_POST["api_type"] == "Initiate")
        {           
            // Extract transaction details
            $date               = datetime();//$_POST["transaction_date"];
            $amount             = $_POST["transaction_amount"];
            $type               = $_POST["transaction_type"];
            $method             = $_POST["transaction_method"];
            $name               = $_POST["transaction_name"];
            $mobile             = $_POST["transaction_mobile"];
            $email              = $_POST["transaction_email"];
            $paybill            = $_POST["transaction_paybill"];
            $account_number     = $_POST["transaction_account_number"];
            $account_name       = $_POST["transaction_account_name"];
            $reference          = $_POST["transaction_reference"];
            $merchant_reference = $_POST["transaction_merchant_reference"];            
            $code		= $_POST["transaction_code"];            
            $status		= $_POST["transaction_status"];            
            
            // Perform action e.g update order/invoice as paid, save to database or log
            $status    = "OK";
            //process_payment($_POST, $status); // replace with your own actions
            //Add payment to the database
            $payment = OnlinePayment::make($reference, $amount, $account_name, $date, $type, $method, $merchant_reference, $mobile);
            if ($payment && $payment->create()) {
                //activate the account
                if (User::activate_account($merchant_reference) && Personal_details::activate_account($merchant_reference)) {
                    $message = "Your account has been activated Successfuly";
                }
            }

            // Acknowledge API call
            $response= array();
            $response["api_key"]                        = $_POST["api_key"];
            $response["api_signature"]                  = $_POST["api_signature"];
            $response["api_version"]                    = $_POST["api_version"];
            $response["api_type"]                       = "Receipt";
            $response["transaction_reference"]          = $_POST["transaction_reference"];
            $response["transaction_status_code"]        = "001";
            $response["transaction_status"]             = "Success";
            $response["transaction_status_description"] = "Transaction received successfully.";
            $response["transaction_custom_sms"]         = "We have recieved your payment, your acount is activated! " . $code . " confirmed."; // Customize SMS. *Premium 
            $json_response                              = json_encode($response);
            header("Content-Type: application/json"); 
            echo $json_response; // This should be the only printed output of this page 
                                 // to prevent header session already sent error occuring              
        }
        // Process Acknowledge
        if($_POST["api_type"] == "Acknowledge")
        {
            // Perform action e.g update your transaction table as confirmed
            $status    = "Acknowledged"; 
            //process_payment($_POST, $status);  // replace with your own actions                     
        }
    }
    else 
    {    
        // Log attempted fraud if api credentials are wrong
        $status    = "Fraud";
        //process_payment($_POST, $status); // replace with your own actions 
    }           
}