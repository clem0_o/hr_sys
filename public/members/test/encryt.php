<?php
//require once the needed class
require_once("../../../includes/initialize.php");

?>
<?php
//we read all from users
$all_users = User::find_all();
$count = 0;



foreach ($all_users as $user) {
    //read a password
    $old_password = $user->password;
    //echo '<br/>';
    //check if the value is already encrypted
    if (strlen($old_password) < 25) {
        //encrypt the password
        $new_password = password_hash($old_password, PASSWORD_DEFAULT);
        
        //update the password
        $user->password = $new_password;
        //save to the db
        if ($user->update()) {
            $count += 1;
        }
    }
}

echo $count;

?>