<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Insight Management</title>

	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><span>Insight</span>Management</a>
			</div>
			<!-- Single button -->
			<div class="btn-group pull-right user-menu">
				<button type="button" class="btn btn-default glyphicon glyphicon-user dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			     <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="#">Action</a></li>
					<li><a href="#">Another action</a></li>
					<li><a href="#">Something else here</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="#">Separated link</a></li>
				</ul>
			</div>
		</div>
	</nav><!-- End of head nav-->
	<!-- Side bar nav-->
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<ul class="nav menu">
			<li class="active"><a href="#">
				<i class="glyphicon glyphicon-user visible-xs"></i> 
				<span class="visible-sm visible-md visible-lg"><i class="glyphicon glyphicon-user"></i> Profile </span>
			</a></li>
			<li class="active"><a href="#">
				<i class="glyphicon glyphicon-briefcase visible-xs"></i> 
				<span class="visible-sm visible-md visible-lg"><i class="glyphicon glyphicon-briefcase"></i> Applied Jobs </span>
			</a></li>
			<li class="active"><a href="#">
				<i class="glyphicon glyphicon-tasks visible-xs"></i> 
				<span class="visible-sm visible-md visible-lg"><i class="glyphicon glyphicon-tasks"></i> Available Jobs </span>
			</a></li>
			<!--<li class="active"><a href="#">
				<i class="glyphicon glyphicon-user visible-xs"></i> 
				<span class="visible-sm visible-md visible-lg"><i class="glyphicon glyphicon-user"></i> Profile </span>
			</a></li>-->
		</ul>
	</div><!-- End of side nav-->
	<!--Main content area -->
	<div class="col-xs-10 col-xs-offset-1 col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#" class="glyphicon glyphicon-home"></a></li>
				<li class="active">Change with php of js</li>
			</ol>
		</div><!--/.row -Bread crumbs-->

		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">Profile</h2>
			</div>
		</div><!--/.row -Page header-->
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						Personal Details
					</div>
					<div class="panel-body">
						<p><label>Name: <?php?></label></p>
						<p><label>Phone No: </label></p>
						<p><label>Email: </label></p>
						<p><label>Residence: </label></p>
						<p><label>Account Status: </label></p>
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12 ">	
			</div>
		</div><!--/.row-->
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-default" id="tabs">
					<div class="panel-head">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#working_history" aria-controls="working_history" role="tab" data-toggle="tab">Working History</a></li>
							<li role="presentation"><a href="#edu_background" aria-controls="edu_background" role="tab" data-toggle="tab">Education Background</a></li>
							<li role="presentation"><a href="#other_qualifications" aria-controls="other_qualifications" role="tab" data-toggle="tab">Other Qualifications</a></li>
							<li role="presentation"><a href="#referees" aria-controls="referees" role="tab" data-toggle="tab">Referees</a></li>
						</ul>
					</div>
					<div class="panel-body">
						 <!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="working_history">
								<h4>Working History</h4>
								<div class="table-responsive">
									<?php if (!empty($work_history)) { ?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>No</th>
												<th>Organisation Name</th>
												<th>Position Held</th>
												<th>Jobs Description</th>
												<th>Time At work</th>
												<th>Edit</th>
											</tr>
										</thead>
										<tbody>
		                                  <?php 
		                                  foreach ($work_history as $work) :
		                                    $output = "<tr>";
		                                      $output .= "<td>1.</td>";
		                                      $output .= "<td>".$work->organization_name."</td>";
		                                      $output .= "<td>".$work->position_held."</td>";
		                                      $output .= "<td>".$work->job_description."</td>";
		                                      $output .= "<td>".$work->start_date." - ".$work->last_date()."</td>";
		                                      $output .= "<td><a href=\"edit_work_profile.php?work_id=".$work->id."\" class=\"btn btn-link\">Edit</a></td>";
		                                    $output .= "</tr>";
		                                    echo $output;
		                                    endforeach;
		                                    } else {
		                                      echo "You have no entry yet";
		                                      } ?>
	                                    </tbody>
									</table>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="edu_background">
								<h4>Education Background</h4>
								<div class="table-responsive">
									<?php if (!empty($education_history)) { ?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Institution Name</th>
												<th>Course Taken</th>
												<th>Level</th>
												<th>Major</th>
												<th>Time At the institution</th>
											</tr>
										</thead>
										<tbody>
		                                  <?php  foreach ($education_history as $education) {
		                                    $output = "<tr>";
		                                      $output .= "<td>1.</td>";
		                                      $output .= "<td>".$education->institution."</td>";
		                                      $output .= "<td>".$education->course_taken."</td>";
		                                      $output .= "<td>".$education->level."</td>";
		                                      $output .= "<td>".$education->major."</td>";
		                                      $output .= "<td>".$education->start_date." - ".$education->last_date()."</td>";
		                                      $output .= "<td><a href=\"edit_education_profile.php?edu_id=".$education->id."\" class=\"btn btn-link\">Edit</a></td>";
		                                    $output .= "</tr>";
		                                    echo $output;
		                                    }
		                                    } else {
		                                      echo "You have no entry yet";
		                                      } ?>
	                                    </tbody>
									</table>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="other_qualifications">
								<h4>Other Qualifications</h4>
								<div class="table-responsive">
									<?php if (!empty($other_qualifications)) { ?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Description</th>
												<th>Type</th>
												<th>Where Attained</th>
											</tr>
										</thead>
										<tbody>
		                                  <?php foreach ($other_qualifications as $qualification) {
		                                    $output = "<tr>";
		                                      $output .= "<td>1.</td>";
		                                      $output .= "<td>".$qualification->name."</td>";
		                                      $output .= "<td>".$qualification->description."</td>";
		                                      $output .= "<td>".$qualification->type."</td>";
		                                      $output .= "<td>".$qualification->where_attained."</td>";
		                                      $output .= "<td><a href=\"edit_otherq_profile.php?otherq_id=".$qualification->id."\" class=\"btn btn-link\">Edit</a></td>";
		                                    $output .= "</tr>";
		                                    echo $output;
		                                    }
		                                    } else {
		                                      echo "You have no entry yet";
		                                      } ?>
	                                    </tbody>
									</table>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="referees">
								<h4>Referees</h4>
								<div class="table-responsive">
									<?php  if (!empty($referees)) { ?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Title</th>
												<th>Phone No</th>
												<th>Email</th>
											</tr>
										</thead>
										<tbody>
		                                  <?php foreach ($referees as $qualification) {
		                                    $output = "<tr>";
		                                      $output .= "<td>1.</td>";
		                                      $output .= "<td>".$qualification->name."</td>";
		                                      $output .= "<td>".$qualification->description."</td>";
		                                      $output .= "<td>".$qualification->type."</td>";
		                                      $output .= "<td>".$qualification->where_attained."</td>";
		                                      $output .= "<td><a href=\"edit_otherq_profile.php?otherq_id=".$qualification->id."\" class=\"btn btn-link\">Edit</a></td>";
		                                    $output .= "</tr>";
		                                    echo $output;
		                                    }
		                                    } else {
		                                      echo "You have no entry yet";
		                                      }  ?>
	                                    </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div><!--/.panel-->
			</div>
		</div><!--/.row-->	
	</div><!--End of main-->
<script type="text/javascript" src="assets/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>