<?php require_once("../../../../includes/initialize.php"); ?>
<?php include_layout_template('../members/test/New_look/layouts/header.php'); ?>
	
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<!--<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<ul class="nav menu">
			<li class="active"><a href="eprofile.php"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Profile </a></li>
			<li><a href="eapplied_jobs.php"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Applied Jobs</a></li>
			<li><a href="eavailable_jobs.php"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Available Jobs</a></li>
			
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Job seeker</li>
			</ol>
		</div><!--/.row -Bread crumbs-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Applied Jobs</h1>
			</div>
		</div><!--/.row -Page header-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						Interviews Schedule
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Date</th>
									<th>Venue</th>
									<th>Job Title</th>
									<th>Company</th>
									<th>Confirm Attendance</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						Status Of jobs Applied
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Job Title</th>
									<th>Company</th>
									<th>Status</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			
		</div><!--/.row-->
								
		<div class="row">
			
		</div><!--/.row-->
	</div>	<!--/.main-->

<?php include_layout_template('../members/test/New_look/layouts/footer.php'); ?>

