<?php require_once("../../../../includes/initialize.php"); ?>
<?php include_layout_template('../members/test/New_look/layouts/header.php'); ?>
	
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<!--<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<ul class="nav menu">
			<li class="active"><a href="eprofile.php"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Profile </a></li>
			<li><a href="eapplied_jobs.php"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Applied Jobs</a></li>
			<li><a href="eavailable_jobs.php"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Available Jobs</a></li>
			
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row -Bread crumbs-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Available Jobs</h1>
			</div>
		</div><!--/.row -Page header-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="well">
					<form action="" class=" form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Job Key Word">
                    </div>
                    <div class="form-group">
                        <select name="" id="" class="form-control">
                            <option>Select Your City</option>
                            <option selected>New york, CA</option>
                            <option>New york, CA</option>
                            <option>New york, CA</option>
                            <option>New york, CA</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="" id="" class="form-control">
                            <option>Select Your Category</option>
                            <option selected>Graphic Design</option>
                            <option>Web Design</option>
                            <option>App Design</option>
                        </select>
                    </div>
                    <input type="submit" class="btn" value="Search">
                </form>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Jobs</div>
					<div class="panel-body">
						<div class="job-posts table-responsive">
                            <table class="table">
                                <tr class="odd wow fadeInUp" data-wow-delay="1s">
                                    <td class="tbl-logo"><img src="img/job-logo1.png" alt=""></td>
                                    <td class="tbl-title"><h4>Web Designer <br><span class="job-type">full time</span></h4></td>
                                    <td><p><i class="icon-location"></i>San Franciso, USA</p></td>
                                    <td><p>&dollar; 14000</p></td>
                                    <td class="tbl-apply"><a href="#">Apply now</a></td>
                                </tr>
                                <tr class="even wow fadeInUp" data-wow-delay="1.1s">
                                    <td class="tbl-logo"><img src="img/job-logo2.png" alt=""></td>
                                    <td class="tbl-title"><h4>Front End Developer <br><span class="job-type">full time</span></h4></td>
                                    <td><p><i class="icon-location"></i>San Franciso, USA</p></td>
                                    <td><p>&dollar; 14000</p></td>
                                    <td class="tbl-apply"><a href="#">Apply now</a></td>
                                </tr>
                                <tr class="odd wow fadeInUp" data-wow-delay="1.2s">
                                    <td class="tbl-logo"><img src="img/job-logo3.png" alt=""></td>
                                    <td class="tbl-title"><h4>HR Manager <br><span class="job-type">full time</span></h4></td>
                                    <td><p><i class="icon-location"></i>San Franciso, USA</p></td>
                                    <td><p>&dollar; 14000</p></td>
                                    <td class="tbl-apply"><a href="#">Apply now</a></td>
                                </tr>
                            </table>
                        </div>	
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
								
		<div class="row">
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	
<?php include_layout_template('../members/test/New_look/layouts/footer.php'); ?>
