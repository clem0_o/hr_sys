<?php
//require once the needed class
require_once("../../../includes/initialize.php");

?>
<?php
//we read all from users
$all_paid = Payment::find_all();
$count_user = 0;
$count_dets = 0;



foreach ($all_paid as $single_paid) {
    //read trans-code
    $trans_code = $single_paid->transaction_code;
    $confirmed = User_payment::check_payment($trans_code);
    //Read the user and personal dert tables if its there
    if ($confirmed) {
        $user_paid = User::find_by_id((int) $confirmed->user_id);
        $det_paid = Personal_details::find_by_user_id((int) $confirmed->user_id);
        
        //Now here everything fall apart it gives an error unrecognised object
        if (!$user_paid->payment=='Active') {
            $user_paid->payment='Active';
            if ($user_paid->update()) {
                $count_user += 1;
            }
        }
        
        if (!$det_paid->pay_status=='Active') {
            $det_paid->payment='Active';
            if ($det_paid->update()) {
                $count_dets += 1;
            }
        }
    }
    
}

echo $count_user.'<br/>';
echo $count_dets.'<br/>';

?>