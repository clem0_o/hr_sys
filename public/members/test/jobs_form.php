<?php
require_once("../../../includes/initialize.php");
$list_jobs = Job::count_all_in ($field);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <title>Insight Management</title>
	<!-- BOOTSTRAP STYLES-->
   <link href="../../css/bootstrap.css" rel="stylesheet" />
   <!-- FONTAWESOME STYLES-->
   <link href="../../css/font-awesome.css" rel="stylesheet" />
      <!-- CUSTOM STYLES-->
   <link href="../../css/custom.css" rel="stylesheet" />
      <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head><!-- Header -->
<body>
	<div id="wrapper" class="lighter-bg-overright">
		<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html"> Admin </a> 
			</div>
			<div style="color: white;
					padding: 15px 50px 5px 50px;
					float: right;
					font-size: 16px;">
				<a href="../logout.php" class="btn btn-success square-btn-adjust">Logout</a>
				<a href="" class="btn btn-success square-btn-adjust">Login</a>
            <a href="" class="btn btn-primary square-btn-adjust">Sign up</a>
			</div>
		</nav>
        <!--End of hearder-->


		<!-- Start of the Page-->
      <div id="page-wrapper">
         <div class="container-fluid">
               <!-- ====Page Heading==== -->

            
				
				
				<!--Showing the jobs-->
				
                <div class="row jobs"><!--These is where the jobs goes-->
                    <div class="col-md-8 lighter-bg">
						
                <div class="text-center">
                    <h1>Recent Jobs</h1>
						  <hr/>
                </div>	
							
            <div class="row lighter-bg">
               <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                  <form action="" class=" form-inline">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Job Key Word">
                     </div>
                     <div class="form-group">
                        <select name="" id="" class="form-control">
                           <option>Select Your City</option>
                           <option selected>New york, CA</option>
                           <option>New york, CA</option>
                           <option>New york, CA</option>
                           <option>New york, CA</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <select name="" id="" class="form-control">
                           <option>Select Your Category</option>
                           <option selected>Graphic Design</option>
                           <option>Web Design</option>
                           <option>App Design</option>
                        </select>
                     </div>
                     <input type="submit" class="btn" value="Search">
						</form>
               </div>
            </div>
            <!-- /.Search bar on top -->
							
                        <div class="job-posts table-responsive">
                            <table class="table">
                                <tr class="odd wow fadeInUp">
                                    <td class="tbl-logo"><img src="img/job-logo1.png" alt=""></td>
                                    <td class="tbl-title"><h4>Web Designer <br><span class="job-type">full time</span></h4></td>
                                    <td><p><i class="icon-location"></i>San Franciso, USA</p></td>
                                    <td><p>&dollar; 14000</p></td>
                                    <td class="tbl-apply"><a href="#">Apply now</a></td>
                                </tr>
                                <tr class="even wow fadeInUp">
                                    <td class="tbl-logo"><img src="img/job-logo2.png" alt=""></td>
                                    <td class="tbl-title"><h4>Front End Developer <br><span class="job-type">full time</span></h4></td>
                                    <td><p><i class="icon-location"></i>San Franciso, USA</p></td>
                                    <td><p>&dollar; 14000</p></td>
                                    <td class="tbl-apply"><a href="#">Apply now</a></td>
                                </tr>
                                <tr class="odd wow fadeInUp">
                                    <td class="tbl-logo"><img src="img/job-logo3.png" alt=""></td>
                                    <td class="tbl-title"><h4>HR Manager <br><span class="job-type">full time</span></h4></td>
                                    <td><p><i class="icon-location"></i>San Franciso, USA</p></td>
                                    <td><p>&dollar; 14000</p></td>
                                    <td class="tbl-apply"><a href="#">Apply now</a></td>
                                </tr>
                                <tr class="even wow fadeInUp">
                                    <td class="tbl-logo"><img src="img/job-logo4.png" alt=""></td>
                                    <td class="tbl-title"><h4>Internship Designer <br><span class="job-type">full time</span></h4></td>
                                    <td><p><i class="icon-location"></i>San Franciso, USA</p></td>
                                    <td><p>&dollar; 14000</p></td>
                                    <td class="tbl-apply"><a href="#">Apply now</a></td>
                                </tr>
                            </table>
                        </div>
                        <div class="more-jobs">
                            <a href=""> <i class="fa fa-refresh"></i>View more jobs</a>
                        </div>
                    </div>
						  
						  
                    <div class="col-md-2 hidden-sm">
                        <div class="lighter-bg">
                            <h2>Seeking a job?</h2>
                            <a href="#">Create a Account</a>
                        </div>
                    </div>
						  
                </div>
            <hr>
				
				
			</div>
            <!-- /.container-fluid -->

      </div>
      <!-- /#page-wrapper -->

   <!--Start of footer -->
   </div>
      <!-- /. WRAPPER  -->
      <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
      <!-- JQUERY SCRIPTS -->
      <script src="../../js/jquery-1.11.1.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
      <script src="../../js/bootstrap.min.js"></script>
      <!-- METISMENU SCRIPTS -->
      <script src="../../js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
      <script src="../../js/custom.js"></script>
</body>
</html>