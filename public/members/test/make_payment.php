<?php
//require once the needed class
require_once("../../../includes/initialize.php");
//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
}
//####################form processing#########################
//form has been submited
//if (isset($_POST['submit'])) {
//  $uid = $session->user_id;
//  $transaction_code = trim($_POST['mpesa_pin']);
//  $name = trim($_POST['first_name']);
//  $date = datetime();
//
//  $error = false;
//
//  if (empty($transaction_code)) {
//    $transaction_code_error = 'Transaction code is empty, Please fill';
//    $error = true;
//  }
//
//  if (empty($name)) {
//     $name_error = 'Name is empty, Please fill';
//    $error = true;
//  }
//
//  if ($error === false) {
//    $exits = User_payment::find_by_trans_id ($transaction_code);
//      if (empty($exits)) {
//      $user_payment = User_payment::make($uid, $transaction_code, $name, $date) ;
//      if ($user_payment && $user_payment->create()) {
//        redirect_to("employees/index.php");
//      }
//
//    } else {
//      $message = 'The code you entered has already been used.';
//    }
//  }
//} else {
//  $transaction_code = "";
//  $name = "";
//}
?>
<?php

?>
<?php include_layout_template('header_forms.php'); ?>

</div>
</div>
</div>
<div class="row">
  <div class="col-sm-6 col-sm-offset-3 form-box">
    <div class="form-top">
      <div class="form-top-left">
        <h3>Register in our Site</h3>
        <div class="text-danger bg-danger">
          <span class="error_message"><?php echo output_message($message); ?></span>
          </div>
          <p><ul class="mpesa">
						<li>Go to M-PESA Menu on your phone</li>
						<li>Select Pay Bill (may be under the Lipa Na M-Pesa)</li>
						<li>Enter Business Number - 934870</li>
						<li>Enter Account Number - <?php echo "imc/".$session->user_id; ?></li>
						<li>Enter Amount - 600</li>
						<li>Enter your M-PESA PIN</li>
						<li>Send and wait for SMS receipt from INSIGHT MANAGEMENT</li>
						<btn><a href="" class="btn btn-success">finish transaction</a></btn>
					</ul></p>
          <h4><?php echo isset($message) ? $message : false; ?></h4>
      </div>
      <div class="form-top-right">
        <span><img src="../img/ico/logo1.png" /></span>
      </div>
    </div>
<div class="form-bottom">

	<!--form to collect mpesa details from the client-->
  <!--<form role="form" action="" method="post" class="registration-form">
		<div class="form-group">
			<label class="control-label" for="mpesa_pin">Enter the transaction code</label>
			<input type="text" name="mpesa_pin" placeholder="mpesa pin" class="form-control" id="mpesa_pin" value="<?php //echo !empty($mpesa_pin) ? $mpesa_pin : false;  ?>">
      <span class="error"><?php //echo isset($transaction_code_error) ? $transaction_code_error : false; ?></span>
		</div>
    <div class="form-group">
		  <label class="control-label" for="first_name">Enter your first name as in the m-pesa message</label>
			<input type="text" name="first_name" placeholder="first name..." class="form-last-name form-control" id="first_name" value="<?php //echo !empty($name) ? $name : false;  ?>">
      <span class="error"><?php //echo isset($name_error) ? $name_error : false; ?></span>
		</div>
	<button type="submit" name="submit" class="btn btn-lg btn-success">Submit!</button>
  <br/><br/>
  <a class="btn btn-lg btn-primary center-block" href="employees/index.php">Skip</a>
</form>-->
</div>
</div>
</div>
<?php include_layout_template('footer_forms.php'); ?>  