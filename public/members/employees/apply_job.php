<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$apply_job = Job::find_by_id($_GET["jid"]);
$user = User::find_by_id($session->user_id);
$education = Education_bg::find_by_user_id($session->user_id);
?>
<?php
if (isset($_POST['submit'])) {
	if (Token::check($_POST['token'])) {
		$app_exists = Application::check_app($session->user_id, $_GET['jid']);
		if (empty($app_exists)) {
			$allowed_types = array(
				'application/msword',
				'text/pdf',
				'application/vnd.oasis.opendocument.tex',
				'application/pdf'
			);
			
			$error = false;
			
			if (empty($_FILES['cv']['name'])) {
				$uploaded_error = 'There is no file attached';
				$error = true;
			} elseif ($_FILES['cv']['size'] > 6000000) {
				$uploaded_error = 'The attached is too big, please make sure its less than 6Mb.';
				$error = true;
			} elseif (!in_array($_FILES['cv']['type'], $allowed_types) ) {
				$uploaded_error = 'The attached file is not of permited type, please make sure its a .Doc, .Docx, .pdf .rtf or .odt.';
				$error = true;
			}
			
			if ($error===false) {
				$application = new Application;
				$cv = new Cv;
				$cv_name = $_GET["jid"].$session->user_id.$_FILES['cv']['name'];

				$cv->id = (INT) 0;
				$cv->user_id = $user->id;
				$cv->reff_number = $_GET["jid"];
				$cv->date_posted    = datetime();
				$cv->attach_file($_FILES['cv'], $cv_name);
				if ($cv_id = $cv->save()) {
					
					$application->id = (INT) 0;
					$application->user_id =(INT) $user->id;
					$application->cv_id =(INT) $cv_id;
					$application->job_id =(INT) $_GET["jid"];
					$application->aplication_date = datetime();
					$application->additional_info = trim($_POST['additional_info']);
					$application->status = "Received";
					//Insert into the application table
					$application->create();
					redirect_to("available_jobs.php");
				} else {
					$message = "File upload failed for some reason";
				}
			}
		} else {
			$message = "You have already made an application";
			redirect_to('available_jobs.php');
		}
	}
}


?>
<?php include_layout_template('form_header1in.php'); ?>

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3>
						<span class="glyphicon glyphicon-open"> Apply for a job </span>
				</div><!--End of the header-->
				<div class="panel-body">
					<?php
					if (!empty($message)) {$error_out = "<div class=\"alert alert-danger\" role=\"alert\">";
						$error_out .= "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>";
						$error_out .= "<span class=\"sr-only\">Error:</span>";
						$error_out .=  output_message($message); 
					$error_out .= "</div>";
					echo $error_out;
					}?><!--End of Error section -->
					<p> </p>
					<hr/>
					
					<div class="well">
						<h3 class="page-header">
							<label>Job Details</label>
						</h3>
						<div class="row">
							<div class="col-sm-4">
								<p class="lead">
									<label>Job Title: </label>
									<?php echo $apply_job->job_title; ?>
								</p>
							</div>
							<div class="col-sm-4">
								<p class="lead">
									<label>Job Category: </label>
									<?php echo $apply_job->category; ?>
								</p>
							</div>
							<div class="col-sm-4">
								<p class="lead">
									<label>Job Location: </label>
									<?php echo $apply_job->location; ?>
								</p>
							</div>
							<div class="col-sm-12">
								<p class="lead">
									<label>Job Description: </label>
									<?php echo $apply_job->job_description_detailed; ?>
								</p>
								<p class="lead">
									<label>Job Requirements: </label>
									<?php echo $apply_job->requirements; ?>
								</p>
								<p class="lead">
									<label>Job Qualifications: </label>
									<?php echo $apply_job->qualifications; ?>
								</p>
							</div>
						</div>
					</div><!--End of Jobs details-->
					<hr/>
					
					<div class="well">
						<h3 class="page-header">
							<label>Personal Details</label>
						</h3>
						<div class="row">
							<form action="apply_job.php?jid=<?php echo $_GET["jid"]; ?>"  enctype="multipart/form-data" method="post">
								<div class="col-sm-6">
									<p class="lead">
										<label>Name: </label>
										<?php echo $user->full_name(); ?>
									</p>
									<p class="lead">
									  <label>Phone Number: </label>
									  <?php echo $user->phone_number; ?>
									</p>
									<p class="lead">
										<label>Education Summary: </label>
		
										<?php if (!empty($education)) { ?>
										<?php echo $education->summary(); ?>
										<?php } else { echo "Details not entered"; }?>
									</p>
								</div>
								<div class="col-sm-6">
									<p class="lead">
										<label>Upload your CV: </label>
											<p><label class="file">
												<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
												<input type="file" name="cv" id="cv">
												<span class="file-custom"></span>
											</label></p>
											<span class="error"><?php echo isset($uploaded_error) ? $uploaded_error : false; ?></span>
									</p>
									<p class="lead">
										<label>Add a short description of yourself: </label>
										 <textarea class="form-control" name="additional_info" rows="5" id="additional_info"></textarea>
									</p>
								</div>
								<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
								<button type="submit" name="submit" class="btn btn-success">Apply</button>
								<a href="available_jobs.php" class="btn btn-primary" role="button">Cancel</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->

<?php include_layout_template('form_footer1in.php'); ?>
