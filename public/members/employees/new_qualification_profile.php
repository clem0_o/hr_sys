<?php
require_once("../../../includes/initialize.php");

//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
  
}
?>
<?php //form proccessing
$message = "";
//form has been submited
if (isset($_POST['submit'])) {
	$other = new OtherQualifications; //instanciate the employer class

	//read the input data
  //$employer->id = $session->user_id;
  $other->id =(INT) 0;
	$other->user_id = $session->user_id;
	$other->name = trim($_POST['name']);
	$other->description = trim($_POST['description']);
	$other->type = trim($_POST['type']);
  $other->where_attained = trim($_POST['where_attained']);

	//Validate the input data
	if ($other->name=="") { $message = "provide name!";  
  } else if($other->description=="") { $message = "provide description !";  
	} else if($other->type=="") { $message = "provide type !";
	} else if($other->where_attained=="") { $message = "provide where_attained !";
  } else {
		//Enter the data in the database
		if($other->create()){
			$session->message = "Details successfully added!!";
			redirect_to('profile.php');
		}
	}
}
?>
<?php 
!empty(Employer::find_by_user_id($session->user_id)) ? $employer=Employer::find_by_user_id($session->user_id) : false; 
?>

<?php include_layout_template('header_employee.php'); ?>


<div id="page-wrapper">
    <div class="container-fluid">

    <?php echo output_message($message); ?>

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                  Add Qualifications/Other Certs History
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-bar-chart-o"></i> Profile
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

<div class="row">
<div class="well">

<form role="form" action="new_qualification_profile.php"  method="post" class="registration-form">
  <div class="form-group">
  <div class="col-xs-4">
    <label for="name" class="control-label"> Certificate Name: </label>
    <p>
      <input type="text" name="name" class="form-control" id="name" placeholder="Certificate name" />
    </p>
  </div>


  <div class="col-xs-4">
    <label for="type" class="control-label"> Type: </label>
    <p>
      <input type="text" name="type" class="form-control" id="type" placeholder="type of cert" />
    </p>
  </div>

  <div class="col-xs-4">
    <label for="where_attained" class="control-label"> Level of Study: </label>
    <p>
      <input type="text" name="where_attained" class="form-control" id="where_attained" placeholder="where attained the study" />
    </p>
  </div>
  </div>

  <div class="form-group">
  <div class="col-xs-12">
    <label for="description" class="control-label"> Description: </label>
    <p>
      <textarea type="text" name="description" class="form-control" id="description" placeholder="description"></textarea>
    </p>
  </div>

  </div>

  <button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
  <a href="profile.php" class="btn btn-default">Cancel</a>
</form>
</div><!--end of well-->
</div><!-- end of form row -->
</div>
    <!-- /.container-fluid -->

</div><!-- end of the page wrapper -->
<?php include_layout_template('footer_employee.php'); ?>