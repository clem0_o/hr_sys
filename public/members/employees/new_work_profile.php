<?php
require_once("../../../includes/initialize.php");

//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
  
}
?>
<?php //form proccessing
$message = "";
//form has been submited
if (isset($_POST['submit'])) {

	$id =(INT) 0;
  $user_id= $session->user_id;
	$orgname = trim($_POST['organization_name']);
	$posheld = trim($_POST['position_held']);
	$jobdes = trim($_POST['job_description']);
  $startdate = trim($_POST['start_year'])."-".trim($_POST['start_month'])."-".trim($_POST['start_day']);
  $enddate = trim($_POST['end_year'])."-".trim($_POST['end_month'])."-".trim($_POST['end_day']);
  $reasfleaving = trim($_POST['reason_for_leaving']);

  //Validate the input data
  $error = false;

  if (empty($orgname)) {
    $orgname_error = "Please fill the name of the Organization";
    $error = true;
  }

  if (empty($posheld)) {
    $posheld_error = "Please fill the name of the Position Held";
    $error = true;
  }

  if (empty($jobdes)) {
    $jobdes_error = "Please fill the Job Description";
    $error = true;
  }

  if (empty($startdate)) {
    $startdate_error = "Please fill the date when you started the job";
    $error = true;
  }

  if (empty($enddate)) {
    $enddate_error = "Please fill the date that you stopped working";
    $error = true;
  }

  if (empty($reasfleaving)) {
    $reasfleaving_error = "Please fill the reason for leaving the working";
    $error = true;
  }

  if ($error === false) { 
    $work = WorkingHistory::make($id, $user_id, $orgname, $posheld, $jobdes, $startdate, $enddate, $reasfleaving) ; 
    if ($work && $work->create()) {
      $session->message = "Details successfully added!!";
      redirect_to('profile.php');
    }
  }
	
}
?>
<?php include_layout_template('header_employee.php'); ?>


<div id="page-wrapper">
    <div class="container-fluid">

    <?php echo output_message($message); ?>

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                  Add Work History
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-bar-chart-o"></i> Profile
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

<div class="row">
<div class="well">

<form role="form" action="new_work_profile.php"  method="post" class="registration-form">
  <div class="form-group">
  <div class="col-xs-6">
    <label for="organization_name" class="control-label"> Organization Name: </label>
    <p>
      <input type="text" name="organization_name" class="form-control" id="organization_name" placeholder="organization name" value="<?php echo isset($orgname) ? $orgname : false; ?>" required>
      <span><?php echo isset($orgname_error) ? $orgname_error : false; ?></span>
    </p>
  </div>


  <div class="col-xs-6">
    <label for="position_held" class="control-label"> Position Held: </label>
    <p>
      <input type="text" name="position_held" class="form-control" id="position_held" placeholder="position held" value="<?php echo isset($posheld) ? $posheld : false; ?>" required>
      <span><?php echo isset($posheld_error) ? $posheld_error : false; ?></span>
    </p>
  </div>
  </div>

<div class="form-group">
  <div class="col-xs-12">
    <label for="job_description" class="control-label"> Job Description: </label>
    <p>
      <textarea type="text" name="job_description" class="form-control" id="job_description" placeholder="job description" required><?php echo isset($jobdes) ? $jobdes : false; ?></textarea>
      <span><?php echo isset($jobdes_error) ? $jobdes_error : false; ?></span>
    </p>
  </div>
  </div>

  <div class="form-group">
  <div class="col-xs-6">
    <label for="start_date" class="control-label"> Start Date: </label>
    <p>
      <?php echo months_of_year("start_month"); ?>
      <?php echo days_of_the_month("start_day"); ?>
      <?php echo years_long("start_year"); ?>
      <span><?php echo isset($startdate_error) ? $startdate_error : false; ?></span>
    </p>
  </div>


  <div class="col-xs-6">
    <label for="end_date" class="control-label"> End Date: </label>
    <p>
      <?php echo months_of_year("end_month"); ?>
      <?php echo days_of_the_month("end_day"); ?>
      <?php echo years_long("end_year"); ?>
      <span><?php echo isset($enddate_error) ? $enddate_error : false; ?></span>
    </p>
  </div>
  </div>

<div class="form-group">
  <div class="col-xs-12">
    <label for="reason_for_leaving" class="control-label"> Reason for leaving: </label>
    <p>
      <textarea type="text" name="reason_for_leaving" class="form-control" id="reason_for_leaving" placeholder="reason for leaving..." required><?php echo isset($reasfleaving) ? $reasfleaving: false; ?></textarea>
      <span><?php echo isset($reasfleaving_error) ? $reasfleaving_error : false; ?></span>
    </p>
  </div>
  </div>

  <button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
  <a href="profile.php" class="btn btn-default">Cancel</a>
</form>
</div><!--end of well-->
</div><!-- end of form row -->
</div>
    <!-- /.container-fluid -->

</div><!-- end of the page wrapper -->
<?php include_layout_template('footer_employee.php'); ?>