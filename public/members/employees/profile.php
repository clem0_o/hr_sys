<?php
require_once("../../../includes/initialize.php");
?>
<?php
//This brings in the user details by searching by id
$user = User::find_by_id($session->user_id);
$personal_dets = Personal_details::find_by_user_id($session->user_id);

//Fetch for the work history of the job seeker 
$work_history = WorkingHistory::find_all_user_id($user->id);

//Fetch for the education history of the job seeker 
$education_history = Education_bg::find_all_user_id($user->id);

//Fetch for the other qualification history of the job seeker 
$other_qualifications = OtherQualifications::find_all_user_id($user->id);

//Fetch for the refferees of the job seeker 
$referees = Referee::find_all_user_id($user->id);
?>

<?php include_layout_template('header.php'); ?>

<!-- Side bar nav-->
<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
	<ul class="nav menu">
		<li class="active"><a href="#">
			<i class="glyphicon glyphicon-user visible-xs"></i> 
			<span class="visible-sm visible-md visible-lg"><i class="glyphicon glyphicon-user"></i> Profile </span>
		</a></li>
		<li class="active"><a href="applied_jobs.php">
			<i class="glyphicon glyphicon-briefcase visible-xs"></i> 
			<span class="visible-sm visible-md visible-lg"><i class="glyphicon glyphicon-briefcase"></i> Applied Jobs </span>
		</a></li>
		<li class="active"><a href="available_jobs.php">
			<i class="glyphicon glyphicon-tasks visible-xs"></i> 
			<span class="visible-sm visible-md visible-lg"><i class="glyphicon glyphicon-tasks"></i> Available Jobs </span>
		</a></li>
		<!--<li class="active"><a href="#">
			<i class="glyphicon glyphicon-user visible-xs"></i> 
			<span class="visible-sm visible-md visible-lg"><i class="glyphicon glyphicon-user"></i> Profile </span>
		</a></li>-->
	</ul>
</div><!-- End of side nav-->
		
<!--Main content area -->
<div class="col-xs-10 col-xs-offset-1 col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<!-- Bread crums-->
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#" class="glyphicon glyphicon-home"></a></li>
			<li class="active">Profile (<?php echo $user->full_name(); ?>)</li>
		</ol>
	</div><!--/.row -Bread crumbs-->
	<!--Page head-->
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Profile</h1>
		</div>
	</div><!--/.row -Page header-->
	
	<!--Personal Details-->
	<div class="row">
		<div class="col-lg-8 col-sm-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					Personal Details
				</div>
				<div class="panel-body">
					<div id="user_status"></div><!--User feedback details come there -->
					<p>
						<label>Name: <?php echo $user->full_name(); ?></label>
						<button class="pull-right btn btn-primary glyphicon glyphicon-edit" type="button" data-toggle="collapse" data-target="#names_form" aria-expanded="false" aria-controls="names_form">Edit</button>
						<div class="collapse" id="names_form">
							<div class="well">
								<form action="" name="edit_names" id="edit_names" method="post" class="in-line">
									<div class="form-group">
										<input type="text" placeholder="First name..." name="firstName" value="<?php echo $user->first_name; ?>">
										<input type="text" placeholder="Last name..." name="lastName" value="<?php echo $user->last_name; ?>">
										<input type="hidden" name="userId" value="<?php echo $session->user_id; ?>">
										<input type="submit" name="editNameSubmit" id="editNameSubmit" value="submit" class="pull-right btn btn-success">
									</div>
								</form>
							</div>
						</div>
					</p>
					<p>
						<label>Phone No: <?php echo $user->phone_number; ?> </label>
						<button class="pull-right btn btn-primary glyphicon glyphicon-edit" type="button" data-toggle="collapse" data-target="#phone_form" aria-expanded="false" aria-controls="phone_form">Edit</button>
						<div class="collapse" id="phone_form">
							<div class="well">
								<form action="" name="edit_phone" id="edit_phone" method="post" class="in-line">
									<div class="form-group">
										<input type="text" placeholder="Phone number..." name="phone_number" value="<?php echo $user->phone_number; ?>">
										<input type="hidden" name="userId" value="<?php echo $session->user_id; ?>">
										<input type="submit" name="phoneSubmit" id="phoneSubmit" value="submit" class="pull-right btn btn-success">
									</div>
								</form>
							</div>
						</div>
					</p>
					<p>
						<label>Email: <?php echo $user->email; ?></label>
						<button class="pull-right btn btn-primary glyphicon glyphicon-edit" type="button" data-toggle="collapse" data-target="#email_form" aria-expanded="false" aria-controls="email_form">Edit</button>
						<div class="collapse" id="email_form">
							<div class="well">
								<form action="" name="edit_email" id="edit_email" method="post" class="in-line">
									<div class="form-group">
										<input type="text" placeholder="Email..." name="email" value="<?php echo $user->email; ?>">
										<input type="hidden" name="userId" value="<?php echo $session->user_id; ?>">
										<input type="submit" name="emailSubmit" id="emailSubmit" value="submit" class="pull-right btn btn-success">
									</div>
								</form>
							</div>
						</div>
					</p>
					<p>
						<label>Residence: <?php echo $user->region; ?></label>
						<button class="pull-right btn btn-primary glyphicon glyphicon-edit" type="button" data-toggle="collapse" data-target="#region_form" aria-expanded="false" aria-controls="region_form">Edit</button>
						<div class="collapse" id="region_form">
							<div class="well">
								<form action="" name="edit_region" id="edit_region" method="post" class="in-line">
									<div class="form-group">
										<input type="text" placeholder="Region..." name="region" value="<?php echo $user->region; ?>">
										<input type="hidden" name="userId" value="<?php echo $session->user_id; ?>">
										<input type="submit" name="regionSubmit" id="regionSubmit" value="submit" class="pull-right btn btn-success">
									</div>
								</form>
							</div>
						</div>
					</p>
					<p>
						<label>Account Status: <?php echo $user->payment; ?></label>
						<a class="pull-right btn btn-success" href="../make_payment.php"> Update </a>
					</p>
					<hr/>
					<p>
						<!--Find if personal details was filled-->
						<?php 
						if (!$personal_dets) {
							echo ('You have not filled personals: <a href="../personal_details_form.php">Fill form</a>');
						}
						?>
					</p>
					<p>
						<!--To add a way edit more of user details--> 
					</p>
				</div><!--End of panel-body-->
			</div><!--end of panel panel-info -->
		</div><!--End of this col-lg-8 col-sm-12-->

		<div class="clo-lg-2 col-sm-12">
			<!--The Ad goes here-->
		</div>
	</div><!--/.row-->
		
		
	<div class="row">
		<div class="col-lg-8 col-sm-12">
			<div class="panel panel-default" id="tabs">
				<div class="panel-head">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#working_history" aria-controls="working_history" role="tab" data-toggle="tab">Working History</a></li>

						<li role="presentation"><a href="#edu_background" aria-controls="edu_background" role="tab" data-toggle="tab">Education Background</a></li>

						<li role="presentation"><a href="#other_qualifications" aria-controls="other_qualifications" role="tab" data-toggle="tab">Other Qualifications</a></li>

						<li role="presentation"><a href="#referees" aria-controls="referees" role="tab" data-toggle="tab">Referees</a></li>

					</ul>
				</div>
					
				<div class="panel-body">
					<!-- Tab panes -->
					<!-- working history section -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="working_history">
							<h3> Working History</h3>
							<!-- succes of fail massege-->
							<div id="wh_status"></div><!-- Show success message-->

							<div id="wh_load_form"></div><!--Load create wh form-->

							<div class="table-responsive">
							<?php if (!empty($work_history)) { 
								echo 'Add a new entry:- <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_working_history" aria-expanded="false" aria-controls="add_working_history"> Add work history </button>';	
							?>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Organisation Name</th>
											<th>Position Held</th>
											<th>Jobs Description</th>
											<th>Time At work</th>
											<th>Edit</th>
										</tr>
									</thead>
									<tbody>
	                                <?php 
	                                foreach ($work_history as $work) :
	                                    $output = "<tr>";
	                                    $output .= "<td>".$work->organization_name."</td>";
	                                    $output .= "<td>".$work->position_held."</td>";
	                                    $output .= "<td>".$work->job_description."</td>";
	                                    $output .= "<td>".$work->start_date." - ".$work->end_date."</td>";
	                                    
	                                    $output .= '<td><p><button class="btn btn-primary" type="button" id="wh_create_new"><i class="glyphicon glyphicon-edit"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-trash"></i></button></p></td>';
	                                    $output .= "</tr>";
	                                    echo $output;
	                                    endforeach;

                                    } else {
	                                    echo 'You have not made entries yet.  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_working_history" aria-expanded="false" aria-controls="add_working_history"> Add work history </button>';
	                                } ?>
									<div class="collapse" id="add_working_history">
										<div class="well">
											<form role="form" action=""  method="post" class="registration-form" id="wh_create_form">
												<div class="form-group">
												<div class="col-xs-6">
													<label for="organization_name" class="control-label"> Organization Name: </label>
													<p>
													<input type="text" name="organization_name" class="form-control" id="organization_name" placeholder="organization name" value="<?php echo isset($orgname) ? $orgname : false; ?>" required>
													<span><?php echo isset($orgname_error) ? $orgname_error : false; ?></span>
													</p>
												</div>


												<div class="col-xs-6">
													<label for="position_held" class="control-label"> Position Held: </label>
													<p>
													<input type="text" name="position_held" class="form-control" id="position_held" placeholder="position held" value="<?php echo isset($posheld) ? $posheld : false; ?>" required>
													<span><?php echo isset($posheld_error) ? $posheld_error : false; ?></span>
													</p>
												</div>
												</div>

												<div class="form-group">
												<div class="col-xs-12">
													<label for="job_description" class="control-label"> Job Description: </label>
													<p>
													<textarea type="text" name="job_description" class="form-control" id="job_description" placeholder="job description" required><?php echo isset($jobdes) ? $jobdes : false; ?></textarea>
													<span><?php echo isset($jobdes_error) ? $jobdes_error : false; ?></span>
													</p>
												</div>
												</div>

												<div class="form-group">
												<div class="col-xs-6">
													<label for="start_date" class="control-label"> Start Date: </label>
													<p>
													<input type="date" class="form-control" name="wh_create_start_date" id="wh_create_start_date">
													<span><?php echo isset($startdate_error) ? $startdate_error : false; ?></span>
													</p>
												</div>


												<div class="col-xs-6">
													<label for="end_date" class="control-label"> End Date: </label>
													<p>
													<input type="date" class="form-control" name="wh_create_end_date" id="wh_create_end_date">
													<span><?php echo isset($enddate_error) ? $enddate_error : false; ?></span>
													</p>
												</div>
												</div>

												<div class="form-group">
												<div class="col-xs-12">
													<label for="reason_for_leaving" class="control-label"> Reason for leaving: </label>
													<p>
													<textarea type="text" name="reason_for_leaving" class="form-control" id="reason_for_leaving" placeholder="reason for leaving..." required><?php echo isset($reasfleaving) ? $reasfleaving: false; ?></textarea>
													<span><?php echo isset($reasfleaving_error) ? $reasfleaving_error : false; ?></span>
													</p>
												</div>
												</div>

												<button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
												<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_working_history" aria-expanded="false" aria-controls="add_working_history"> Cancel </button>
											</form>
										</div>
									</div>
	                                </tbody>
								</table>
							</div><!--End of table responsive class-->
						</div><!--End of working history-->

						<!--Begining of edu backgroung-->
						<div role="tabpanel" class="tab-pane fade" id="edu_background">
							<h4>Education Background</h4>

							<div id="eh_status"></div><!-- Show success message-->
							<div class="table-responsive">
								<?php if (!empty($education_history)) { 
									echo 'Add a new entry:- <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_edu_history" aria-expanded="false" aria-controls="add_edu_history"> Add work history </button>';
								?>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Institution Name</th>
											<th>Course Taken</th>
											<th>Level</th>
											<th>Major</th>
											<th>Time At the institution</th>
											<th>Edit</th>
										</tr>
									</thead>
									<tbody>
		                            <?php  foreach ($education_history as $education) {

										// var_dump($education_history);
		                                $output = "<tr>";
                                		$output .= '<td></td>';
		                                $output .= '<td>'.$education->institution.'</td>';
		                                $output .= '<td>'.$education->course_taken.'</td>';
		                                $output .= '<td>'.$education->level.'</td>';
		                                $output .= '<td>'.$education->major.'</td>';
		                                $output .= '<td>'.$education->start_date.' - '.$education->last_date().'</td>';
		                                $output .= '<td><p><button class="btn btn-primary" type="button" id="eh_edit_form"><i class="glyphicon glyphicon-edit"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-trash"></i></button></p></td>';
		                                $output .= '</tr>';

										echo $output;
		                                }
		                            } else {
		                                echo 'You have no entry yet. <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_edu_history" aria-expanded="false" aria-controls="add_edu_history"> Add edu history </button>';
		                            } ?>
									<div class="collapse" id="add_edu_history">
										<div class="well">
											<form role="form" action="" id="eh_create_form" enctype="multipart/form-data" method="post" class="registration-form">
												<div class="form-group">
												<div class="col-xs-4">
													<label for="institution" class="control-label"> Institution Name: </label>
													<p>
													<input type="text" name="institution" class="form-control" id="institution" placeholder="institution name" value="<?php echo isset($institution) ? $institution : false; ?>" required />
													<span class="error"><?php echo isset($institution_error) ? $institution_error : false; ?></span>
													</p>
												</div>


												<div class="col-xs-4">
													<label for="course_taken" class="control-label"> Course Taken: </label>
													<p>
													<input type="text" name="course_taken" class="form-control" id="course_taken" placeholder="course taken" value="<?php echo isset($cos_taken) ? $cos_taken : false; ?>" required/>
													<span class="error"><?php echo isset($cos_taken_error) ? $cos_taken_error : false; ?></span>
													</p>
												</div>

												<div class="col-xs-4">
													<label for="level" class="control-label"> Level of Study: </label>
													<p>
													<select type="text" name="level" class="form-control" id="level" placeholder="level of study" required>
														<option>Chose level of education</option>
														<option value="class 8">Class 8</option>
														<option value="form four">Form Four</option>
														<option value="diploma">Diploma</option>
														<option value="degree">Degree</option>
														<option value="masters">Masters</option>
														<option value="phd">PHD</option>
													</select>
													<span class="error"><?php echo isset($level_error) ? $level_error : false; ?></span>
													</p>
												</div>
												</div>

												<div class="form-group">
												<div class="col-xs-4">
													<label for="major" class="control-label">Major: </label>
													<p>
													<input type="text" name="major" class="form-control" id="major" placeholder="major" value="<?php echo isset($major) ? $major : false; ?>" required/>
													<span class="error"><?php echo isset($major_error) ? $major_error : false; ?></span>
													</p>
												</div>


												<div class="col-xs-4">
													<label for="start_date" class="control-label"> Start Date: </label>
													<p>
													<input class="form-control" name="eh_create_start_date" id="eh_create_start_date" required>
													<span class="error"><?php echo isset($start_date_error) ? $start_date_error : false; ?></span>
													</p>
												</div>

												<div class="col-xs-4">
													<label for="end_date" class="control-label"> End Date: </label>
													<p>
													<input class="form-control" name="eh_create_end_date" id="eh_create_end_date" required>
													<span class="error"><?php echo isset($end_date_error) ? $end_date_error : false; ?></span>
													</p>
												</div>
												</div>

												<div class="form-group">
													<div class="row">
													<div class="col-sm-6">
														<label for="cert" class="control-label"> Upload certificate: </label>
														<p>
														<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
														<input type="file" name="cert" id="cert">
														<span class="file-custom"></span>
														<span class="error"><?php echo isset($upload_error) ? $upload_error : false; ?></span>
														</p>
													</div>
													
													<div class="col-sm-6">
													<label for="cert_desc" class="control-label"> Discription: </label>
													<p>
														<textarea type="text" name="cert_desc" class="form-control" id="cert_desc" placeholder="Discription 255 charactors..." required ><?php echo isset($cert_desc) ? $cert_desc : false; ?></textarea>
														<span class="error"><?php echo isset($cert_desc_error) ? $cert_desc_error : false; ?></span>
													</p>
													</div>
													</div>
												</div>
												<div class="row">
													<input type="hidden" name="mode" value="eh_create">
													<button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
													<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_edu_history" aria-expanded="false" aria-controls="add_edu_history"> Cancel </button>
												</div>
											</form>
										</div>
									</div>
	                                </tbody>
								</table>
							</div>
						</div>

						<!-- begining of the Other Qualifications -->
						<div role="tabpanel" class="tab-pane fade" id="other_qualifications">
							<h4>Other Qualifications</h4>
							<div id="oq_status"></div><!-- Show success message-->
							<div class="table-responsive">
								<?php if (!empty($other_qualifications)) { 
									echo 'Add a new entry:- <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_other_qualification" aria-expanded="false" aria-controls="add_other_qualification"> Add work history </button>';	
								?>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>
											<th>Description</th>
											<th>Type</th>
											<th>Where Attained</th>
											<th>Edit</th>
										</tr>
									</thead>
									<tbody>
		                                <?php foreach ($other_qualifications as $qualification) {
		                                $output = '<tr>';
		                                $output .= '<td></td>';
		                                $output .= '<td>'.$qualification->name.'</td>';
		                                $output .= '<td>'.$qualification->description.'</td>';
		                                $output .= '<td>'.$qualification->type.'</td>';
		                                $output .= '<td>'.$qualification->where_attained.'</td>';
		                                $output .= '<td><p><button class="btn btn-primary" type="button" id="oq_edit_form"><i class="glyphicon glyphicon-edit"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-trash"></i></button></p></td>';
		                                $output .= '</tr>';

										echo $output;
		                                }
		                                } else {
		                                    echo 'You have no entry yet. <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_other_qualification" aria-expanded="false" aria-controls="add_other_qualification"> Add other qualifications </button>';
		                                } ?>
										<div class="collapse" id="add_other_qualification">
										<div class="well">
											<form role="form" action="" id="oq_create_form"  method="post" class="registration-form">
												<div class="form-group">
												<div class="col-xs-4">
													<label for="name" class="control-label"> Institution Name: </label>
													<p>
													<input type="text" name="name" class="form-control" id="name" placeholder="name" value="" />
													</p>
												</div>


												<div class="col-xs-4">
													<label for="type" class="control-label"> Type: </label>
													<p>
													<input type="text" name="type" class="form-control" id="type" placeholder="type" value="" />
													</p>
												</div>

												<div class="col-xs-4">
													<label for="where_attained" class="control-label"> Course Taken: </label>
													<p>
													<input type="text" name="where_attained" class="form-control" id="where_attained" placeholder="where_attained" value="" />
													</p>
												</div>
												</div> 

												<div class="form-group">
												<div class="col-xs-12">
													<label for="description" class="control-label"> Description: </label>
													<p>
													<textarea type="text" name="description" class="form-control" id="description" placeholder="description" value="" ></textarea>
													</p>
												</div>
												</div> 

												<input type="hidden" name="mode" value="oq_create" />
												<button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
												<a href="profile.php" class="btn btn-default">Cancel</a>
											</form>
										</div>
									</div>
	                                </tbody>
								</table>
							</div>
						</div>

						<!-- Begining of referee -->
						<div role="tabpanel" class="tab-pane fade" id="referees">
							<h4>Referees</h4>
							<div id="re_status"></div><!-- Show success message-->
							<div class="table-responsive">
								<?php  if (!empty($referees)) { 
									echo 'Add a new entry:- <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_referee" aria-expanded="false" aria-controls="add_referee"> Add work history </button>';	
								?>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>
											<th>Title</th>
											<th>organisation name</th>
											<th>Email</th>
											<th>Phone No</th>
											<th>Edit</th>
										</tr>
									</thead>
									<tbody>
		                                <?php foreach ($referees as $referee) {
		                                   $output = '<tr>';
		                                    $output .= '<td></td>';
		                                    $output .= '<td>'.$referee->name.'</td>';
		                                    $output .= '<td>'.$referee->job_title.'</td>';
		                                    $output .= '<td>'.$referee->org_name.'</td>';
		                                    $output .= '<td>'.$referee->email.'</td>';
		                                    $output .= '<td>'.$referee->phone_number.'</td>';
		                                    $output .= '<td><p><button class="btn btn-primary" type="button" id="re_edit_form"><i class="glyphicon glyphicon-edit"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-trash"></i></button></p></td>';
		                                	$output .= '</tr>';
		                                    $output .= '</tr>';

											echo $output;
		                                }
		                                } else {
		                                    echo 'You have no entry yet.  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#add_referee" aria-expanded="false" aria-controls="add_referee"> Add referee </button>';
		                                    }  ?>
										<div class="collapse" id="add_referee">
										<div class="well">
											<form role="form" action="" id="re_create_form"  method="post" class="registration-form">
												<div class="form-group">
												<div class="col-xs-4">
													<label for="name" class="control-label"> Referee's Name: </label>
													<p>
													<input type="text" name="referee_name" class="form-control" id="referee_name" placeholder="Referee name" value="" />
													</p>
												</div>


												<div class="col-xs-4">
													<label for="type" class="control-label"> Organisation name: </label>
													<p>
													<input type="text" name="org_name" class="form-control" id="org_name" placeholder="Organisation name..." value="" />
													</p>
												</div>

												<div class="col-xs-4">
													<label for="type" class="control-label"> Job Title: </label>
													<p>
													<input type="text" name="job_title" class="form-control" id="job_title" placeholder="Job Title..." value="" />
													</p>
												</div>

												<div class="col-xs-6">
													<label for="where_attained" class="control-label"> Phone Number: </label>
													<p>
													<input type="text" name="ref_phone_number" class="form-control" id="ref_phone_number" placeholder="Phone number" value="" />
													</p>
												</div>
												</div> 

												<div class="form-group">
												<div class="col-xs-6">
													<label for="description" class="control-label"> Email: </label>
													<p>
													<input type="text" name="ref_email" class="form-control" id="ref_email" placeholder="Phone number... " value="" />
													</p>
												</div>
												</div> 

												<input type="hidden" name="mode" value="re_create" />
												<button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
												<a href="profile.php" class="btn btn-default">Cancel</a>
											</form>
										</div>
									</div>
	                                </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div><!--/.panel-->
		</div>
	</div><!--/.row-->

	<div class="row">
			
	</div><!--/.row-->
								
	<div class="row">
			
	</div><!--/.row-->
</div>	<!--/.main-->
	

<?php include_layout_template('footer.php'); ?>