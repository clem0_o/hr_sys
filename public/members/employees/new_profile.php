<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$user = User::find_by_id($session->user_id);
//$user = new User();
$work_history = WorkingHistory::find_all_user_id($user->id);
$jobs_history = new WorkingHistory;

$education_history = Education_bg::find_all_user_id($user->id);
$education_history_obj = new Education_bg;

$other_qualifications = OtherQualifications::find_all_user_id($user->id);
//$other_qualifications_obj = new OtherQualifications;
?>
<?php include_layout_template('header_employee.php'); ?>

<?php echo $session->message(); ?>

<div id="page-wrapper">

            <div class="container-fluid">
            
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            New Profile
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-bar-chart-o"></i> Profile
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
            <form action="new_profile.php">
                <!-- Personal details -->
                <div class="well well-sm">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Personal Details</h3>
                    </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="col-xs-6 col-sm-3 placeholder">
                        <img src="img.jpg" alt="My Image" class="img-responsive" >
                        <h4>Profile Photo</h4>
                        <a href="edit_profile.php"><span class="text-muted"> edit photo</span></a>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <p class="lead">
                      <div class="form-group">
                        <label>Name: </label>
                          <input class="form-control" value="<?php echo $user->full_name(); ?>">
                          <p class="help-block">Edit your name here.</p>
                      </div>
                    </p>


                    <p class="lead">
                      <div class="form-group">
                        <label>Phone Number: </label>
                        <input class="form-control" value="<?php echo $user->phone_number; ?>">
                        <p class="help-block">Edit the phone number here, Example 0722000000.</p>
                      </div> 
                    </p>


                            <p class="lead">
                                <div class="form-group">
                                    <label>Email: </label>
                                    <input class="form-control" value="<?php echo $user->email; ?>">
                                <p class="help-block">Edit your email, Example Kjohn@imc.co.ke.</p>
                            </div> 
                            </p>


                            <p class="lead">                            
                            <div class="form-group">
                                <label>Recidence: </label>
                                <input class="form-control" value="<?php echo $user->region; ?>">
                                <p class="help-block">Example block-level help text here.</p>
                            </div> 
                            </p>
                        </div>
                 </div>
                </div>
                <!-- /.row -->
                <!-- Working history -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Working History</h3>
                            </div>
                            <div class="panel-body">
                                <!-- This is were the table will be -->
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Organisation Name</th>
                                      <th>Position Held</th>
                                      <th>Jobs Description</th>
                                      <th>Start Time</th>
                                      <th>End Time</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach ($work_history as $work) {
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <p><a href="#" class="btn btn-link">Edit Work History</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <!-- Education background -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Education Background</h3>
                            </div>
                            <div class="panel-body">
                                <!-- This is were the table will be -->
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Institution Name</th>
                                      <th>Course Taken</th>
                                      <th>Level</th>
                                      <th>Major</th>
                                      <th>Time At the institution</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach ($education_history as $education) {
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <p><a href="#" class="btn btn-link">Edit Education Background</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


                <!-- Other certificates and qualifications -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Other Qualifications</h3>
                            </div>
                            <div class="panel-body">
                                <!-- This is were the table will be -->
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Name</th>
                                      <th>Description</th>
                                      <th>Type</th>
                                      <th>Where Attained</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach ($other_qualifications as $qualifications) {
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                      $output .= "<td><input class=\"form-control\" value=\"\"</td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    } ?>
                                  </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <p><a href="#" class="btn btn-link">Edit Other Qualifications</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    
                        <ol class="breadcrumb">
                        <div class="col-ms-12">
                            <input type="submit" name="submit" value="submit" class="btn btn-primary" />
                            <a  href="profile.php"><button type="button" class="btn btn-lg btn-default">Cancel</button></a>
                        </div>
                        </ol>
                    
                </div>
            </form>
            </div>
            <!-- /.container-fluid -->

<?php include_layout_template('footerer_employee.php'); ?>