<?php
require_once("../../../includes/initialize.php");

//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
  
}
?>
<?php  
$work_history = WorkingHistory::find_by_id($_GET['work_id']);
?>
<?php //form proccessing
$message = "";
//form has been submited
if (isset($_POST['submit'])) {
	$work = new WorkingHistory; //instanciate the employer class

	//read the input data
  //$employer->id = $session->user_id;
  $work->id = $_GET['work_id'];
	$work->user_id = $session->user_id;
	$work->organization_name = trim($_POST['organization_name']);
	$work->position_held = trim($_POST['position_held']);
	$work->job_description = trim($_POST['job_description']);
  $work->start_date = trim($_POST['start_date']);
  $work->end_date = trim($_POST['end_date']);

	//Validate the input data
	if ($work->organization_name=="") { $message = "provide organization name!";  
  } else if($work->position_held=="") { $message = "provide position held !";  
	} else if($work->job_description=="") { $message = "provide job description !";
	} else if($work->start_date=="") { $message = "provide start date !";
  } else if($work->end_date=="") { $message = "provide end date !";
  } else {
		//Enter the data in the database
		if($work->update()){
			$session->message = "Details successfully added!!";
			redirect_to('profile.php');
		}
	}
}
?>


<?php include_layout_template('header_employee.php'); ?>


<div id="page-wrapper">
    <div class="container-fluid">

    <?php echo output_message($message); ?>

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                  Edit Work History
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-bar-chart-o"></i> Profile
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

<div class="row">
<div class="well">

<form role="form" action="edit_work_profile.php?work_id=<?php echo $_GET['work_id']; ?>"  method="post" class="registration-form">
  <div class="form-group">
  <div class="col-xs-6">
    <label for="organization_name" class="control-label"> Organization Name: </label>
    <p>
      <input type="text" name="organization_name" class="form-control" id="organization_name" placeholder="organization name" value="<?php echo $work_history->organization_name; ?>" />
    </p>
  </div>


  <div class="col-xs-6">
    <label for="position_held" class="control-label"> Position Held: </label>
    <p>
      <input type="text" name="position_held" class="form-control" id="position_held" placeholder="position held" value="<?php echo $work_history->position_held; ?>" />
    </p>
  </div>
  </div> 

<div class="form-group">
  <div class="col-xs-12">
    <label for="job_description" class="control-label"> Job Description: </label>
    <p>
      <textarea type="text" name="job_description" class="form-control" id="job_description" placeholder="job description" value="" ><?php echo $work_history->job_description; ?></textarea>
    </p>
  </div>
  </div>

  <div class="form-group">
  <div class="col-xs-6">
    <label for="start_date" class="control-label"> Start Date: </label>
    <p>
      <input type="text" name="start_date" class="form-control" id="start_date" placeholder="start date" value="<?php echo $work_history->start_date; ?>" />
    </p>
  </div>


  <div class="col-xs-6">
    <label for="end_date" class="control-label"> End Date: </label>
    <p>
      <input type="text" name="end_date" class="form-control" id="end_date" placeholder="end date" value="<?php echo $work_history->end_date; ?>" />
    </p>
  </div>
  </div>

  <button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
  <a href="profile.php" class="btn btn-default">Cancel</a>
</form>
</div><!--end of well-->
</div><!-- end of form row -->
</div>
    <!-- /.container-fluid -->

</div><!-- end of the page wrapper -->
<?php include_layout_template('footer_employee.php'); ?>