<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$employee_details = Employee::find_by_user_id($session->user_id);
?>
<?php
if (isset($_POST['submit'])) {
    $employee = new Employee;
    $cv = new Cv;

    // $cv->user_id = $session->user_id;
    // $cv->reff_number = "base_cv";
    // $cv->date_posted    = datetime();
    // $cv->attach_file($_FILES['base_cv']);
    // $cv_id = $cv->save();

    $uid = $session->user_id;
    $dob = trim($_POST['dob']);
    $pob = trim($_POST['pob']);
    $mstatus = trim($_POST['marital_status']);
    $ycomphighschool = trim($_POST['date_completed_high']);
    // $base_cv =  $cv_id;
    $employee_dets = $employee->make($uid, $dob, $pob, $mstatus, $ycomphighschool, $base_cv);

    if ($employee_dets && $employee_dets->create()) {
        $session->message = "Operation successful";
        redirect_to("employee_details.php");
    } else {
        $session->message = "Operation Failed";
    }
}

?>
<?php include_layout_template('header_nav_test.php'); ?>
<div id="page-wrapper">
	<div class="container-fluid">
		<!-- Page Heading -->
        <div class="row">
    <?php echo $session->message(); ?>
            <div class="col-lg-12">
                <h1 class="page-header"><?php// var_dump($_FILES);?>
                    Edit Employee Details
                </h1>
                <ol class="breadcrumb">
                    <li>
                    	<i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-bar-chart-o"></i> Profile
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- The body -->
        <div class="well">
        	<div class="row">
                <div class="col-lg-12">
                   <h3 class="page-header">Employee Details</h3>
                </div>
            </div>
            <div class="row">
                <form role="form" action="add_employee_details.php" method="POST">
                    <div class="col-sm-6">
                        <div class="form-group">
            	           <p class="lead">
                                <label>Date of Birth: </label>
                                <input type="text" name="dob" placeholder="Date of birth..." class="form-control" id="dob" value="">
                            </p>
                        </div>
                        <div class="form-group">
                            <p class="lead">
                                <label>Place of birth: </label>
                                <input type="text" name="pob" placeholder="Place of birth..." class="form-control" id="pob" value="">
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
            	           <p class="lead">
                                <label>Year completed high school: </label>
                                <input type="text" name="date_completed_high" placeholder="year completed high school..." class="form-control" id="date_completed_high" value="">
                            </p>
                        </div>
                        <div class="form-group">
                            <p class="lead">
                                <label>Marital status: </label>
                                <input type="text" name="marital_status" placeholder="marital status..." class="form-control" id="marital_status" value="">
                            </p>
                        </div>
                    </div>

                    <p class="lead">
                        <label>Upload your CV: </label>
                        <p><label class="file">
                            <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                            <input type="FILE" name="base_cv" id="base_cv">
                            <span class="file-custom"></span>
                            </label></p>
                        </p>

                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary" value="Upload">Submit Details</button>
                    </div>
                </form>

        	</div>
        	<div class="row">
                <p><a class="btn btn-link" href="employee_details.php">Cancel</a></p>
            </div>
        </div>
	</div>
</div>
</div>
<?php include_layout_template('footer_nav_test.php'); ?>