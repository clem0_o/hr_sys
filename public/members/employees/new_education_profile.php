<?php
require_once("../../../includes/initialize.php");

//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
  
}
?>
<?php //form proccessing
$message = "";
//form has been submited
if (isset($_POST['submit'])) {
  $userid = $session->user_id;
  $institution = trim($_POST['institution']);
  $cos_taken = trim($_POST['course_taken']);
  $level = trim($_POST['level']);
  $major = trim($_POST['major']);
  $start_date = trim($_POST['start_year'])."-".trim($_POST['start_month'])."-".trim($_POST['start_day']);
  $end_date =  trim($_POST['end_year'])."-".trim($_POST['end_month'])."-".trim($_POST['end_day']);
  $cert_desc = trim($_POST['cert_desc']);


  //Validate the input data

  $error = false;
  if (empty($_FILES['cert'])) {
    $upload_error = "Please fill the Institution.";
    $error = true;
  }

  if (empty($institution)) {
    $institution_error = "Please fill the Institution.";
    $error = true;
  }

  if (empty($cos_taken)) {
    $cos_taken_error = "Please fill the Course taken.";
    $error = true;
  }

  if (empty($level)) {
    $level_error = "Please fill the Level of education.";
    $error = true;
  }

  if (empty($major)) {
    $major_error = "Please fill the Major field.";
    $error = true;
  }

  if (empty($start_date)) {
    $start_date_error = "Please fill the date you started.";
    $error = true;
  }

  if (empty($end_date)) {
    $end_date_error = "Please fill the date you end.";
    $error = true;
  }

  if (empty($cert_desc)) {
    $cert_desc_error = "Please fill the Description field.";
    $error = true;
  }

  if ($error === false) {
    $user = User::find_by_id ($session->user_id);
    $certs = new Certs;

    $certs->id =(INT) 0;
    $certs->user_id = $session->user_id;
    $certs->description = $cert_desc;
    $certs->date_posted = datetime();
    $certs->attach_file($_FILES['cert'], $user->cvname());
    $cert_id = $certs->create();
    
    $edu = Education_bg::make ($userid, $cert_id, $institution, $cos_taken, $level, $major, $start_date, $end_date); 
      
    //Enter the data in the database
    if($edu && $edu->create()){
      $message = "Details successfully added!!";
      redirect_to('profile.php');
    }
  }
}
?>
<?php include_layout_template('header_employee.php'); ?>

<div id="page-wrapper">
    <div class="container-fluid">

    <?php echo output_message($message); ?>

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                  Add Education History
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-bar-chart-o"></i> Profile
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

<div class="row">
<div class="well">

<form role="form" action="new_education_profile.php" enctype="multipart/form-data" method="post" class="registration-form">
  <div class="form-group">
  <div class="col-xs-4">
    <label for="institution" class="control-label"> Institution Name: </label>
    <p>
      <input type="text" name="institution" class="form-control" id="institution" placeholder="institution name" value="<?php echo isset($institution) ? $institution : false; ?>"  />
      <span class="error"><?php echo isset($institution_error) ? $institution_error : false; ?></span>
    </p>
  </div>


  <div class="col-xs-4">
    <label for="course_taken" class="control-label"> Course Taken: </label>
    <p>
      <input type="text" name="course_taken" class="form-control" id="course_taken" placeholder="course taken" value="<?php echo isset($cos_taken) ? $cos_taken : false; ?>" />
      <span class="error"><?php echo isset($cos_taken_error) ? $cos_taken_error : false; ?></span>
    </p>
  </div>

  <div class="col-xs-4">
    <label for="level" class="control-label"> Level of Study: </label>
    <p>
      <select type="text" name="level" class="form-control" id="level" placeholder="level of study">
        <option>Chose level of education</option>
        <option value="class 8">Class 8</option>
        <option value="form four">Form Four</option>
        <option value="diploma">Diploma</option>
        <option value="degree">Degree</option>
        <option value="masters">Masters</option>
        <option value="phd">PHD</option>
      </select>
      <span class="error"><?php echo isset($level_error) ? $level_error : false; ?></span>
    </p>
  </div>
  </div>

  <div class="form-group">
  <div class="col-xs-4">
    <label for="major" class="control-label">Major: </label>
    <p>
      <input type="text" name="major" class="form-control" id="major" placeholder="major" value="<?php echo isset($major) ? $major : false; ?>" />
      <span class="error"><?php echo isset($major_error) ? $major_error : false; ?></span>
    </p>
  </div>


  <div class="col-xs-4">
    <label for="start_date" class="control-label"> Start Date: </label>
    <p>
      <?php echo months_of_year("start_month"); ?>
      <?php echo days_of_the_month("start_day"); ?>
      <?php echo years_long("start_year"); ?>
      <span class="error"><?php echo isset($start_date_error) ? $start_date_error : false; ?></span>
    </p>
  </div>

  <div class="col-xs-4">
    <label for="end_date" class="control-label"> End Date: </label>
    <p>
      <?php echo months_of_year("end_month"); ?>
      <?php echo days_of_the_month("end_day"); ?>
      <?php echo years_long("end_year"); ?>
      
      <span class="error"><?php echo isset($end_date_error) ? $end_date_error : false; ?></span>
    </p>
  </div>
  </div>

  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <label for="cert" class="control-label"> Upload certificate: </label>
        <p>
          <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
          <input type="file" name="cert" id="cert">
          <span class="file-custom"></span>
          <span class="error"><?php echo isset($upload_error) ? $upload_error : false; ?></span>
        </p>
      </div>
      
    <div class="col-sm-6">
      <label for="cert_desc" class="control-label"> Discription: </label>
      <p>
        <textarea type="text" name="cert_desc" class="form-control" id="cert_desc" placeholder="Discription 255 charactors..."  ><?php echo isset($cert_desc) ? $cert_desc : false; ?></textarea>
        <span class="error"><?php echo isset($cert_desc_error) ? $cert_desc_error : false; ?></span>
      </p>
    </div>
    </div>
  </div>
  <div class="row">
    <button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
    <a href="profile.php" class="btn btn-default">Cancel</a>
  </div>
</form>
</div><!--end of well-->
</div><!-- end of form row -->
</div>
    <!-- /.container-fluid -->

<?php include_layout_template('footer_employee.php.php'); ?>