<?php
require_once("../../../includes/initialize.php");

//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
  
}
?>
<?php  
$education_history = Education_bg::find_by_id($_GET['edu_id']);
?>
<?php //form proccessing
$message = "";
//form has been submited
if (isset($_POST['submit'])) {
	$education = new Education_bg; //instanciate the employer class

	//read the input data
  //$employer->id = $session->user_id;
  $education->id = $_GET['edu_id'];
	$education->user_id = $session->user_id;
	$education->institution = trim($_POST['institution']);
	$education->course_taken  = trim($_POST['course_taken']);
	$education->level = trim($_POST['level']);
  $education->major = trim($_POST['major']);
  $education->start_date = trim($_POST['start_date']);
  $education->end_date = trim($_POST['end_date']);

	//Validate the input data
	if ($education->institution=="") { $message = "provide institution!";  
  } else if($education->course_taken=="") { $message = "provide course_taken !";  
	} else if($education->level=="") { $message = "provide level!";
	} else if($education->start_date=="") { $message = "provide start date !";
  } else if($education->end_date=="") { $message = "provide end date !";
  } else {
		//Enter the data in the database
		if($education->update()){
			$session->message = "Details successfully added!!";
			redirect_to('profile.php');
		}
	}
}
?>


<?php include_layout_template('header_employee.php'); ?>


<div id="page-wrapper">
    <div class="container-fluid">

    <?php echo output_message($message); ?>

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                  Edit Work History
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-bar-chart-o"></i> Profile
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

<div class="row">
<div class="well">

<form role="form" action="edit_education_profile.php?edu_id=<?php echo $_GET['edu_id']; ?>"  method="post" class="registration-form">
  <div class="form-group">
  <div class="col-xs-4">
    <label for="institution" class="control-label"> Institution Name: </label>
    <p>
      <input type="text" name="institution" class="form-control" id="institution" placeholder="institution" value="<?php echo $education_history->institution; ?>" />
    </p>
  </div>


  <div class="col-xs-4">
    <label for="course_taken" class="control-label"> Course Taken: </label>
    <p>
      <input type="text" name="course_taken" class="form-control" id="course_taken" placeholder="course taken" value="<?php echo $education_history->course_taken; ?>" />
    </p>
  </div>

  <div class="col-xs-4">
    <label for="level" class="control-label"> Course Taken: </label>
    <p>
      <input type="text" name="level" class="form-control" id="level" placeholder="level" value="<?php echo $education_history->level; ?>" />
    </p>
  </div>
  </div> 

<div class="form-group">
  <div class="col-xs-4">
    <label for="major" class="control-label"> Major: </label>
    <p>
      <input type="text" name="major" class="form-control" id="major" placeholder="major" value="<?php echo $education_history->major; ?>" />
    </p>
  </div>


  <div class="col-xs-4">
    <label for="start_date" class="control-label"> Start Date: </label>
    <p>
      <input type="text" name="start_date" class="form-control" id="start_date" placeholder="start date" value="<?php echo $education_history->start_date; ?>" />
    </p>
  </div>

  <div class="col-xs-4">
    <label for="end_date" class="control-label"> End Date: </label>
    <p>
      <input type="text" name="end_date" class="form-control" id="end_date" placeholder="end date" value="<?php echo $education_history->end_date; ?>" />
    </p>
  </div>
  </div> 


  <button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
  <a href="profile.php" class="btn btn-default">Cancel</a>
</form>
</div><!--end of well-->
</div><!-- end of form row -->
</div>
    <!-- /.container-fluid -->

</div><!-- end of the page wrapper -->
<?php include_layout_template('footer_employee.php'); ?>