<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
($session->usertype=="Employee") ? true : redirect_to("../login.php");
?>

<?php
//This brings in the user details by searching by id
$user = User::find_by_id($session->user_id);
$personal_dets = Personal_details::find_by_user_id($session->user_id);

//Fetch for the work history of the job seeker 
$work_history = WorkingHistory::find_all_user_id($user->id);
$jobs_history = new WorkingHistory;

//Fetch for the education history of the job seeker 
$education_history = Education_bg::find_all_user_id($user->id);
$education_history_obj = new Education_bg;

//Fetch for the other qualification history of the job seeker 
$other_qualifications = OtherQualifications::find_all_user_id($user->id);
?>
<?php include_layout_template('header_employee.php'); ?>
<div id="page-wrapper">
            <div class=" page-inner">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Profile 
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class=""></i> Profile
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <!-- Personal details -->
                <div class="well well-sm">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header text-center">Personal Details</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                            <p class="lead">
                                <label>Name: </label>
                                <?php echo $user->full_name(); ?>
                            </p>
                            <p class="lead">
                            <label>Phone Number: </label>
                                <?php echo $user->phone_number; ?>
                            </p>
                            <p class="lead">
                            <label>Email: </label>
                                <?php echo $user->email; ?>
                            </p>
                            <p class="lead">                            
                            <label>Recidence: </label>
                                <?php echo $user->region; ?>
                            </p>
                            <p class="lead">                            
                            <label>Account status: </label>
                                <?php echo $user->payment; ?>
                            </p>
                            <p class="lead">
                                    <?php empty($personal_dets) ? $link = "<a href=\"../personal_details_form.php\" class=\"text-danger\">Your details are empty, Please add</a>" : $link = "<a href=\"employee_details.php\">View details</a>"; ?>
                                    <?php echo $link; ?> &nbsp;&nbsp;&nbsp;<a href="../make_payment.php">Make Payment</a>
                            </p>
                    </div>
                    <div class="col-ms-6 text-danger">
                        <?php
                                    if (!($user->payment=='Active') && !($personal_dets->pay_status=='Active')) {//Check if the account is active
                                                
                                                if ($payment_checker=User_payment::find_by_user_id($user->id)) {//Check if payment is entered in user_payment table
                                                            $trans_id=$payment_checker->transaction_code;//Read the transaction code
                                                            if (Payment::find_by_trans_id ($trans_id)) {//Check if trans_code is entered in the payment table 
                                                                        
                                                                        User::activate_account($user->id); //Activate if entered
                                                                        Personal_details::activate_account($user->id); //Activate in the personale details db
                                                            } else {
                                                                        echo 'Your account is awaiting Approval to be activated.';
                                                            }
                                                } else {
                                                            echo 'Your Account is not activated, please make payment to have full access of our services. <a href="../make_payment.php">Make Payment</a>';           
                                                }   
                                    }
                        ?>
                    </div>
                 </div>
                </div>
                <!-- /.row -->
                <!-- Working history -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Working History</h3>
                            </div>
                            <div class="panel-body">
                                <!-- This is were the table will be -->
                                <?php if (!empty($work_history)) { ?>
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Organisation Name</th>
                                      <th>Position Held</th>
                                      <th>Jobs Description</th>
                                      <th>Time At work</th>
                                      <th>Edit</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php 
                                  foreach ($work_history as $work) :
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td>".$work->organization_name."</td>";
                                      $output .= "<td>".$work->position_held."</td>";
                                      $output .= "<td>".$work->job_description."</td>";
                                      $output .= "<td>".$work->start_date." - ".$jobs_history->last_date()."</td>";
                                      $output .= "<td><a href=\"edit_work_profile.php?work_id=".$work->id."\" class=\"btn btn-link\">Edit</a></td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    endforeach;
                                    } else {
                                      echo "You have no entry yet";
                                      } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <p><a href="new_work_profile.php" class="btn btn-link">Add Work History</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
<?php if ($session->usertype=="Employee" || $session->usertype=="employee") { ?>
                <!-- Education background -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Education Background</h3>
                            </div>
                            <div class="panel-body">
                                <!-- This is were the table will be -->
                                <?php if (!empty($education_history)) { ?>
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Institution Name</th>
                                      <th>Course Taken</th>
                                      <th>Level</th>
                                      <th>Major</th>
                                      <th>Time At the institution</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach ($education_history as $education) {
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td>".$education->institution."</td>";
                                      $output .= "<td>".$education->course_taken."</td>";
                                      $output .= "<td>".$education->level."</td>";
                                      $output .= "<td>".$education->major."</td>";
                                      $output .= "<td>".$education->start_date." - ".$education_history_obj->last_date()."</td>";
                                      $output .= "<td><a href=\"edit_education_profile.php?edu_id=".$education->id."\" class=\"btn btn-link\">Edit</a></td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    }
                                    } else {
                                      echo "You have no entry yet";
                                      } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <p><a href="new_education_profile.php" class="btn btn-link">Add Education Background</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


                <!-- Other certificates and qualifications -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Other Qualifications</h3>
                            </div>
                            <div class="panel-body">
                                <!-- This is were the table will be -->
                                <?php if (!empty($other_qualifications)) { ?>
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Name</th>
                                      <th>Description</th>
                                      <th>Type</th>
                                      <th>Where Attained</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach ($other_qualifications as $qualification) {
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td>".$qualification->name."</td>";
                                      $output .= "<td>".$qualification->description."</td>";
                                      $output .= "<td>".$qualification->type."</td>";
                                      $output .= "<td>".$qualification->where_attained."</td>";
                                      $output .= "<td><a href=\"edit_otherq_profile.php?otherq_id=".$qualification->id."\" class=\"btn btn-link\">Edit</a></td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    }
                                    } else {
                                      echo "You have no entry yet";
                                      } ?>
                                  </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <p><a href="new_qualification_profile.php" class="btn btn-link">Add Other Qualifications</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php } elseif ($session->usertype=="Casual" || $session->usertype=="casual") { false; } ?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include_layout_template('footer_employee.php'); ?>