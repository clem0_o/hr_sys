<?php
require_once("../../../includes/initialize.php");

function getMode () {
	if (isset ($_POST)) {
		return $_POST['mode'];
	} else {
		return $_GET['mode'];
	}
}

switch (getMode ()) {

	case 'user_update_name':
		$user_id = $_POST['userId'];
		$firstName = trim($_POST['firstName']);
		$lastName = trim($_POST['lastName']);

		//validation

		$user = User::find_by_id ($user_id);
		if ($user) {
			//do somethin
			$user->first_name = $firstName;
			$user->last_name = $lastName;

			if ($user->update()) {
				$output = '<p>Name update was successful.</p>';
				echo $output;
			}
			
		}
	break;

	case 'user_update_phone':
		$user_id = $_POST['userId'];
		$phone_number = $_POST['phone_number'];
		//validation

		$user = User::find_by_id ($user_id);
		if ($user) {
			//do somethin
			$user->phone_number = $phone_number;

			if ($user->update()) {
				$output = '<p>Phone number update was successful.</p>';
				echo $output;
			}
		}
	break;

	case 'user_update_email':
		$user_id = $_POST['userId'];
		$email = $_POST['email'];
		//validation

		$user = User::find_by_id ($user_id);
		if ($user) {
			//do somethin
			$user->email = $email;

			if ($user->update()) {
				$output = '<p>Email update was successful.</p>';
				echo $output;
			}
		}
	break;

	case 'user_update_region':
		$user_id = $_POST['userId'];
		$region = $_POST['region'];
		//validation

		$user = User::find_by_id ($user_id);
		if ($user) {
			//do somethin
			$user->region = $region;

			if ($user->update()) {
				$output = '<p>Region update was successful.</p>';
				echo $output;
			}
		}
	break;

	case 'wh_create':	
		$id =(INT) 0;
		$user_id= $session->user_id;
		$orgname = trim($_POST['organization_name']);
		$posheld = trim($_POST['position_held']);
		$jobdes = trim($_POST['job_description']);
		$startdate = trim($_POST['wh_create_start_date']);
  		$enddate = trim($_POST['wh_create_end_date']);
		$reasfleaving = trim($_POST['reason_for_leaving']);

		//Validate the input data
		$error = false;
		$error_array = [];


		if (empty($orgname)) {
			$error_array['orgname_error'] = 'Please fill the name of the Organization';
    		$error = true;
		} elseif (strlen($orgname)>60) {
			$error_array['orgname_error'] = 'The organisation name is too long';
			$error = true;
		}

		if (empty($posheld)) {
		    $error_array['posheld_error'] = "Please fill the name of the Position Held";
		    $error = true;
		} elseif (strlen($posheld)>60) {
			$error_array['posheld_error'] = 'Position held is too long';
			$error = true;
		}

		if (empty($jobdes)) {
		    $error_array['jobdes_error'] = "Please fill the Job Description";
		    $error = true;
		}

		if (empty($startdate)) {
		    $error_array['startdate_error'] = "Please fill the date when you started the job";
		    $error = true;
		}

		if (empty($enddate)) {
		    $error_array['enddate_error'] = "Please fill the date that you stopped working";
		    $error = true;
		}

		if (empty($reasfleaving)) {
		    $error_array['reasfleaving_error'] = "Please fill the reason for leaving the working";
		    $error = true;
		}

		//make date comptible with mysql
		$startdate = mysql_date ($startdate);
		$enddate = mysql_date ($enddate);

		if ($error === false) {
		    $work = WorkingHistory::make($id, $user_id, $orgname, $posheld, $jobdes, $startdate, $enddate, $reasfleaving) ; 
		    if ($work && $work->create()) {
				echo '<div class="alert alert-success" role="alert">Success <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
		    } else {
				var_dump ($error_array);
			}
		}

	break;

	case 'eh_create':
		$userid = $session->user_id;
		$institution = trim($_POST['institution']);
		$cos_taken = trim($_POST['course_taken']);
		$level = trim($_POST['level']);
		$major = trim($_POST['major']);
		$start_date = trim($_POST['eh_create_start_date']);
		$end_date =  trim($_POST['eh_create_end_date']);
		$cert_desc = trim($_POST['cert_desc']);

		//Validate the input data
		$error = false;
		$error_array = [];

		if (empty($_FILES['cert'])) {
			$error_array['upload_error'] = "Please attach file.";
			$error = true;
		}

		if (empty($institution)) {
			$error_array['institution_error'] = "Please fill the Institution.";
			$error = true;
		}

		if (empty($cos_taken)) {
			$error_array['cos_taken_error'] = "Please fill the Course taken.";
			$error = true;
		}

		if (empty($level)) {
			$error_array['level_error'] = "Please fill the Level of education.";
			$error = true;
		}

		if (empty($major)) {
			$error_array['major_error'] = "Please fill the Major field.";
			$error = true;
		}

		if (empty($start_date)) {
			$error_array['start_date_error'] = "Please fill the date you started.";
			$error = true;
		}

		if (empty($end_date)) {
			$error_array['end_date_error'] = "Please fill the date you end.";
			$error = true;
		}

		if (empty($cert_desc)) {
			$error_array['cert_desc_error'] = "Please fill the Description field.";
			$error = true;
		}

		//make date comptible with mysql
		$start_date = mysql_date ($start_date);
		$end_date = mysql_date ($end_date);

		if ($error === false) {
			$user = User::find_by_id ($userid);
			$certs = new Certs;

			$certs->id =(INT) 0;
			$certs->user_id = $session->user_id;
			$certs->description = $cert_desc;
			$certs->date_posted = datetime();
			$certs->attach_file($_FILES['cert'], $user->cvname());
			$cert_id = $certs->create();
			
			$edu = Education_bg::make ($userid, $cert_id, $institution, $cos_taken, $level, $major, $start_date, $end_date); 
			
			//Enter the data in the database
			if($edu && $edu->create()){
				$message = "Details successfully added!!";
				echo $message;
			}
		} else { 
			var_dump($error_array);
		}
	break;
	
	case 'oq_create':
		$userid = $session->user_id;
		$where_attained = trim($_POST['where_attained']);
		$type = trim($_POST['type']);
		$name = trim($_POST['name']);
		$description = trim($_POST['description']);

		//Validate the input data
		$error = false;
		$error_array = [];

		if (empty($where_attained)) {
			$error_array['institution_error'] = "Please fill Where you attainded the other qualification.";
			$error = true;
		}

		if (empty($type)) {
			$error_array['type_error'] = "Please fill what type of qualification it is.";
			$error = true;
		}

		if (empty($name)) {
			$error_array['name_error'] = "Please fill the name of the qualification.";
			$error = true;
		}

		if (empty($description)) {
			$error_array['description_error'] = "Please fill discription of the other qualification.";
			$error = true;
		}

		if ($error === false) {
			$other_qualification = OtherQualifications::make ($userid, $name, $where_attained, $type, $description); 
			
			//Enter the data in the database
			if($other_qualification && $other_qualification->create()){
				$message = "Details successfully added!!";
				echo $message;
			}
		} else { 
			var_dump($error_array);
		}
	break;
	
	case 're_create':
		$userid = $session->user_id;
		$name = trim($_POST['referee_name']);
		$org_name = trim($_POST['org_name']);
		$job_title = trim($_POST['job_title']);
		$phone_number = trim($_POST['ref_phone_number']);
		$email = trim($_POST['ref_email']);

		//Validate the input data
		$error = false;
		$error_array = [];

		if (empty($name)) {
			$error_array['referee_name_error'] = "Please fill the name of the referee.";
			$error = true;
		}

		if (empty($org_name)) {
			$error_array['org_name_error'] = "Please fill organisation name of the referee.";
			$error = true;
		}

		if (empty($job_title)) {
			$error_array['job_title_error'] = "Please fill the job title of the referee.";
			$error = true;
		}

		if (empty($phone_number)) {
			$error_array['ref_phone_number_error'] = "Please fill the phone number of the referee.";
			$error = true;
		}

		if (empty($email)) {
			$error_array['ref_email_error'] = "Please fill discription of the email of the referee.";
			$error = true;
		}

		if ($error === false) {
			$ref = Referee::make ($userid, $name, $job_title, $org_name, $email, $phone_number ); 
			
			//Enter the data in the database
			if($ref && $ref->create()){
				$message = "Details successfully added!!";
				echo $message;
			}
		} else { 
			var_dump($error_array);
		}
	break;
	
	case 'update':
		$id = trim($_POST['id']);
		$user_id= $session->user_id;
		$orgname = trim($_POST['organization_name']);
		$posheld = trim($_POST['position_held']);
		$jobdes = trim($_POST['job_description']);
		$startdate = trim($_POST['whe_start_date']);
  		$enddate = trim($_POST['whe_end_date']);
		$reasfleaving = trim($_POST['reason_for_leaving']);

		//Validate the input data
		$error = false;
		$error_array = [];


		if (empty($orgname)) {
			$error_array['orgname_error'] = 'Please fill the name of the Organization';
    		$error = true;
		} elseif (strlen($orgname)>60) {
			$error_array['orgname_error'] = 'The organisation name is too long';
			$error = true;
		}

		if (empty($posheld)) {
		    $error_array['posheld_error'] = "Please fill the name of the Position Held";
		    $error = true;
		} elseif (strlen($posheld)>60) {
			$error_array['posheld_error'] = 'Position held is too long';
			$error = true;
		}

		if (empty($jobdes)) {
		    $error_array['jobdes_error'] = "Please fill the Job Description";
		    $error = true;
		}

		if (empty($startdate)) {
		    $error_array['startdate_error'] = "Please fill the date when you started the job";
		    $error = true;
		}

		if (empty($enddate)) {
		    $error_array['enddate_error'] = "Please fill the date that you stopped working";
		    $error = true;
		}

		if (empty($reasfleaving)) {
		    $error_array['reasfleaving_error'] = "Please fill the reason for leaving the working";
		    $error = true;
		}

		if ($error === false) {
		    $work = WorkingHistory::make($id, $user_id, $orgname, $posheld, $jobdes, $startdate, $enddate, $reasfleaving) ; 
		    if ($work && $work->update()) {
		      echo '<div class="alert alert-success" role="alert">Success <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
		    } 
		}

	break;

	default:
		# code...
		break;

	
}


?>