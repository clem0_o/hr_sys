<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$jobs = new Job;
$listed_jobs = Job::find_all();



?>
<?php include_layout_template('header.php'); ?>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<!--<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<ul class="nav menu">
			<li><a href="profile.php"> Profile </a></li>
			<li><a href="applied_jobs.php"> Applied Jobs</a></li>
			<li class="active"><a href="available_jobs.php"> Available Jobs</a></li>
		</ul>	
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row -Bread crumbs-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Available Jobs</h1>
			</div>
		</div><!--/.row -Page header-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="well">
					<form action="" class=" form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Job Key Word">
                    </div>
                    <div class="form-group">
                        <select name="" id="" class="form-control">
                            <option>Select Your City</option>
                            <option selected>New york, CA</option>
                            <option>New york, CA</option>
                            <option>New york, CA</option>
                            <option>New york, CA</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="" id="" class="form-control">
                            <option>Select Your Category</option>
                            <option selected>Graphic Design</option>
                            <option>Web Design</option>
                            <option>App Design</option>
                        </select>
                    </div>
                    <input type="submit" class="btn disabled" value="Search">
                </form>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Jobs</div>
					<div class="panel-body">
						<div class="job-posts table-responsive">
                            <table class="table">
				<?php $counter=1; foreach ($listed_jobs as $jobs_listed) :
				$disable = Application::check_app($session->user_id, $jobs_listed->id);
				?>
                                <tr class="odd wow fadeInUp" data-wow-delay="1s">
                                    <td class="tbl-logo"><i class="glyphicon glyphicon-folder-open"></i></td>
                                    <td class="tbl-title"><h4><?php echo $jobs_listed->job_title; ?><br><span class="job-type"><?php echo $jobs_listed->employment_type; ?></span></h4></td>
                                    <td><p><i class="icon-location"></i><?php echo $jobs_listed->location; ?>, <?php echo $jobs_listed->country; ?></p></td>
                                    <td><p>Ksh/= <?php echo $jobs_listed->min_salary; ?></p></td>
                                    <td class="tbl-apply"><?php echo "<a href=\"apply_job.php?jid=".$jobs_listed->id."\" class=\"btn btn-link\" ".$disable.">apply</a>" ?></td>
                                </tr>
				<?php endforeach; ?>
                            </table>
                        </div>	
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
								
		<div class="row">
			
		</div><!--/.row-->
	</div>	<!--/.main-->

<?php include_layout_template('footer.php'); ?>