<?php
require_once("../../../includes/initialize.php");
?>
<?php
$appied_jobs = Application::find_all_user_id($session->user_id);

$jobs = new Job;
$employer = new Employer;
?>
<?php include_layout_template('header.php'); ?>
	
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<!--<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<ul class="nav menu">
			<li><a href="profile.php"> Profile </a></li>
			<li class="active"><a href="applied_jobs.php"> Applied Jobs</a></li>
			<li><a href="available_jobs.php"> Available Jobs</a></li>
		</ul>	
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"class="glyphicon glyphicon-home"></a></li>
				<li class="active">Jobs aplied </li>
			</ol>
		</div><!--/.row -Bread crumbs-->
		
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Applied Jobs</h1>
			</div>
		</div><!--/.row -Page header-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						Interviews Schedule
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Date</th>
									<th>Venue</th>
									<th>Job Title</th>
									<th>Company</th>
									<th>Confirm Attendance</th>
								</tr>
							</thead>
							
						</table>
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						Status Of jobs Applied
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Job Title</th>
									<th>Application date</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody><?php $counter=1; foreach ($appied_jobs as $job_applied) : ?>
							<?php
							$job_dets = $jobs->find_by_id($job_applied->job_id);
							//$employer_dets = $employer->find_by_id($job_dets->employer_id); 
							?>
								<tr>
								    <td><?php echo $counter;  $counter++; ?></td>
								    <td><?php echo $job_dets->job_title; ?></td>
								   <!-- <td><?php //echo $employer_dets->company_name; ?></td>-->
								    <td><?php echo $job_applied->aplication_date; ?></td>
								    <td><?php echo $job_applied->status; ?></td>
								</tr><?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			
		</div><!--/.row-->
								
		<div class="row">
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	
<?php include_layout_template('footer.php'); ?>