<?php
require_once("../../../includes/initialize.php");
?>
<?php
//This brings in the user details by searching by id
$user = User::find_by_id($session->user_id);
$personal_dets = Personal_details::find_by_user_id($session->user_id);

//Fetch for the work history of the job seeker 
$work_history = WorkingHistory::find_all_user_id($user->id);

//Fetch for the education history of the job seeker 
$education_history = Education_bg::find_all_user_id($user->id);

//Fetch for the other qualification history of the job seeker 
$other_qualifications = OtherQualifications::find_all_user_id($user->id);

//Fetch for the refferees of the job seeker 
//$referees = Referees::find_all_user_id($user->id);

?>

