<?php
require_once("../../../includes/initialize.php");
//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) { 
}
?>
<?php
$per_details = Personal_details::find_by_user_id($session->user_id);
?>
<?php
$message = "";

//form has been submited
if (isset($_POST['submit'])) {
  $uid = $session->user_id;
  $edu_level = trim($_POST['edu_level']);
  $field = trim($_POST['field']);
  $work_exp = trim($_POST['work_exp']);
  $pod = trim($_POST['pod']);
  $dob = trim($_POST['year'])."-".trim($_POST['month'])."-".trim($_POST['day']);
  $id_no = trim($_POST['nid']);
  $nationality = trim($_POST['nationality']);
  $district = trim($_POST['hdistrict']);
  $location = trim($_POST['location']);
  $sub_location = trim($_POST['sub_loc']);
  $village = trim($_POST['village']);
  $marital_status = trim($_POST['mstatus']);
  $spouses_name = trim($_POST['spname']);
  $netx_of_keen = trim($_POST['nok']);
  $knscontact = trim($_POST['keencontact']);
  $acc_status = "Waiting";
  
    $error = false;

  if (empty($_FILES['cv'])) {
    $uploaded_error = 'There is no file attached';
    $error = true;
  }

  if (empty($edu_level)) {
    $edu_level_error = 'Please chooose an education level.';
    $error = true;
  }

  if (empty($field)) {
    $field_error = 'Please chooose your Professional field.';
    $error = true;
  }

  if (empty($work_exp)) {
    $work_exp_error = 'Please enter your Professional expirience.';
    $error = true;
  }

  if (empty($pod)) {
    $pod_error = 'Please enter your Place of birth.';
    $error = true;
  }

  if (empty($dob)) {
    $dob_error = 'Please enter your date of birth.';
    $error = true;
  }

  if (empty($id_no)) {
    $id_no_error = 'Please enter your id number.';
    $error = true;
  }

  if (empty($nationality)) {
    $nationality_error = 'Please enter your Nationality.';
    $error = true;
  }

  if (empty($district)) {
    $district_error = 'Please enter your District.';
    $error = true;
  }

  if (empty($location)) {
    $location_error = 'Please enter your Location.';
    $error = true;
  }

  if (empty($sub_location)) {
    $sub_location_error = 'Please enter your Sub location.';
    $error = true;
  }

  if (empty($village)) {
    $village_error = 'Please enter Where you carrently live.';
    $error = true;
  }

  if (empty($marital_status)) {
    $marital_status_error = 'Please enter your Marital status.';
    $error = true;
  }

  if (empty($spouses_name)) {
    $spouses_name_error = 'Please enter your Spouses name.';
    $error = true;
  }

  if (empty($netx_of_keen)) {
    $netx_of_keen_error = 'Please enter your netx of keen.';
    $error = true;
  }
  
  if ($error === false) {
  
    $cv_update = Cv::find_by_id($per_details->base_cv_id);
  
    $cv_update->user_id = $session->user_id;
    $cv_update->reff_number = "base_cv";
    $cv_update->date_posted = datetime();
    $cv_update->attach_file($_FILES['cv']);
    $cv_id = $cv_update->save();
  
    $add_personal_details = Personal_details::make($uid, $cv_id, $field, $pod, $dob, $id_no, $nationality, $district, $location, $sub_location, $village, $marital_status, $spouses_name, $netx_of_keen, $knscontact);
  
    if ($add_personal_details && $add_personal_details->update()) {
        redirect_to("profile.php");     
      }
    }
  } else {
  $uid = "";
  $cv = "";
  $field = "";
  $padd = "";
  $pcode = "";
  $pod = "";
  $dob = "";
  $id_no = "";
  $nationality = "";
  $district = "";
  $location = "";
  $sub_location = "";
  $village = "";
  $marital_status = "";
  $spouses_name = "";
  $netx_of_keen = "";
  $knscontact = "";
}
?>

<?php include_layout_template('header_employee.php'); ?>
<p>
  Please enter the required details in the form below:
</p>
</div>
</div>
</div>
<div class="row">
  <div class="col-sm-8 col-sm-offset-2 form-box">
    <div class="form-top">
      <div class="form-top-left">
        <h3>Register in our Site</h3>
        <span class="error_message"><?php echo output_message($message); ?></span>
          <p>Please the form below correctly :</p>
      </div>
      <div class="form-top-right">
        <span><img src="../img/ico/logo1.png" /></span>
      </div>
    </div>
  <div class="form-bottom">

<form action="edit_employee_details.php" enctype="multipart/form-data" method="post">



    <div class="input-group col-xs-12">
      <label for="" class="control-label">Education Overview </label>
      <p>
        <div class="col-xs-6">
          <label for="edu_level" class="sr-only">Education Level </label>
          <select name="edu_level" class="form-control" id="edu_level" required>
            <option value="">Choose Education level</option>
            <option value="O level">Form 4</option>
            <option value="Certificate">Certificate</option>
            <option value="Diploma">Diploma</option>
            <option value="Degree">Degree</option>
          </select>
          <span class="error"><?php echo isset($edu_level_error) ? $edu_level_error : false; ?></span>
        </div><!-- End of education level -->
        <div class="col-xs-6">
          <label for="work_exp" class="sr-only">Experience </label>
            <?php echo experience_yrs (); ?>
            <span class="error"><?php echo isset($work_exp_error) ? $work_exp_error : false; ?></span>
        </div><!-- End of experience -->
      </p>
    </div>

  <div class="input-group col-xs-12">
    <label for="" class="control-label">Date of birth </label>
    <p>
      <div class="col-xs-6">
        <input type="text" name="pod" class="form-control" id="first_name" placeholder="Place of birth" value="<?php echo !empty($pod) ? $pod : $per_details->place_of_birth; ?>" />
      </div>
      <div class="col-xs-6">
        <input type="text" name="dob" class="form-control" id="dob" placeholder="Date of birth" value="<?php echo !empty($dob) ? $dob : $per_details->date_of_birth; ?>"  />
      </div>
    </p>
  </div>

  <div class="input-group col-xs-12">
    <label for="" class="control-label">Nationality </label>
    <p>
      <div class="col-xs-6">
        <input type="text" name="nid" class="form-control" id="nid" placeholder="National Id number" value="<?php echo !empty($id_no) ? $id_no : $per_details->id_number; ?>" />
      </div>
      <div class="col-xs-6">
        <input type="text" name="nationality" class="form-control" id="nationality" placeholder="Nationality" value="<?php echo !empty($nationality) ? $nationality : $per_details->nationality; ?>"  />
      </div>
    </p>
  </div>

  <div class="input-group col-xs-12">
    <label for="" class="control-label">Physical Address </label>
    <p>
      <div class="col-xs-6">
        <input type="text" name="hdistrict" class="form-control" id="hdistrict" placeholder="Home district" value="<?php echo !empty($district) ? $district : $per_details->district; ?>" />
      </div>
      <div class="col-xs-6">
        <input type="text" name="location" class="form-control" id="location" placeholder="Location" value="<?php echo !empty($location) ? $location : $per_details->location; ?>"  />
      </div>
    </p>
    <br/><br/>
    <p>
      <div class="col-xs-6">
        <input type="text" name="sub_loc" class="form-control" id="sub_loc" placeholder="Sub-location" value="<?php echo !empty($sub_location) ? $sub_location : $per_details->sub_location; ?>" />
      </div>
      <div class="col-xs-6">
        <input type="text" name="village" class="form-control" id="village" placeholder="Village" value="<?php echo !empty($village) ? $village : $per_details->village; ?>"  />
      </div>
    </p>
  </div>

  <div class="input-group col-xs-12">
    <label for="" class="control-label">Marital Status: </label>
    <p>
      <div class="col-xs-6">
        <input type="text" name="mstatus" class="form-control" id="mstatus" placeholder="marital status..." value="<?php echo !empty($marital_status) ? $marital_status : $per_details->marital_status; ?>" />
      </div>
      <div class="col-xs-6">
        <input type="text" name="spname" class="form-control" id="spname" placeholder="Spouse's name..." value="<?php echo !empty($spouses_name) ? $spouses_name : $per_details->spouses_name; ?>" />
      </div>
    </p>
  </div>

  <div class="input-group col-xs-12">
    <label for="" class="control-label">Next of Keen: </label>
    <p>
      <div class="col-xs-6">
        <input type="text" name="nok" class="form-control" id="nok" placeholder="marital Next of keen..." value="<?php echo !empty($netx_of_keen) ? $netx_of_keen : $per_details->netx_of_keen; ?>" />
      </div>
      <div class="col-xs-6">
        <input type="text" name="keencontact" class="form-control" id="keencontact" placeholder="Next of keen contact..." value="<?php echo !empty($knscontact) ? $knscontact : $per_details->keens_contact; ?>" />
      </div>
    </p>
  </div>

  <div class="input-group col-xs-12">
    <label for="" class="control-label">Upload your CV: </label>
    <p>
    <div class="col-xs-6">
      <label>Upload your CV: </label>
      <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
      <input type="file" name="cv" id="cv">
      <span class="file-custom"></span>
    </div>
    <div class="col-xs-6">
      <label>field interested: </label>
      <?php echo list_industries_fields(); ?>
    </div>
    </p>
  </div>

  <br/><br/>

  <button type="submit" name="submit" class="btn btn-lg btn-primary">Edit!</button>&nbsp;&nbsp;&nbsp;
    <br/>
  <a class="btn btn-lg btn-default" href="index.php">
    <i class=""></i> Cancel
  </a>
</form>

</div>
</div>
</div>
<?php include_layout_template('footer_employee.php'); ?> 