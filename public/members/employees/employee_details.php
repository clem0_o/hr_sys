<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$employee_details = Employee::find_by_user_id($session->user_id);
$personal_dets = Personal_details::find_by_user_id($session->user_id);
?>
<?php include_layout_template('header_employee.php'); ?>
<?php echo $session->message(); ?>
<div id="page-wrapper">
	<div class="container-fluid">
		<!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Employee Personal Details
                </h1>
                <ol class="breadcrumb">
                    <li>
                    	<i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-bar-chart-o"></i> Profile
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- The body -->
        <div class="well">
        	<div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Employee Personal Details</h3>
                </div>
            </div>
            <?php if (!empty($personal_dets)) { ?>
                <div class="col-lg-8">
                    <p class="lead">
                        Place of birth: 
                        <?php echo "<strong>".$personal_dets->place_of_birth."</strong> &nbsp; &nbsp; The date of birth:  <strong>".$personal_dets->date_of_birth."</strong>"; ?>
                    </p>
                    <p class="lead">
                        Nationality: 
                        <?php echo "<strong>".$personal_dets->id_number."</strong> &nbsp; &nbsp;  Nationality:  <strong>".$personal_dets->nationality."</strong>"; ?>
                    </p>
                    <p class="lead">                            
                        Recidence: 
                        <?php echo "<strong>".$personal_dets->district."</strong> &nbsp; &nbsp;  The location: <strong>".$personal_dets->location."</strong>"; ?>
                    </p>
                    <p class="lead">                            
                        Next of keen: 
                        <?php echo "<strong>".$personal_dets->netx_of_keen."</strong> &nbsp; &nbsp;  Keens Contact: <strong>".$personal_dets->keens_contact."</strong>"; ?>
                    </p>
                </div> 
            <h2><?php } else { echo "Please enter personal details";} ?></h2>
        	<div class="row">
            <br/><br/>
            <?php //This will choose wether to show Add details or Edit details ?>
            <?php if (empty($personal_dets)) { echo "<p>Add details: <a class=\"btn btn-link\" href=\"add_employee_details.php\">Add Details</a></p>"; } else { echo "<p> Edit details: <a class=\"btn btn-link\" href=\"edit_employee_details.php\">Edit Details</a></p>";} ?>
            </div>
        </div>
	</div>
</div>
<?php include_layout_template('footer_employee.php'); ?>