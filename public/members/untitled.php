<?php include_layout_template('header_employers.php'); ?>

    <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Jobs Posted
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Tables
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <h2>List of active posted jobs</h2>
                        <?php if (!empty($job_list)) { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Ref Number</th>
                                        <th>Job Title</th>
                                        <th>Remaining Time</th>
                                        <th>No of Apps</th>
                                    </tr>
                                </thead>
                                <tbody><?php $counter=1; foreach ($job_list as $jobs_listed) : ?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo $jobs_listed->reff_number; ?></td>
                                        <td><?php echo $jobs_listed->job_title; ?></td>
                                        <td><?php echo remaining_time($jobs->end_date); ?></td>
                                        <td><a href="jobs_applications.php"><?php echo $applications->number_of_app($jobs_listed->reff_number); ?></a></td>
                                    </tr><?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } else { echo "There is no data entered";} ?>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <a href="add_job.php">new job</a>
                        </ol>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include_layout_template('footer_employers.php'); ?>