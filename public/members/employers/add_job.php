<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
if (isset($_POST['submit'])) {
    $user  = new User;
    $user->find_by_id($session->user_id);
    $employer = Employer::find_by_user_id($session->user_id);
    $job = new Job;

    $job->reff_number = trim($_POST['reff_number']);
    $job->employer_id = ($employer->id);
    $job->job_title = trim($_POST['job_title']);
    $job->job_description_brief = trim($_POST['job_description_brief']);
    $job->job_description_detailed = trim($_POST['job_description_detailed']);
    $job->category = trim($_POST['category']);
    $job->location = trim($_POST['location']);
    $job->employment_type = trim($_POST['employment_type']);
    $job->requirements = trim($_POST['requirements']);
    $job->qualifications = trim($_POST['qualifications']);
    $job->additional_info = trim($_POST['additional_info']);
    $job->max_salary = trim($_POST['max_salary']);
    $job->min_salary = trim($_POST['min_salary']);
    $job->currency = trim($_POST['currency']);
    $job->start_date = trim($_POST['start_date']);
    $job->end_date = trim($_POST['end_date']);
    //$job->visible = (!empty(trim($_POST['$visible']))) ? trim($_POST['$visible']) : null;

    $job->user_id = $session->user_id;
    $job->posted_date = datetime();

    if ($job->save()) {
        //successs message should come here
        redirect_to('jobs_posted.php');
    }
    //failure message should come here

} else {
    $reff_number = "";
    $job_title = "";
    $job_description_brief = "";
    $job_description_detailed = "";
    $category = "";
    $location = "";
    $employment_type = "";
    $requirements = "";
    $qualifications = "";
    $additional_info = "";
    $max_salary = "";
    $min_salary = "";
    $currency = "";
    $posted_date = "";
    $start_date = "";
    $end_date = "";
    $visible = "";
}


//var_dump($_POST);

?>
<?php include_layout_template('header_employers.php'); ?>

	<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add a new job
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Tables
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
<!-- the body goes here -->

                <div class="row">
                    <div class="col-ms-6">
                        <div class="well">
                          <form role="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" class="registration-form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="reff_number">Reff Number</label>
                                            <input type="text" name="reff_number" class="form-control" id="reff_number" placeholder="Enter Reff Number">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_title">Job Title</label>
                                            <input type="text" name="job_title" class="form-control" id="job_title" placeholder="Enter Job Title">
                                        </div>
                                    </div>                                      
                                </div>
                                <!-- End of the row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_description_brief">Brief Job Description</label>
                                            <textarea class="form-control" name="job_description_brief" id="job_description_brief" rows="5" placeholder="Enter Brief Job Description"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_description_detailed">Detailed Job Description</label>
                                            <textarea class="form-control" name="job_description_detailed" id="job_description_detailed" rows="5" placeholder="Enter Detailed Job Description"></textarea>
                                        </div>
                                    </div>                                 
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <input type="text" name="category" class="form-control" id="category" placeholder="Enter Job Category">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="location">Location</label>
                                            <input type="text" name="location" class="form-control" id="location" placeholder="Enter Job Location">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="employment_type">Type</label>
                                            <input type="text" name="employment_type" class="form-control" id="employment_type" placeholder="Enter Job Type">
                                        </div>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="requirements">Job Requiements</label>
                                            <textarea class="form-control" name="requirements" id="requirements" rows="5" placeholder="Enter Detailed Job Requiements"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="qualifications">Job Qualification</label>
                                            <textarea class="form-control" name="qualifications" id="qualifications" rows="5" placeholder="Enter Detailed Job Qualification"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="">Addional Infomation</label>
                                            <textarea class="form-control" name="additional_info" rows="5" id="additional_info" placeholder="Enter Detailed Addional Infomation"></textarea>
                                        </div>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->


                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="max_salary">Max Salary</label>
                                            <input type="text" name="max_salary" class="form-control" id="max_salary" placeholder="Enter Max Salary">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="min_salary">Min Salary</label>
                                            <input type="text" name="min_salary" class="form-control" id="min_salary" placeholder="Enter Min Salary">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="currency">Currency</label>
                                            <input type="text" name="currency" class="form-control" id="currency" placeholder="Enter Payment Currency">
                                        </div>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="start_date">Start Date</label>
                                            <input type="date" name="start_date" class="form-control" id="start_date" placeholder="Enter start date: YYYY-mm-dd">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="end_date">End Date</label>
                                            <input type="date" name="end_date" class="form-control" id="end_date" placeholder="Enter end date: YYYY-mm-dd">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                    <fieldset disabled>
                                        <div class="form-group">
                                             <label for="visible">Visibility</label>
                                             <input type="text" name="visible" class="form-control" id="visible" placeholder="Enter visibility, 1 or 0">
                                        </div>
                                    </fieldset>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->
                                
                              <button type="submit" name="submit" class="btn btn-default">Submit</button>
                            </form> 
                        </div>
                    </div>
                </div>
                <!-- /.row -->


                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <a href="add_job.php">new job</a>
                        </ol>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include_layout_template('footer_employers.php'); ?>