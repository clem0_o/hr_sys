<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
isset($_GET['jid']) ? $jid = $_GET['jid'] : $jid = 0;
if (!$jid==0) {
	$jobs_posted = Job::find_by_id($_GET['jid']);
	$applications = Application::find_app_by_job_id ($_GET['jid']);
}




$employer = new Employer;

?>
<?php include_layout_template('header.php'); ?>

	
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<!--<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<ul class="nav menu">
			<li class="active"><a href="profile.php"> Profile </a></li>
			<li><a href="jobs_posted.php"> Posted Jobs</a></li>
			<li><a href="#"> Applications Jobs</a></li>
		</ul>	
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"class="glyphicon glyphicon-home"></a></li>
				<li class="active">Applications </li>
			</ol>
		</div><!--/.row -Bread crumbs-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Applications</h1>
			</div>
		</div><!--/.row -Page header-->
		<?php if (!$jid==0) { ?>
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<?php echo isset($jobs_posted->job_title) ? 'List of applications to <strong>'.$jobs_posted->job_title.'</strong>' : 'There is no application.' ;?> 
					</div>
					<div class="panel-body">
                        <?php
						if (!empty($applications)) { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Phone number</th>
                                        <th>Email</th>
                                        <th>CV</th>
                                    </tr>
                                </thead>
                                <tbody><?php
					$counter=1; foreach ($applications as $application) :
					//if (time()>=strtotime($jobs_listed->end_date)) :
					$user = User::find_by_id($application->user_id);
					$cv = Cv::find_by_id($application->cv_id);
					?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo $user->full_name(); ?></td>
                                        <td><?php echo $user->phone_number; ?></td>
                                        <td><?php echo $user->email; ?></td>
                                        <td><a href="../employees/employee_files/cv/<?php echo $cv->file_name; ?>"> View cv</a></td>
                                    </tr><?php
				//endif;
				endforeach;
				?>
                                </tbody>
                            </table>
                        </div>
                        <?php } else { echo "There is no data entered";} ?>	
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		<?php } else { ?>
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<?php echo isset($jobs_posted->job_title) ? 'List of applications to <strong>'.$jobs_posted->job_title.'</strong>' : 'There is no application.' ;?> 
					</div>
					<div class="panel-body">
                        <?php
						if (!empty($applications)) { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Phone number</th>
                                        <th>Email</th>
                                        <th>CV</th>
                                    </tr>
                                </thead>
                                <tbody><?php
					$counter=1; foreach ($applications as $application) :
					//if (time()>=strtotime($jobs_listed->end_date)) :
					$user = User::find_by_id($application->user_id);
					$cv = Cv::find_by_id($application->cv_id);
					?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo $user->full_name(); ?></td>
                                        <td><?php echo $user->phone_number; ?></td>
                                        <td><?php echo $user->email; ?></td>
                                        <td><a href="../employees/employee_files/cv/<?php echo $cv->file_name; ?>"> View cv</a></td>
                                    </tr><?php
				//endif;
				endforeach;
				?>
                                </tbody>
                            </table>
                        </div>
                        <?php } else { echo "There is no data entered";} ?>	
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		<div class="row">
			
		</div><!--/.row-->
		<?php } ?>	
		<div class="row">
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	

<?php include_layout_template('footer.php'); ?>