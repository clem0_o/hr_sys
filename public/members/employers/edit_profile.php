<?php
require_once("../../../includes/initialize.php");

//include_layout_template('admin_header.php');

//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
  
}
?>
<?php
$user = User::find_by_id($session->user_id);
$employer=Employer::find_by_user_id($session->user_id);
?>
<?php //form proccessing
$message = "";
//form has been submited
if (isset($_POST['submit'])) {
	$employer = new Employer; //instanciate the employer class

	//read the input data
  $employer->id = $employer->id;
	$employer->user_id = $session->user_id;
	$employer->company_name = trim($_POST['company_name']);
	$employer->location = trim($_POST['location']);
	$employer->industry = trim($_POST['industry']);

	//Validate the input data
	if ($employer->company_name=="") { $message = "provide company name !";  
  } else if($employer->location=="") { $message = "provide location !";  
	} else if($employer->industry=="") { $message = "provide industry !";  
	} else {
		//Enter the data in the database
		if($employer->update()){
			$session->message = "Details successfully added!!";
			redirect_to('profile.php');
		}
	}
}
?>

<?php include_layout_template('header_employers.php'); ?>

<?php echo output_message($message); ?>
<div id="page-wrapper">
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                  Edit Company Profile
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-bar-chart-o"></i> Profile
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

<div class="row">
<div class="well">

<form role="form" action="edit_profile.php"  method="post" class="registration-form">
  <div class="form-group">
  <div class="row">
    <label for="company_name" class="control-label">Company Name: </label>
    <p>
      <div class="col-xs-6">
      
        <input type="text" name="company_name" class="form-control" id="company_name" placeholder="company name" value="<?php echo $employer->company_name; ?>" />

      </div>
    </p>
    </div>
  </div>


  <div class="form-group">
  <div class="row">
    <label for="location" class="control-label">Location: </label>
    <p>
      <div class="col-xs-6">
        <input type="text" name="location" class="form-control" id="location" placeholder="location..." value="<?php echo $employer->location; ?>" />
      </div>
    </p>
    </div>
  </div>

  <div class="form-group">
  <div class="row">
    <label for="industry" class="control-label">Indurstry: </label>
    <p>
      <div class="col-sm-4">
        <input type="text" name="industry" class="form-control" id="industry" placeholder="industry..." value="<?php echo $employer->industry; ?>" />
      </div>
    </p>
    </div>
  </div>

  <button type="submit" name="submit" class="btn btn-primary">Add Details!</button>
  <a href="profile.php" class="btn btn-default">Cancel</a>
</form>
</div><!--end of well-->
</div><!-- end of form row -->
</div>
    <!-- /.container-fluid -->

</div><!-- end of the page wrapper -->