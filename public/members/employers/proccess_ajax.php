<?php
require_once("../../../includes/initialize.php");

function getMode () {
	if (isset ($_POST)) {
		return $_POST['mode'];
	} else {
		return $_GET['mode'];
	}
}

switch (getMode ()) {
    case 'employer_update_name':
		$user_id = $_POST['userId'];
		$firstName = trim($_POST['firstName']);
		$lastName = trim($_POST['lastName']);

		//validation

		$user = User::find_by_id ($user_id);
		if ($user) {
			//do somethin
			$user->first_name = $firstName;
			$user->last_name = $lastName;

			if ($user->update()) {
				$output = '<p>Name update was successful.</p>';
				echo $output;
			}
			
		}
	break;

	case 'employer_update_phone':
		$user_id = $_POST['userId'];
		$phone_number = $_POST['phone_number'];
		//validation

		$user = User::find_by_id ($user_id);
		if ($user) {
			//do somethin
			$user->phone_number = $phone_number;

			if ($user->update()) {
				$output = '<p>Phone number update was successful.</p>';
				echo $output;
			}
		}
	break;

	case 'employer_update_email':
		$user_id = $_POST['userId'];
		$email = $_POST['email'];
		//validation

		$user = User::find_by_id ($user_id);
		if ($user) {
			//do somethin
			$user->email = $email;

			if ($user->update()) {
				$output = '<p>Email update was successful.</p>';
				echo $output;
			}
		}
	break;

	case 'employer_update_region':
		$user_id = $_POST['userId'];
		$region = $_POST['region'];
		//validation

		$user = User::find_by_id ($user_id);
		if ($user) {
			//do somethin
			$user->region = $region;

			if ($user->update()) {
				$output = '<p>Region update was successful.</p>';
				echo $output;
			}
		}
	break;

    case 'new_job':
        $id = (INT) 0;
        $uid = $session->user_id;
        $jtitle = trim($_POST['job_title']);
        $j_des_brief = trim($_POST['job_description_brief']);
        $j_des_det = trim($_POST['job_description_detailed']);
        $category = trim($_POST['category']);
        $location = trim($_POST['location']);
        $country = trim($_POST['country']);
        $employment_type = trim($_POST['employment_type']);
        $requirements = trim($_POST['requirements']);
        $qualifications = trim($_POST['qualifications']);
        $additional_info = trim($_POST['additional_info']);
        $max_salary = trim($_POST['max_salary']);
        $min_salary = trim($_POST['min_salary']);
        $currency = trim($_POST['currency']);
        $start_date = trim($_POST['job_start_date']);
        $end_date = trim($_POST['job_end_date']);
        $image = 'Not set yet.';

        //Validations

		//preparation for db
		$start_date = mysql_date ($start_date);
		$end_date = mysql_date ($end_date);

        $job = Job::make ($id, $uid, $jtitle, $j_des_brief, $j_des_det, $category, $location, $country, $employment_type, $requirements, $qualifications, $additional_info, $max_salary, $min_salary, $currency, $start_date, $end_date, $image);

		if ($job && $job->create()) {
			echo '<div class="alert alert-success" role="alert">Success <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
		}

    break;
}
?>