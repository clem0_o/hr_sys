
<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
// This brings in the user details
$user = User::find_by_id($_GET["uc_id"]);

//
$empl = Employer::find_by_user_id($_GET["uc_id"]);

$work_history = WorkingHistory::find_all_user_id($_GET["uc_id"]);

$education_history = Education_bg::find_all_user_id($_GET["uc_id"]);

$other_qualifications = OtherQualifications::find_all_user_id($_GET["uc_id"]);
?>
<?php include_layout_template('header.php'); ?>

    
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <!--<form role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
        </form>-->
        <ul class="nav menu">
            <li class="active"><a href="profile.php"> Profile </a></li>
            <li><a href="cv.php"> Cvs</a></li>
            <li><a href="#"> Applications</a></li>
            <li><a href="#"> Payment</a></li>
        </ul>   
    </div><!--/.sidebar-->
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"class="glyphicon glyphicon-home"></a></li>
                <li class="active">Profile (<?php echo $user->full_name(); ?>)</li>
            </ol>
        </div><!--/.row -Bread crumbs-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Profile</h1>
            </div>
        </div><!--/.row -Page header-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Personal Details
                    </div>
                    <div class="panel-body">
                        <p><label>Name: <?php echo $user->full_name(); ?></label></p>
                        <p><label>Phone No: <?php echo $user->phone_number; ?> </label></p>
                        <p><label>Email: <?php echo $user->email; ?></label></p>
                        <p><label>Residence: <?php echo $user->region; ?></label></p>
                        <p><label>Account Status: <?php echo $user->payment; ?></label></p>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Work information
                    </div>
                    <div class="panel-body">
                          <?php if (!empty($work_history)) { foreach ($work_history as $w_history) { ?>
                        <p class="lead">
                            <label>Organization Name: </label>
                            <?php echo !empty($w_history->organization_name) ? $w_history->organization_name : "There are no entries"; ?>
                        </p>
                        <p class="lead">
                            <label>Position Held: </label>
                            <?php echo !empty($w_history->position_held) ? $w_history->position_held: "There are no entries"; ?>
                        </p>
                        <p class="lead">
                            <label>Job Description: </label>
                            <?php echo !empty($w_history->job_description) ? $w_history->job_description : "There are no entries"; ?>
                        </p>
                        <p class="lead">
                            <label>Duration: </label>
                            <?php echo !empty($w_history->start_date) ? ($w_history->start_date." to someday") : false; ?>
                        </p>
                    <?php } } else {
                        echo "There are no entries!!!";
                        } ?>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Education Info
                    </div>
                    <div class="panel-body">
                        <?php if (!empty($education_history)) { foreach ($education_history as $edu_history) { ?>
                        <p class="lead">
                            <label>Institution Name: </label>
                            <?php echo !empty($edu_history->institution) ? $edu_history->institution : "There are no entries"; ?>
                        </p>
                        <p class="lead">
                            <label>Course Taken: </label>
                            <?php echo !empty($edu_history->course_taken) ? $edu_history->course_taken: "There are no entries"; ?>
                        </p>
                        <p class="lead">
                            <label>Level: </label>
                            <?php echo !empty($edu_history->level) ? $edu_history->level : "There are no entries"; ?>
                        </p>
                        <p class="lead">
                            <label>Duration: </label>
                            <?php echo !empty($edu_history->start_date) ? ($edu_history->start_date." to someday") : false; ?>
                        </p>

                    <?php } } else {
                        echo "There are no entries!!!";
                        } ?>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
            
        <div class="row">
            
        </div><!--/.row-->
                                
        <div class="row">
            
        </div><!--/.row-->
    </div>  <!--/.main-->
    

<?php include_layout_template('footer.php'); ?>