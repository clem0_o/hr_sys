<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
//$users = User::find_all();
//1. the current page number ($current_page)
$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

//2. records per page ($per_page)
$per_page = 10;

//3. total record count ($total_count)
$total_count = User::count_all();

//find all

$pagination = new Pagination($page, $per_page, $total_count);

//Instead of finding all records, just find the records for this page
$sql = "SELECT * FROM users ";
$sql .= "ORDER BY id desc ";
$sql .= "LIMIT {$per_page} ";
$sql .= "OFFSET {$pagination->offset()}";
$users = User::find_by_sql($sql);

?>
<?php include_layout_template('header.php'); ?>

    
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <!--<form role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
        </form>-->
        <ul class="nav menu">
            <li><a href="profile.php"> Profile </a></li>
            <li><a href="cv.php"> Cvs</a></li>
            <li><a href="jobs.php"> Jobs </a></li>
            <li class="active"><a href="admin_payments.php"> Payment</a></li>
            <li><a href="all_applications.php"> Applications</a></li>
        </ul>   
    </div><!--/.sidebar-->
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"class="glyphicon glyphicon-home"></a></li>
                <li class="active"> Payment </li>
            </ol>
        </div><!--/.row -Bread crumbs-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Payment</h1>
            </div>
        </div><!--/.row -Page header-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Personal Details
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>User Name</th>
                                        <th>Full Name</th>
                                        <th>Type</th>
                                        <th>Payment</th>
                                        <th>Activate User</th>
                                    </tr>
                                </thead>
                                <tbody><?php $counter=1; foreach ($users as $user) : ?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo $user->username; ?></td>
                                        <td><?php echo $user->first_name." ".$user->last_name." : ".$user->phone_number; ?></td>
                                        <td><?php echo $user->usertype; ?></td>
                                        <td><?php echo $user->payment; ?></td>
                                        <td><?php echo "<a href=\"add_payment.php?user=".$user->id."\" class=\"btn btn-link\">Activate</a>" ?></td>
                                    </tr><?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
			<?php
				if ($pagination->total_pages() > 1) {
					
					if ($pagination->has_previous_page()) {
						echo " <a href=\"admin_payments.php?page=";
						echo $pagination->previous_page();
						echo "\">&laquo; Previous</a> ";
					}
					
					for ($i=1; $i <= $pagination->total_pages(); $i++) {
						if ($i == $page) {
							echo " <span class=\"selected\">{$i}</span> ";
						} else {
							echo " <a href=\"admin_payments.php?page={$i}\">{$i}</a> ";
						}
					}
					
					if ($pagination->has_next_page()) {
						echo " <a href=\"admin_payments.php?page=";
						echo $pagination->next_page();
						echo "\">Next &raquo;</a>";
					}
				}
				
			?>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
            
        <div class="row">
            
        </div><!--/.row-->
                                
        <div class="row">
            
        </div><!--/.row-->
    </div>  <!--/.main-->
    
