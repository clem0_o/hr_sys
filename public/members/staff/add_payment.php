<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$message = "";

//form has been submitted
if (isset($_POST['submit'])) {

	//read the input data
	$uid = $session->user_id;
	$transaction_code = trim($_POST['transaction_code']);
	$amount = trim($_POST['amount']);
	$payee_name = trim($_POST['payee_name']);
	$date_posted = datetime();
	$date_paid = trim($_POST['date_paid']);


    $error = false;

    if (empty($transaction_code)) {
      $transaction_code_error = 'transaction code is empty, Please fill';
      $error = true;
    }

    if (empty($amount)) {
      $amount_error = 'Amount is empty, Please fill';
      $error = true;
    }

    if (empty($date_posted)) {
      $date_posted_error = 'Date posted is empty, Please fill';
      $error = true;
    }

    if (empty($date_paid)) {
      $date_paid_error = 'Date paid is empty, Please fill';
      $error = true;
    }

    if ($error === false) {
    	$exists = Payment::find_by_trans_id($transaction_code);

    	if (empty($exists)) {
			$payment = Payment::make($uid, $transaction_code, $amount, $payee_name, $date_posted, $date_paid);

			if ($payment && $payment->create()) {
				
				$paid_id = User_payment::check_payment ($transaction_code, $payee_name);
				if (!empty($paid_id)) {
					if (User::activate_account($paid_id->user_id) && Personal_details::activate_account($paid_id->user_id)) {
						$session->message = "Account was successively activated";
						redirect_to("admin_payments.php");
					} else {
						$message = "Activation failed, The update has issues please check!!!";
					}
				} else {
					$message = "The transaction_code entered don't have a much";
					redirect_to("admin_payments.php");
				}
			}
		} else {
			$message = "The contraction Code is already Entered";
		}
	}
} else {
	$uid = "";
	$transaction_code = "";
	$amount = "";
	$payee_name = "";
	$date_posted = "";
	$date_paid = "";
}
?>
<?php include_layout_template('header_admin.php'); ?>
<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Add Users Payment
                </h1>
                <ol class="breadcrumb">
	                <li>
	                	<i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
	                </li>
	                <li class="active">
	                    <i class="fa fa-table"></i> Tables
	                </li>
                </ol>
            </div>
		</div>
        <!-- /.row -->

        <!-- Personal details -->
        <div class="well well-sm">
        	<div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Fill in the form</h3>
                    <h4 style="color: red;"><?php echo isset($message) ? $message : false; ?></h4>
                </div>
            </div>
            <!-- end of row-->
            <div class="row">
            	<div class="col-sm-12">
            		<div class="panel panel-primary">
            		
            			<div class="panel panel-primary">
	                    	<div class="panel-heading">
		                        <div class="row">
		                            <div class="col-xs-3">
		                                <i class="fa fa-comments fa-5x"></i>
		                            </div>
		                            <div class="col-xs-9 text-right">
		                                <div class="huge"></div>
		                                <div>Fill in details</div>
		                            </div>
		                        </div>
	                    	</div>
	                    	<!-- end of panel heading  -->
	                    	<div class="">


	                    		<form action=""  method="post">
	                    			<div class="row">
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Transaction code: </label>
	                        						<input class="form-control" name="transaction_code" value="<?php echo !empty($transaction_code) ? $transaction_code : false; ?>">
      													<span class="error"><?php echo isset($transaction_code_error) ? $transaction_code_error : false; ?></span>
	                        						<p class="help-block">Enter the mpesa code.</p>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the username -->
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Amount: </label>
	                        						<input class="form-control" name="amount" value="<?php echo !empty($amount) ? $amount : false; ?>">
      													<span class="error"><?php echo isset($amount_error) ? $amount_error : false; ?></span>
	                        						<p class="help-block">Enter the amount.</p>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the password -->
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Payee Name: </label>
	                        						<input class="form-control" name="payee_name" value="<?php echo !empty($payee_name) ? $payee_name : false; ?>">
      													<span class="error"><?php echo isset($payee_name_error) ? $payee_name_error : false; ?></span>
	                        						<p class="help-block">Enter the payee name.</p>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the password -->
	                    			</div>
	                    			<!-- end of row -->

	                    			<div class="row">
	                    				<div class="col-sm-4">
	                    					<p class="lead">
                    							<div class="form-group">
	                        						<label>Date Paid: </label>
	                        						<input class="form-control" name="date_paid" value="<?php echo !empty($date_paid) ? $date_paid : false; ?>">
	                        						<span class="error"><?php echo isset($date_paid_error) ? $date_paid_error : false; ?></span>
	                        						<p class="help-block">Enter the date paid.</p>
                      							</div> 
                    						</p>
	                    				</div>
	                    				<!-- end of the username -->
	                    			</div>
	                    			<!-- end of row -->

	                    			<button type="submit" name="submit" class="btn btn-primary">Enter Payment</button>
	                    		</form>
	                    	</div>
	                    	<a href="index.php">
                        		<div class="panel-footer">
                            		<span class="pull-left"> Cancel </span>
                            		<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            		<div class="clearfix"></div>
                        		</div>
                    		</a>
            			</div>
            	</div>
            
            </div>
        </div>
        <!-- end of well -->

    </div>
    <!-- End of container-->
</div>
