<?php
require_once("../../../includes/initialize.php");
require("../../../includes/fpdf/fpdf.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
if (isset($session->chose)) {
    $search = new Search();
    $search_results="";
}
?>
<?php
$pdf = new FPDF();

$pdf->AddPage();

//Header image
$pdf->image('staff_files/image.jpg');
$pdf->SetFont("Arial", "U", "20");
$pdf->Cell(0, 10, "", 0, 1, "C");
$pdf->Cell(60, 10, "Job Seekers List", 1, 10, "C");
$pdf->Cell(0, 10, "", 0, 1, "C");

//Body infomation
$pdf->SetFont("Arial", "", "12");

//header
$pdf->Cell (10, 10, "No: ", 1, 0, "");
$pdf->Cell(50, 10, "Name", 1, 0, "");
$pdf->Cell(30, 10, "Region", 1, 0, "");
$pdf->Cell(30, 10, "Phone No", 1, 0, "");
$pdf->Cell(70, 10, "Email", 1, 1, "");



if (isset($session->chose)) {
$search_results = $search->search_cvs($session->chose);

$counter=1; 
foreach ($search_results as $results) :
	
//$jobs = Job::find_by_id ($results->job_id);
$user = User::find_by_id ($results->user_id);
$employer = Employer::find_by_id ($results->user_id);
$cv = Cv::find_by_id ($results->base_cv_id);



//body
$pdf->Cell (10, 10, $counter, 1, 0, "");
$pdf->Cell(50, 10, $user->full_name, 1, 0, "");
$pdf->Cell(30, 10, $user->region, 1, 0, "");
$pdf->Cell(30, 10, $user->phone_number, 1, 0, "");
$pdf->Cell(70, 10, $user->email, 1, 1, "");

$counter++;

endforeach;
} else {
	$pdf->Cell(0, 10, "No values have been passed", 0, 1, "C");
	 $message = "No values have been passed";
}
// //Working Information
// $pdf->Cell (1, 0, "Work Information: ", 1, 1, "U, C");
// $pdf->Cell(0, 10, "", 0, 1, "C");

// foreach ($work_history as $work) :

// 	!empty($work->organization_name) ? $organization_name=$work->organization_name : $organization_name="No entry";
// 	!empty($work->position_held) ? $position_held=$work->position_held : $position_held="No entry";
// 	!empty($work->job_description) ? $job_description=$work->job_description : $job_description="No entry";
// 	!empty($work->start_date) ? $start_date=$work->start_date : $start_date="No entry";
// 	!empty($work->end_date) ? $end_date=$work->end_date : $end_date="No entry";


// 	$pdf->cell(50, 0, "Organization Name:  ", 0, 0, "");
// 	$pdf->cell(1, 0, $organization_name, 0, 1, "");
// 	$pdf->Cell(0, 10, "", 0, 1, "C");

// 	$pdf->cell(50, 0, "Position Held: ", 0, 0, "");
// 	$pdf->cell(1, 0, $position_held, 0, 1, "");
// 	$pdf->Cell(0, 10, "", 0, 1, "C");

// 	$pdf->cell(50, 0, "Job Description: ", 0, 0, "");
// 	$pdf->cell(1, 0, "{$job_description}", 0, 1, "");
// 	$pdf->Cell(0, 10, "", 0, 1, "C");

// 	$pdf->cell(50, 0, "Duration: ", 0, 0, "");
// 	$pdf->cell(1, 0, "{$start_date} - {$end_date}", 0, 1, "");
// 	$pdf->Cell(0, 10, "", 0, 1, "C");

// endforeach;
// $pdf->Cell(0, 10, "", 0, 1, "C");

// //Education Information
// $pdf->Cell (1, 0, "Education Information: ", 1, 1, "U, C");
// $pdf->Cell(0, 10, "", 0, 1, "C");


// foreach ($education_history as $edu) :

// 	!empty($edu->institution) ? $institution=$edu->institution : $institution="No entry";
// 	!empty($edu->course_taken) ? $course_taken=$edu->course_taken : $course_taken="No entry";
// 	!empty($edu->level) ? $level=$edu->level : $level="No entry";
// 	!empty($edu->start_date) ? $start_date=$edu->start_date : $start_date="No entry";
// 	!empty($edu->end_date) ? $end_date=$edu->end_date : $end_date="No entry";


// 	$pdf->cell(50, 0, "Institution Name:  ", 0, 0, "");
// 	$pdf->cell(1, 0, $organization_name, 0, 1, "");
// 	$pdf->Cell(0, 10, "", 0, 1, "C");

// 	$pdf->cell(50, 0, "Course taken: ", 0, 0, "");
// 	$pdf->cell(1, 0, $position_held, 0, 1, "");
// 	$pdf->Cell(0, 10, "", 0, 1, "C");

// 	$pdf->cell(50, 0, "Education Level: ", 0, 0, "");
// 	$pdf->cell(1, 0, "{$job_description}", 0, 1, "");
// 	$pdf->Cell(0, 10, "", 0, 1, "C");

// 	$pdf->cell(50, 0, "Duration: ", 0, 0, "");
// 	$pdf->cell(1, 0, "{$start_date} - {$end_date}", 0, 1, "");
// 	$pdf->Cell(0, 10, "", 0, 1, "C");

// endforeach;

$pdf->Output();
?>