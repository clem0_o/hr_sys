<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
if (isset($_POST['submit'])) {

    $id = (INT) 0;
    $uid = $session->user_id;
    $jtitle = trim($_POST['job_title']);
    $j_des_brief = trim($_POST['job_description_brief']);
    $j_des_det = trim($_POST['job_description_detailed']);
    $category = trim($_POST['field']);
    $location = trim($_POST['location']);
    $employment_type = trim($_POST['employment_type']);
    $requirements = trim($_POST['requirements']);
    $qualifications = trim($_POST['qualifications']);
    $additional_info = trim($_POST['additional_info']);
    $max_salary = trim($_POST['max_salary']);
    $min_salary = trim($_POST['min_salary']);
    $currency = trim($_POST['currency']);
    $start_date = trim($_POST['start_year'])."-".trim($_POST['start_month'])."-".trim($_POST['start_day']);
    $end_date = trim($_POST['end_year'])."-".trim($_POST['end_month'])."-".trim($_POST['end_day']);

    $error = false;

    if (empty($jtitle)) {
      $jtitle_error = 'Title is empty, Please fill';
      $error = true;
    }

    if (empty($j_des_brief)) {
      $j_des_brief_error = 'Breif job description is empty, Please fill';
      $error = true;
    }

    if (empty($j_des_det)) {
      $j_des_det_error = 'Detailed job description is empty, Please fill';
      $error = true;
    }

    if (empty($category)) {
      $category_error = 'Category is empty, Please fill';
      $error = true;
    }

    if (empty($location)) {
      $location_error = 'Location is empty, Please fill';
      $error = true;
    }

    if (empty($employment_type)) {
      $employment_type_error = 'Employment type is empty, Please fill';
      $error = true;
    }

    if (empty($requirements)) {
      $requirements_error = 'Requirements is empty, Please fill';
      $error = true;
    }

    if (empty($qualifications)) {
      $qualifications_error = 'Qualifications is empty, Please fill';
      $error = true;
    }

    if (empty($additional_info)) {
      $additional_info_error = 'Additional info is empty, Please fill';
      $error = true;
    }

    if (empty($max_salary)) {
      $max_salary_error = 'Maximum salary is empty, Please fill';
      $error = true;
    }

    if (empty($min_salary)) {
      $min_salary_error = 'Minimum salary is empty, Please fill';
      $error = true;
    }

    if (empty($currency)) {
      $currency_error = 'Currency is empty, Please fill';
      $error = true;
    }

    if (empty($start_date)) {
      $start_date_error = 'Start Date is empty, Please fill';
      $error = true;
    }

    if (empty($end_date)) {
      $end_date_error = 'End date is empty, Please fill';
      $error = true;
    }

    if ($error === false) {
        $job = Job::make($id, $uid, $jtitle, $j_des_brief, $j_des_det, $category, $location, $employment_type, $requirements, $qualifications, $additional_info, $max_salary, $min_salary, $currency, $start_date, $end_date); 

        
        if ($job && $job->create()) {
            //successs message should come here
            redirect_to('active_jobs.php');
        }
    //failure message should come here
}
} else {
    $uid = "";
    $jtitle = "";
    $j_des_brief = "";
    $j_des_det = "";
    $category = "";
    $location = "";
    $employment_type = "";
    $requirements = "";
    $qualifications = "";
    $additional_info = "";
    $max_salary = "";
    $min_salary = "";
    $currency = "";
    $start_date = "";
    $end_date = "";
}


//var_dump($_POST);

?>
<?php include_layout_template('header_employee.php'); ?>

	<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add a new job
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Tables
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
<!-- the body goes here -->

                <div class="row">
                    <div class="col-ms-6">
                        <div class="well">
                          <form role="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" class="registration-form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_title">Job Title</label>
                                            <input type="text" name="job_title" class="form-control" id="job_title" placeholder="Enter Job Title" value="<?php echo !empty($jtitle) ? $jtitle : false; ?>" required>
                                            <span class="error"><?php echo isset($jtitle_error) ? $jtitle_error : false; ?></span>
                                        </div>
                                    </div>                                      
                                </div>
                                <!-- End of the row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_description_brief">Brief Job Description</label>
                                            <textarea class="form-control" name="job_description_brief" id="job_description_brief" rows="5" placeholder="Enter Brief Job Description" required><?php echo !empty($j_des_brief) ? $j_des_brief : false; ?></textarea>
                                            <span class="error"><?php echo isset($j_des_brief_error) ? $j_des_brief_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_description_detailed">Detailed Job Description</label>
                                            <textarea class="form-control" name="job_description_detailed" id="job_description_detailed" rows="5" placeholder="Enter Detailed Job Description" required><?php echo !empty($j_des_det) ? $j_des_det : false; ?></textarea>
                                            <span class="error"><?php echo isset($j_des_det_error) ? $j_des_det_error : false; ?></span>
                                        </div>
                                    </div>                                 
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <?php echo list_industries_fields_non_required (); ?>
                                            <span class="error"><?php echo isset($category_error) ? $category_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="location">Location</label>
                                            <input type="text" name="location" class="form-control" id="location" placeholder="Enter Job Location" value="<?php echo !empty($location) ? $location : false; ?>" required>
                                            <span class="error"><?php echo isset($location_error) ? $location_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="employment_type">Type</label>
                                            <?php echo employment_type ('employment_type'); ?>
                                            <span class="error"><?php echo isset($employment_type_error) ? $employment_type_error : false; ?></span>
                                        </div>
                                    </div>                                   
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="requirements">Job Requiements</label>
                                            <textarea class="form-control" name="requirements" id="requirements" rows="5" placeholder="Enter Detailed Job Requiements" required><?php echo !empty($requirements) ? $requirements : false; ?></textarea>
                                            <span class="error"><?php echo isset($requirements_error) ? $requirements_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="qualifications">Job Qualification</label>
                                            <textarea class="form-control" name="qualifications" id="qualifications" rows="5" placeholder="Enter Detailed Job Qualification" required><?php echo !empty($qualifications) ? $qualifications : false; ?></textarea>
                                            <span class="error"><?php echo isset($qualifications_error) ? $qualifications_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="">Addional Infomation</label>
                                            <textarea class="form-control" name="additional_info" rows="5" id="additional_info" placeholder="Enter Detailed Addional Infomation" required><?php echo !empty($additional_info) ? $additional_info : false; ?></textarea>
                                            <span class="error"><?php echo isset($additional_info_error) ? $additional_info_error : false; ?></span>
                                        </div>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->


                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="max_salary">Max Salary</label>
                                            <input type="text" name="max_salary" class="form-control" id="max_salary" placeholder="Enter Max Salary" value="<?php echo !empty($max_salary) ? $max_salary : false; ?>" required>
                                            <span class="error"><?php echo isset($max_salary_error) ? $max_salary_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="min_salary">Min Salary</label>
                                            <input type="text" name="min_salary" class="form-control" id="min_salary" placeholder="Enter Min Salary" value="<?php echo !empty($min_salary) ? $min_salary : false; ?>" required>
                                            <span class="error"><?php echo isset($min_salary_error) ? $min_salary_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <select name="currency" class="form-group" required>
                                                <option value="KSH">Kenya shilings</option>
                                                <option value="USD">Us Dollars</option>
                                            </select>
                                            <span class="error"><?php echo isset($currency_error) ? $currency_error : false; ?></span>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="start_date">Start Date</label>
                                            <?php echo months_of_year("start_month"); ?>
                                            <?php echo days_of_the_month("start_day"); ?>
                                            <?php echo years_long("start_year"); ?>
                                            <span class="error"><?php echo isset($start_date_error) ? $start_date_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="end_date">End Date</label>
                                            
                                            <?php echo months_of_year("end_month"); ?>
                                            <?php echo days_of_the_month("end_day"); ?>
                                            <?php echo years_long("end_year"); ?>
                                            <span class="error"><?php echo isset($end_date_error) ? $end_date_error : false; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                    <fieldset disabled>
                                        <div class="form-group">
                                             <label for="visible">Visibility</label>
                                             <input type="text" name="visible" class="form-control" id="visible" placeholder="Enter visibility, 1 or 0" required>
                                            <span class="error"><?php echo isset($visible_error) ? $visible_error : false; ?></span>
                                        </div>
                                    </fieldset>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->
                                
                              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                            </form> 
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include_layout_template('footer_employers.php'); ?>