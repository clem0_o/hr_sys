<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
// Check if the search data was submitted
if (isset($_GET)) {
    $search = new Search();
    $search_results="";
    !empty($_GET) ? $search_results = $search->search_cvs($_GET) : $message = "No values have been passed";
}

?>
<?php include_layout_template('header.php'); ?>

    
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <!--<form role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
        </form>-->
        <ul class="nav menu">
            <li><a href="profile.php"> Profile </a></li>
            <li class="active"><a href="cv.php"> Cvs</a></li>
            <li><a href="jobs.php"> Jobs </a></li>
            <li><a href="admin_payments.php"> Payment</a></li>
            <li><a href="all_applications.php"> Applications</a></li>
        </ul>   
    </div><!--/.sidebar-->
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"class="glyphicon glyphicon-home"></a></li>
                <li class="active"> CVs</li>
            </ol>
        </div><!--/.row -Bread crumbs-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> CVs </h1>
            </div>
        </div><!--/.row -Page header-->
        
        <div class="row">
            <div class="col-lg-12">



            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3>
                        <span class=" glyphicon glyphicon-log-in"> Find CVs </span>
                </div><!--End of the header-->
                <div class="panel-body">
                    <p>Search cv by name</p>
                    <form role="form" action="" id="search_name" class="form-inline" method="post">
                        <fieldset>
                            <input type="text" class="form-control" name="name_search" id="name_search">
                            <input type="submit" class="" value="Search name">
                        </fieldset>
                    </form>
                    <hr/><br/>
                    <p>Filter the cvs with the fields below or submit empty query to show all cvs: </p>
                    <hr/>
                    <form role="form" action="" method="get">
                        <fieldset>
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Education Level</th>
                                        <th>FIELD</th>
                                        <th>REGION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="search" name="name" class="form-control" id="name" placeholder="name..." /></td>
                                        <td>
                                        <select name="pay_status" class="form-control" id="status">
                                            <option value="">Choose Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Waiting">Waiting</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                        </td>
                                        <td>
                                        <select name="edu_level" class="form-control" id="status">
                                            <option value="">Choose Education level</option>
                                            <option value="O level">Form 4</option>
                                            <option value="Certificate">Certificate</option>
                                            <option value="Diploma">Diloma</option>
                                            <option value="Degree">Degree</option>
                                            <option value="Masters">Masters</option>
                                            <option value="PHD">PHD</option>
                                        </select>
                                        </td>
                                        <td><?php echo list_industries_fields_non_required (); ?></td>
                                        <td><input type="search" name="region" class="form-control" id="region" placeholder="region..." /></td>
                                    </tr>
                                </tbody>
                            </table>

                            <button type="submit" name="submit" class="btn btn-primary">Submit Query!</button>
                        </fieldset>
                    </form>
                </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <?php if ($search_results) : ?>
                <div class="panel panel-default">
					<div class="panel-heading">
                       <h3> Search Results</h3>
                    </div>
					<div class="panel-body">
                        <p>
                            <?php echo $search_results['count']; ?> Results found.
                        </p>
                        <hr/>
						<div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Region</th>
                                        <th>Contacts</th>
                                        <th>Links</th>
                                    </tr>
                                </thead>
                                <tbody><?php $counter=1; foreach ($search_results['result'] as $results) : ?>
                                <?php
                                    //$jobs = Job::find_by_id ($results->job_id);
                                    $user = User::find_by_id ($results->user_id);
                                    $employer = Employer::find_by_id ($results->user_id);
                                    $cv = Cv::find_by_id ($results->base_cv_id);
                                ?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo !empty($user->full_name()) ? $user->full_name() : false; ?></td>
                                        <td><?php echo $user->region; ?></td>
                                        <td><?php echo $user->phone_number." : ".$user->email; ?></td>
                                        <td><?php echo "<a href=\"../employees/employee_files/cv/".$cv->file_name."\" class=\"btn btn-link\">View CV</a>&nbsp;&nbsp;<a href=\"employee_profile.php?uc_id=".$user->id."\" class=\"btn btn-link\">View Profile</a>" ?></td>
                                    </tr><?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
					</div>
				</div>
                <?php $_SESSION["chose"] = $_GET; ?>
                <?php endif; echo $message; ?>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
            
        <div class="row">
            
        </div><!--/.row-->
                                
        <div class="row">
            
        </div><!--/.row-->
    </div>  <!--/.main-->
    

<?php include_layout_template('footer.php'); ?>