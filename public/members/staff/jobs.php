<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$user = User::find_by_id($session->user_id);
$jobs = new Job; 
$job_list = $jobs->find_available_jobs($session->user_id); 
$applications = new Application;

?>
<?php include_layout_template('header.php'); ?>

	
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<!--<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<ul class="nav menu">
			<li>test page</li>
            <li><a href="profile.php"> Profile </a></li>
            <li><a href="cv.php"> Cvs</a></li>
            <li><a href="jobs.php"> Jobs </a></li>
            <li><a href="admin_payments.php"> Payment</a></li>
            <li class="active"><a href="all_applications.php"> Applications</a></li>
		</ul>	
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"class="glyphicon glyphicon-home"></a></li>
				<li class="active">Posted Jobs (<?php echo $user->full_name(); ?>)</li>
			</ol>
		</div><!--/.row -Bread crumbs-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Posted Jobs</h1>
			</div>
		</div><!--/.row -Page header-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						List of active posted jobs
					</div>
					<div class="panel-body">
						<div id="job_status" ></div>
                        <?php if (!empty($job_list)) { 
							echo 'Add new details: <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#new_job" aria-expanded="false" aria-controls="new_job">Post a new Job</button>';	
						?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Ref Number</th>
                                        <th>Job Title</th>
                                        <th>Date expiring</th>
                                        <th>No of Apps</th>
                                    </tr>
                                </thead>
                                <tbody><?php
								$counter=1; foreach ($job_list as $jobs_listed) :
								if (time()>=strtotime($jobs_listed->end_date)) :
								?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo $jobs_listed->id; ?></td>
                                        <td><?php echo $jobs_listed->job_title; ?></td>
                                        <td><?php echo $jobs_listed->end_date; ?></td>
                                        <td><a href="job_applications.php?jid=<?php echo $jobs_listed->id; ?>"> <?php echo $applications->number_of_app($jobs_listed->id); ?> </a></td>
                                    </tr><?php
									endif;
									endforeach;
									?>
                                </tbody>
                            </table>
                        </div>
                        <?php } else { echo 'There are no jobs posted, <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#new_job" aria-expanded="false" aria-controls="new_job">Post a new Job</button>';} ?>

						<div class="collapse" id="new_job">
							<div class="well">
								<form action="" name="new_job_form" id="new_job_form" method="post" class="">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="reff_number">Country </label>
												<input type="text" name="country" class="form-control" id="reff_number" placeholder="country...">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="job_title">Job Title</label>
												<input type="text" name="job_title" class="form-control" id="job_title" placeholder="Enter Job Title">
											</div>
										</div>                                      
									</div>
									<!-- End of the row -->
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="job_description_brief">Brief Job Description</label>
												<textarea class="form-control" name="job_description_brief" id="job_description_brief" rows="5" placeholder="Enter Brief Job Description"></textarea>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="job_description_detailed">Detailed Job Description</label>
												<textarea class="form-control" name="job_description_detailed" id="job_description_detailed" rows="5" placeholder="Enter Detailed Job Description"></textarea>
											</div>
										</div>                                 
									</div>
									<!-- End of the row -->
									<div class="row">
										<div class="col-xs-4">
											<div class="form-group">
												<label for="category">Category</label>
												<input type="text" name="category" class="form-control" id="category" placeholder="Enter Job Category">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="location">Location</label>
												<input type="text" name="location" class="form-control" id="location" placeholder="Enter Job Location">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="employment_type">Type</label>
												<input type="text" name="employment_type" class="form-control" id="employment_type" placeholder="Enter Job Type">
											</div>
										</div>                                     
									</div>
									<!-- End of the row -->

									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="requirements">Job Requiements</label>
												<textarea class="form-control" name="requirements" id="requirements" rows="5" placeholder="Enter Detailed Job Requiements"></textarea>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="qualifications">Job Qualification</label>
												<textarea class="form-control" name="qualifications" id="qualifications" rows="5" placeholder="Enter Detailed Job Qualification"></textarea>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Addional Infomation</label>
												<textarea class="form-control" name="additional_info" rows="5" id="additional_info" placeholder="Enter Detailed Addional Infomation"></textarea>
											</div>
										</div>                                     
									</div>
									<!-- End of the row -->
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="max_salary">Max Salary</label>
												<input type="text" name="max_salary" class="form-control" id="max_salary" placeholder="Enter Max Salary">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="min_salary">Min Salary</label>
												<input type="text" name="min_salary" class="form-control" id="min_salary" placeholder="Enter Min Salary">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="currency">Currency</label>
												<input type="text" name="currency" class="form-control" id="currency" placeholder="Enter Payment Currency">
											</div>
										</div>                                     
									</div>
									<!-- End of the row -->
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="start_date">Start Date</label>
												<input type="date" name="job_start_date" id="job_start_date" class="form-control" id="start_date" placeholder="Enter start date: YYYY-mm-dd">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="end_date">End Date</label>
												<input type="date" name="job_end_date" id="job_end_date" class="form-control" id="end_date" placeholder="Enter end date: YYYY-mm-dd">
											</div>
										</div>
										<div class="col-sm-4">
										<fieldset disabled>
											<div class="form-group">
												<label for="visible">Visibility</label>
												<input type="text" name="visible" class="form-control" id="visible" placeholder="Enter visibility, 1 or 0">
											</div>
										</fieldset>
										</div>                                     
									</div>
									<!-- End of the row -->
									<input type="hidden" name="mode" value="new_job">
									<input type="submit" name="submit" class="btn btn-primary" value="Submit">
									<button class="btn btn-default" type="button" data-toggle="collapse" data-target="#new_job" aria-expanded="false" aria-controls="new_job"> Cancel </button>
								</form>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						Completed jobs
					</div>
					<div class="panel-body">
						<?php if (!empty($job_list)) { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Ref Number</th>
                                        <th>Job Title</th>
                                        <th>Date expiring</th>
                                        <th>No of Apps</th>
                                    </tr>
                                </thead>
                                <tbody><?php
								$counter=1; foreach ($job_list as $jobs_listed) :
								if (time()<=strtotime($jobs_listed->end_date)) :
								?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo $jobs_listed->id; ?></td>
                                        <td><?php echo $jobs_listed->job_title; ?></td>
                                        <td><?php echo $jobs_listed->end_date; ?></td>
                                        <td><a href="job_applications.php?jid=<?php echo $jobs_listed->id; ?>"><?php echo $applications->number_of_app($jobs_listed->id); ?></a></td>
                                    </tr><?php
									endif;
									endforeach;
									?>
                                </tbody>
                            </table>
                        </div>
                        <?php } else { echo "There is no data entered";} ?>
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		<div class="row">
			
		</div><!--/.row-->
								
		<div class="row">
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	

<?php include_layout_template('footer.php'); ?>