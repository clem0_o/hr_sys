<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$jobs = new Job;
$listed_jobs = Job::find_all();
?>
<?php
// Check if the search data was submitted
if (isset($_GET)) {
    $search = new Search();

    $search_results="";

    !empty($_GET) ? $search_results = $search->search_jobs($_GET) : $message = "No values have been passed";
}
?>
<?php include_layout_template('header_admin.php'); ?>

	<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            All Jobs
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i><a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Tables
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <!-- List of jobs as sorted -->

                <!--the searching part of the site -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Find Jobs By: </h2>
                        <div class="table-responsive">
                        <div class="row">
                            <div class="well">
                                
                            </div>
                        </div>

                        <form action="" method="get" >
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>ACTIVE JOBS</th>
                                        <th>DATE Poseted</th>
                                        <th>DATE Expiring</th>
                                        <th>FIELD</th>
                                        <th>REGION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                        <select name="status" class="form-control" id="status">
                                            <option value="">Choose Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Waiting">Waiting</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                        </td>
                                        <td><input type="search" name="date_posted" class="form-control" id="date_posted" placeholder="date posted..." /></td>
                                        <td><input type="search" name="date_expiring" class="form-control" id="date_expiring" placeholder="date expiring..." /></td>
                                        <td><?php echo list_industries_fields_non_required (); ?></td>
                                        <td><input type="search" name="region" class="form-control" id="region" placeholder="region..." /></td>
                                    </tr>
                                </tbody>
                            </table>

                            <button type="submit" name="submit" class="btn btn-primary">Submit Query!</button>
                        </form>
                        </div>
                        <?php if ($search_results) : ?>
                        <div class="row">
                            <div class="col-lg-12">
                            <h2>Search Results</h2>
                                <p>
                                    <?php echo $search_results['count']; ?> Results found.
                                </p>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Job title</th>
                                            <th>Region</th>
                                            <th>Date Posted</th>
                                            <th>Links</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $counter=1; foreach ($search_results['result'] as $results) : ?>
                                        <tr>
                                            <td><?php echo $counter;  $counter++; ?></td>
                                            <td><?php echo $results->job_title; ?></td>
                                            <td><?php echo $results->location; ?></td>
                                            <td><?php echo $results->posted_date; ?></td>
                                            <td><?php echo "<a href=\"edit_job.php?jid=".$results->id."\">View details</a>" ?></td>
                                        </tr><?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                        <?php endif; echo $message; ?>
                    </div>
                </div>
                <!-- /.row -->


                <!-- List of jobs Available -->
                <div class="row">
                    <div class="col-lg-12">
                        <a href="add_job.php" class="btn btn-primary">add a job</a>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include_layout_template('footer_employee.php'); ?>