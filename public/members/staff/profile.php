<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
// This brings in the user details
$user = User::find_by_id($session->user_id);

//
$empl = Employer::find_by_user_id($user->id);

$work_history = WorkingHistory::find_all_user_id($user->id);
$jobs_history = new WorkingHistory;

$education_history = Education_bg::find_all_user_id($user->id);
$education_history_obj = new Education_bg;

$other_qualifications = OtherQualifications::find_all_user_id($user->id);
?>
<?php include_layout_template('header.php'); ?>

    
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <!--<form role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
        </form>-->
        <ul class="nav menu">
            <li class="active"><a href="profile.php"> Profile </a></li>
            <li><a href="cv.php"> Cvs</a></li>
            <li><a href="jobs.php"> Jobs </a></li>
            <li><a href="admin_payments.php"> Payment</a></li>
            <li><a href="all_applications.php"> Applications</a></li>
        </ul>   
    </div><!--/.sidebar-->
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"class="glyphicon glyphicon-home"></a></li>
                <li class="active">Profile (<?php echo $user->full_name(); ?>)</li>
            </ol>
        </div><!--/.row -Bread crumbs-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Profile</h1>
            </div>
        </div><!--/.row -Page header-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Personal Details
                    </div>
                    <div class="panel-body">
                        <div id="user_status"></div>
                        <p>
                            <label>Name: <?php echo $user->full_name(); ?></label>
                            <button class="pull-right btn btn-primary glyphicon glyphicon-edit" type="button" data-toggle="collapse" data-target="#names_form" aria-expanded="false" aria-controls="names_form">Edit</button>
                            <div class="collapse" id="names_form">
							<div class="well">
								<form action="" name="admin_edit_names" id="admin_edit_names" method="post" class="in-line">
									<div class="form-group">
										<input type="text" placeholder="First name..." name="firstName" value="<?php echo $user->first_name; ?>">
										<input type="text" placeholder="Last name..." name="lastName" value="<?php echo $user->last_name; ?>">
										<input type="hidden" name="mode" value="admin_update_names">
                                            <input type="hidden" name="userId" value="<?php echo $session->user_id; ?>">
										<input type="submit" name="editNameSubmit" id="editNameSubmit" value="submit" class="pull-right btn btn-success">
									</div>
								</form>
							</div>
						</div>
                        </p>
                        <p>
                            <label>Phone No: <?php echo $user->phone_number; ?> </label>
                            <button class="pull-right btn btn-primary glyphicon glyphicon-edit" type="button" data-toggle="collapse" data-target="#phone_form" aria-expanded="false" aria-controls="phone_form">Edit</button>
                            <div class="collapse" id="phone_form">
                                <div class="well">
                                    <form action="" name="admin_edit_phone" id="admin_edit_phone" method="post" class="in-line">
                                        <div class="form-group">
                                            <input type="text" placeholder="Phone number..." name="phone_number" value="<?php echo $user->phone_number; ?>">
                                            <input type="hidden" name="mode" value="admin_update_phone">
                                            <input type="hidden" name="userId" value="<?php echo $session->user_id; ?>">
                                            <input type="submit" name="phoneSubmit" id="phoneSubmit" value="submit" class="pull-right btn btn-success">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </p>
                        <p>
                            <label>Email: <?php echo $user->email; ?></label>
                            <button class="pull-right btn btn-primary glyphicon glyphicon-edit" type="button" data-toggle="collapse" data-target="#email_form" aria-expanded="false" aria-controls="email_form">Edit</button>
                            <div class="collapse" id="email_form">
                                <div class="well">
                                    <form action="" name="admin_edit_email" id="admin_edit_email" method="post" class="in-line">
                                        <div class="form-group">
                                            <input type="text" placeholder="Email..." name="email" value="<?php echo $user->email; ?>">
                                            <input type="hidden" name="mode" value="admin_update_email">
                                            <input type="hidden" name="userId" value="<?php echo $session->user_id; ?>">
                                            <input type="submit" name="emailSubmit" id="emailSubmit" value="submit" class="pull-right btn btn-success">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </p>
                        <p>
                            <label>Residence: <?php echo $user->region; ?></label>
                            <button class="pull-right btn btn-primary glyphicon glyphicon-edit" type="button" data-toggle="collapse" data-target="#region_form" aria-expanded="false" aria-controls="region_form">Edit</button>
                            <div class="collapse" id="region_form">
                                <div class="well">
                                    <form action="" name="admin_edit_region" id="admin_edit_region" method="post" class="in-line">
                                        <div class="form-group">
                                            <input type="text" placeholder="Region..." name="region" value="<?php echo $user->region; ?>">
                                            <input type="hidden" name="mode" value="admin_update_region">
                                            <input type="hidden" name="userId" value="<?php echo $session->user_id; ?>">
                                            <input type="submit" name="regionSubmit" id="regionSubmit" value="submit" class="pull-right btn btn-success">
                                        </div>
                                    </form>
                                </div>
						</div>
                        </p>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
            
        <div class="row">
            
        </div><!--/.row-->
                                
        <div class="row">
            
        </div><!--/.row-->
    </div>  <!--/.main-->
    

<?php include_layout_template('footer.php'); ?>