<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
if (isset($_POST['submit'])) {

    $id = $_GET["jid"];
    $uid = $session->user_id;
    $jtitle = trim($_POST['job_title']);
    $j_des_brief = trim($_POST['job_description_brief']);
    $j_des_det = trim($_POST['job_description_detailed']);
    $category = trim($_POST['category']);
    $location = trim($_POST['location']);
    $employment_type = trim($_POST['employment_type']);
    $requirements = trim($_POST['requirements']);
    $qualifications = trim($_POST['qualifications']);
    $additional_info = trim($_POST['additional_info']);
    $max_salary = trim($_POST['max_salary']);
    $min_salary = trim($_POST['min_salary']);
    $currency = trim($_POST['currency']);
    $start_date = trim($_POST['start_date']);
    $end_date = trim($_POST['end_date']);

    $job = Job::make($id, $uid, $jtitle, $j_des_brief, $j_des_det, $category, $location, $employment_type, $requirements, $qualifications, $additional_info, $max_salary, $min_salary, $currency, $start_date, $end_date);

    
    

    if ($job && $job->update()) {
        //successs message should come here
        redirect_to('active_jobs.php');
    }
    //failure message should come here

} else {
    $uid = "";
    $jtitle = "";
    $j_des_brief = "";
    $j_des_det = "";
    $category = "";
    $location = "";
    $employment_type = "";
    $requirements = "";
    $qualifications = "";
    $additional_info = "";
    $max_salary = "";
    $min_salary = "";
    $currency = "";
    $start_date = "";
    $end_date = "";
}


//var_dump($_POST);

?>
<?php
$get_job = Job::find_by_id($_GET["jid"]);

    $uid = "";
    $jtitle = $get_job->job_title;
    $j_des_brief = $get_job->job_description_brief;
    $j_des_det = $get_job->job_description_detailed;
    $category = $get_job->category;
    $location = $get_job->location;
    $employment_type = $get_job->employment_type;
    $requirements = $get_job->requirements;
    $qualifications = $get_job->qualifications;
    $additional_info = $get_job->additional_info;
    $max_salary = $get_job->max_salary;
    $min_salary = $get_job->min_salary;
    $currency = $get_job->currency;
    $start_date = $get_job->start_date;
    $end_date = $get_job->end_date;
?>
<?php include_layout_template('header_admin.php'); ?>

	<div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add a new job
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Tables
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
<!-- the body goes here -->

                <div class="row">
                    <div class="col-ms-6">
                        <div class="well">
                          <form role="form" action="edit_job.php?jid=<?php echo $_GET["jid"]; ?>" method="post" class="registration-form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_title">Job Title</label>
                                            <input type="text" name="job_title" class="form-control" id="job_title" placeholder="Enter Job Title" value="<?php echo !empty($jtitle) ? $jtitle : false; ?>">
                                        </div>
                                    </div>                                      
                                </div>
                                <!-- End of the row -->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_description_brief">Brief Job Description</label>
                                            <textarea class="form-control" name="job_description_brief" id="job_description_brief" rows="5" placeholder="Enter Brief Job Description"><?php echo !empty($j_des_brief) ? $j_des_brief : false; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="job_description_detailed">Detailed Job Description</label>
                                            <textarea class="form-control" name="job_description_detailed" id="job_description_detailed" rows="5" placeholder="Enter Detailed Job Description"><?php echo !empty($j_des_det) ? $j_des_det : false; ?></textarea>
                                        </div>
                                    </div>                                 
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <input type="text" name="category" class="form-control" id="category" placeholder="Enter Job Category" value="<?php echo !empty($category) ? $category : false; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="location">Location</label>
                                            <input type="text" name="location" class="form-control" id="location" placeholder="Enter Job Location" value="<?php echo !empty($location) ? $location : false; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="employment_type">Type</label>
                                            <input type="text" name="employment_type" class="form-control" id="employment_type" placeholder="Enter Job Type" value="<?php echo !empty($employment_type) ? $employment_type : false; ?>">
                                        </div>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="requirements">Job Requiements</label>
                                            <textarea class="form-control" name="requirements" id="requirements" rows="5" placeholder="Enter Detailed Job Requiements"><?php echo !empty($requirements) ? $requirements : false; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="qualifications">Job Qualification</label>
                                            <textarea class="form-control" name="qualifications" id="qualifications" rows="5" placeholder="Enter Detailed Job Qualification"><?php echo !empty($qualifications) ? $qualifications : false; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="">Addional Infomation</label>
                                            <textarea class="form-control" name="additional_info" rows="5" id="additional_info" placeholder="Enter Detailed Addional Infomation"><?php echo !empty($additional_info) ? $additional_info : false; ?></textarea>
                                        </div>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->


                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="max_salary">Max Salary</label>
                                            <input type="text" name="max_salary" class="form-control" id="max_salary" placeholder="Enter Max Salary" value="<?php echo !empty($max_salary) ? $max_salary : false; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="min_salary">Min Salary</label>
                                            <input type="text" name="min_salary" class="form-control" id="min_salary" placeholder="Enter Min Salary" value="<?php echo !empty($min_salary) ? $min_salary : false; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="currency">Currency</label><br/>
                                            <select name="currency" class="form-group">
                                                <option value="KSH">Kenya shilings</option>
                                                <option value="USD">Us Dollars</option>
                                            </select>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="start_date">Start Date</label>
                                            <input type="date" name="start_date" class="form-control" id="start_date" placeholder="Enter start date: YYYY-mm-dd" value="<?php echo !empty($start_date) ? $start_date : false; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="end_date">End Date</label>
                                            <input type="date" name="end_date" class="form-control" id="end_date" placeholder="Enter end date: YYYY-mm-dd" value="<?php echo !empty($end_date) ? $end_date : false; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                    <fieldset disabled>
                                        <div class="form-group">
                                             <label for="visible">Visibility</label>
                                             <input type="text" name="visible" class="form-control" id="visible" placeholder="Enter visibility, 1 or 0">
                                        </div>
                                    </fieldset>
                                    </div>                                     
                                </div>
                                <!-- End of the row -->
                                
                              <button type="submit" name="submit" class="btn btn-default">Submit</button>
                            </form> 
                        </div>
                    </div>
                </div>
                <!-- /.row -->


                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <a href="add_job.php">new job</a>
                        </ol>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include_layout_template('footer_admin.php'); ?>