<?php
require_once("../../../includes/initialize.php");
if (!$session->is_logged_in()) { redirect_to("login.php");}
?>
<?php
$active_application = Application::find_all();
?>
<?php
// Check if the search data was submitted
if (isset($_GET)) {
    $search = new Search();

    $search_results="";

    !empty($_GET) ? $search_results = $search->search_aplication($_GET) : $message = "No values have been passed";
}
?>
<?php include_layout_template('header.php'); ?>

    
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <!--<form role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
        </form>-->
        <ul class="nav menu">
            <li><a href="profile.php"> Profile </a></li>
            <li><a href="cv.php"> Cvs</a></li>
            <li><a href="jobs.php"> Jobs </a></li>
            <li><a href="admin_payments.php"> Payment</a></li>
            <li class="active"><a href="all_applications.php"> Applications</a></li>
        </ul>   
    </div><!--/.sidebar-->
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"class="glyphicon glyphicon-home"></a></li>
                <li class="active">All Applications</li>
            </ol>
        </div><!--/.row -Bread crumbs-->
        
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Applications</h1>
            </div>
        </div><!--/.row -Page header-->
		
		<div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3>
                        <span class=" glyphicon glyphicon-log-in"> Find applications </span>
                </div><!--End of the header-->
                <div class="panel-body">
                    <p>Filter the applications with the fields below or submit empty query to show all applications: </p>
                    <hr/>
                    <form role="form" action="" method="get">
                        <fieldset>
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>ACTIVE Applications</th>
                                        <th>Applied by Date</th>
                                        <th>Aplication Status</th>
                                        <th>FIELD</th>
                                        <th>REGION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                        <select name="pay_status" class="form-control" id="status">
                                            <option value="">Choose Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Waiting">Waiting</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                        </td>
                                        <td><input type="search" name="date_applied" class="form-control" id="date_applied" placeholder="Applied by this Date..." /></td>
                                        <td>
                                        <select name="app_status" class="form-control" id="status">
                                            <option value="">Choose Status</option>
                                            <option value="selected">Selected</option>
                                            <option value="shot_listed">Short Listed</option>
                                            <option value="received">Received</option>
                                        </select>
                                        </td>
                                        <td><?php echo list_industries_fields_non_required (); ?></td>
                                        <td><input type="search" name="region" class="form-control" id="region" placeholder="region..." /></td>
                                    </tr>
                                </tbody>
                            </table>

                            <button type="submit" name="submit" class="btn btn-primary">Submit Query!</button>
                        </fieldset>
                    </form>
                </div>
        </div><!--/.row-->
        
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        All Applications
                    </div>
                    <div class="panel-body">
                        <?php if ($search_results) : ?>
						<h2>Search Results</h2>
                        <p>
                            <?php echo $search_results['count']; ?> Results found.
                        </p>
						<div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Job title</th>
                                        <th>Full Name</th>
                                        <th>Region</th>
                                        <th>Links</th>
                                    </tr>
                                </thead>
                                <tbody><?php $counter=1; foreach ($search_results['result'] as $results) : ?>
                                <?php
                                    $jobs = Job::find_by_id ($results->job_id);
                                    $user = User::find_by_id ($results->user_id);
                                    $employer = Employer::find_by_id ($results->user_id);
                                ?>
                                    <tr>
                                        <td><?php echo $counter;  $counter++; ?></td>
                                        <td><?php echo !empty($jobs->job_title) ? $jobs->job_title : "No entry, something went wrong"; ?></td>
                                        <td><?php echo !empty($user->full_name()) ? $user->full_name() : "No entry, something went wrong"; ?></td>
                                        <td><?php echo !empty($user->region) ? $user->region : "No entry, something went wrong"; ?></td>
                                        <td><?php echo "<a href=\"apply_job.php?ref_number=\" class=\"btn btn-link\">View Profile</a>" ?></td>
                                    </tr><?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
						<?php endif; echo $message; ?>
                    </div>
                </div>
            </div>
            <div class="clo-lg-2 col-sm-12">
                <!--The Ad goes here-->
            </div>
        </div><!--/.row-->
        
        <div class="row">
            
        </div><!--/.row-->
            
        <div class="row">
            
        </div><!--/.row-->
                                
        <div class="row">
            
        </div><!--/.row-->
    </div>  <!--/.main-->
    
