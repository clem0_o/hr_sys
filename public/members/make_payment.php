<?php
//require once the needed class
require_once("../../includes/initialize.php");
//check if there is a session, if true redirect to index.php
if ($session->is_logged_in()) {
}
?>

<?php include_layout_template('form_header.php'); ?>

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3>
						<span class=" glyphicon glyphicon-log-in"> Payment </span>
				</div><!--End of the header-->
				<div class="panel-body">
					<p> To activate your account follow the procedure below: </p>
					<hr/>
					<p>
						<ul class="mpesa">
							<li>Go to M-PESA Menu on your phone</li>
							<li>Select Pay Bill (may be under the Lipa Na M-Pesa)</li>
							<li>Enter Business Number - 961700</li>
							<li>Enter Account Number - <?php echo "IMC/".$session->user_id; ?></li>
							<li>Enter Amount - 600</li>
							<li>Enter your M-PESA PIN</li>
							<li>Send and wait for SMS receipt from INSIGHT MANAGEMENT</li>
							<br/>
							<btn><a href="employees/profile.php" class="btn btn-success">finish transaction</a></btn>
						</ul>
					</p>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->

<?php include_layout_template('form_footer.php'); ?>  