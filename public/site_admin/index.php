<?php

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Insight Management">
    <meta name="author" content="Clement-05">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title> Insight Mamangement </title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="css/icomoon.css">
    <link href="css/animate-custom.css" rel="stylesheet">


    
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    
    <script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/modernizr.custom.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body data-spy="scroll" data-offset="0" data-target="#navbar-main">
  
  
  	<div id="navbar-main">
      <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon icon-shield" style="font-size:30px; color:#3498db;"></span>
          </button>
          <a class="navbar-brand hidden-xs hidden-sm" href="#home"><span class="icon icon-shield" style="font-size:18px; color:#3498db;"></span></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav pull-right">
            <li><a href="#home" class="smoothScroll">Home</a></li>
			<li> <a href="#about" class="smoothScroll"> About</a></li>
			<li> <a href="#services" class="smoothScroll"> Services</a></li>
			<li> <a href="#team" class="smoothScroll"> Team</a></li>
			<li> <a href="#portfolio" class="smoothScroll"> Portfolio</a></li>
			<li> <a href="#blog" class="smoothScroll"> Blog</a></li>
			<li> <a href="#contact" class="smoothScroll"> Contact</a></li>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    </div>

  
  
		<!-- ==== HEADERWRAP ==== -->
	    <div id="headerwrap" id="home" name="home">
			<header class="clearfix">
	  		 		<h1><span class="icon icon-shield"></span></h1>
	  		 		<p> Insight Management. </p>
	  		 		<p>Trust and Reliable</p>
	  		</header>	    
	    </div><!-- /headerwrap -->

		<!-- ==== GREYWRAP ==== -->
		<div id="greywrap">
			<div class="row">
				<div class="col-sm-4 callout">
					<span class="icon icon-stack"></span>
					<h2>Recruitment</h2>
					<p>We offer the very much needed marching of job seekers and the available job offers in all inderstries in Kenya and beyond. </p>
                    <a href="members/loging.php" class="btn btn-primary">Recuitment Portal</a>
				</div><!-- col-lg-4 -->
					
				<div class="col-sm-4 callout">
					<span class="icon icon-eye"></span>
					<h2>Ufahamu Sacco</h2>
					<p>At ufahamu we offer you a trusted and reliable way to save and also offer reliable loans at a very cheap interest. </p>
                    <a href="#" class="btn btn-primary">Ufahamu Sacco</a>
				</div><!-- col-lg-4 -->	
				
				<div class="col-sm-4 callout">
					<span class="icon icon-heart"></span>
					<h2>Clean Source</h2>
					<p>We don't make sites, we craft themes with love & passion. That is our most valued secret. We only do thing that we love.   </p>
                    <a href="#" class="btn btn-primary">Clean Source</a>
				</div><!-- col-lg-4 -->	
			</div><!-- row -->
		</div><!-- greywrap -->
		
		<!-- ==== ABOUT ==== -->
		<div class="container" id="about" name="about">
			<div class="row white">
			<br>
				<h1 class="centered">A LITTLE ABOUT OUR AGENCY</h1>
				<hr>
				
				<div class="col-sm-6">
					<p>Insight Management Consultants Limited (IMC) was incorporated by the Registrar of Companies in Kenya as a Limited Liability Company in March 2006. We have since become a leader in the provision of tailor-made Human Resources Solutions to the challenges facing our customers in specific market niches. We believe in a spirit of partnership with our customers, employees and suppliers where together we generate wealth for all and the fair sharing thereof. Further we believe in total quality, in service delivery beyond expectations, accountability, ownership, and responsible citizenship. We have addressed ourselves to finding lasting solutions to the pertinent competencies required to enhance and dissolve deficiencies in human resource management/development, events management, IT Solutions, project management, Business Process Outsourcing, office solutions, accountancy, and research. We are dedicated to putting customers first. We develop, train, and provide Skilled, semi-skilled and unskilled manpower to both private and public sectors.</p>
				</div><!-- col-lg-6 -->
				
				<div class="col-ms-6">
					<p>Our business is developing human skills and provision of professional services to private and public organizations. All our actions must be measured by our success in achieving this goal. We value above all, our ability to serve everyone who can benefit from the appropriate use of our products and services, thereby providing lasting consumer satisfaction. We are committed to the highest standard of ethics and integrity. We are responsible to our customers, employees and their families, environment and the society we serve. In discharging our duties we do not take professional shortcuts. Our interactions with all segments of the society reflect the high standards we profess. We strive to identify the most critical needs of our consumers. We recognize that the ability to excel and to most completely meet society and customers’ needs depends on the integrity, knowledge, imagination, skills, diversity and teamwork of our employees and we value these qualities most highly. To this end we strive to create an environment of mutual respect, encouragement and teamwork; an environment that rewards commitment and performance and is responsible to the needs of our employees and their families.</p>
				</div><!-- col-lg-6 -->
			</div><!-- row -->
		</div><!-- container -->
		
		<!-- ==== SECTION DIVIDER1 -->
		<section class="section-divider textdivider divider1">
			<div class="container">
				<h1>OUR VALUES</h1>
				<hr>
				<p>Our business is developing human skills and provision of professional services to private and public organizations. All our actions must be measured by our success in achieving this goal. We value above all, our ability to serve everyone who can benefit from the appropriate use of our products and services, thereby providing lasting consumer satisfaction.f</p>
			</div><!-- container -->
		</section><!-- section -->
		
		
		<!-- ==== SERVICES ==== -->
		<div class="container" id="services" name="services">
			<br>
			<br>
            <!-- <div id="greywrap"> -->
			<div class="row">
				<h2 class="centered">ONE BRAND, ONE VOICE.</h2>
				<hr>
				<br>
                <div class="row"><!-- row 1 -->
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Staff Recruitment</h3>
                        <p>IMCL specializes in highly qualified individuals for a wide range of Permanent, Temporary and Contract office support and administrative positions. All of our applicants are thoroughly screened, skill tested and reference checked before you meet them. Our goal is 100% client and applicant satisfaction.</p>
                    </div>
                </div><!-- End of staff recruitment -->
                    
                
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Research</h3>
                        <p>We provide systematic qualitative and quantitative data collection and analysis services for our clients. We have qualified and experienced field managers and data collection executives for accurate results. We provide our data in the format preferred by the client, (SPSS, ASCII, MS Excel, M.S Access etc).</p>
                    </div>
                </div><!-- End of Research -->
                
                
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Business Process Outsourcing</h3>
                        <p>Our Business process outsourcing (BPO) Division specializes on back office outsourcing – which includes internal business functions such as human resources, Accounting or book keeping, and front office outsourcing – which includes customer-related services such as contact center services.</p>
                    </div>
                </div><!-- End of Business Process Outsourcing -->
                
                </div><!-- end of row 1 -->
                
                <div class="row">
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Event Management</h3>
                        <p>There is no room for laxity at Insight Management Consultants Limited. We get down to the nitty-gritty as we go about our business unperturbed. We hate guess-work at this age of advanced technology when precision hangs loose on everyone’s lip. So we are professional and precise in all we do. In event management, we have the entire array to facilitate conferences of any magnitude under the watchful eyes of highly qualified operational and technical personnel.</p>
                    </div>
                </div><!-- End of staff recruitment -->
                
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Payroll Services</h3>
                        <p>As a payroll services provider, we offer cost-efficient and dependable solutions. Our company handles end-to-end payroll management. With a professional payroll service company handling your accounts, you will have time to grow and expand your business. The services do not require setup fees and it has no software installations. Everything is handled by IMCL.</p>
                    </div>
                </div><!-- End of payroll services -->
                    
                    
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Training Services</h3>
                        <p>Companies survive and thrive by creating and maintaining satisfied customers through their people. Insight Management Consultants Limited understands the importance of reviving, developing, and rewarding employees through management training. Our experience and expertise in management training, leadership, change training, consulting, and coaching services, makes it easier for us to achieve our clients’ objectives.</p>
                    </div>
                </div><!-- End of payroll services -->
                
                </div><!-- End of row 2-->
                
                 <div class="row">
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Employment Background Checks</h3>
                        <p>We have a highly experienced team that offer a personal low-cost service to all businesses. It doesn’t matter whether you have a handful of employees or thousands; we tailor our services and costs to suit you! The more the number of people you deal with in your organization the higher the risk to your business goals. To manage that risk you need the protection of a comprehensive and effective employment screening service. We at IMCL offer a wide range of services from a full pre-employment check, to a service tailored to suit you:</p>
                    </div>
                </div><!-- End of staff recruitment -->
                
                
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Health & Safety Policy</h3>
                        <p>Our objective is to protect our workers from injuries and illness. We are ultimately responsible for workers health and safety and we are committed to taking every reasonable precaution for the protection of all the workers. To fulfill this commitment, we make every effort to provide and maintain a safe and healthy workplace by adhering to acceptable industry standards. In working with our clients, we ensure we respect their Health and safety policy and where necessary we may give proposals on how to improve it.</p>
                    </div>
                </div><!-- End of payroll services -->
                     
                <div class="col-sm-4">
                    <div class="well">
                        <h3>Project Management</h3>
                        <p>We have expertise and experience in managing projects. By working closely with our clients’ leadership teams, we develop and manage the transition project plan including the associated communication, training, rollout plans and deliverables (such as the training curriculum, materials, and schedule for training all the users of the system). We provide forums to enable the exchange of information, skills, techniques and methods, which will enhance the professional status of project management.</p>
                    </div>
                </div><!-- End of payroll services -->
                     
                 
                </div><!-- End of row 3-->
				
			</div><!-- row -->
            <!-- </div><!-- greywrap -->
			
			<div class="row">
				<h2 class="centered">CUSTOMER SERVICE, ALWAYS COME FIRST.</h2>
				<hr>
				<br>
				<div class="col-lg-offset-2 col-lg-8">
					<img class="img-responsive" src="img/iphone.png" alt="">
				</div><!-- col -->
			</div><!-- row -->
		</div><!-- container -->
  		

		<!-- ==== SECTION DIVIDER2 -->
		<section class="section-divider textdivider divider2">
			<div class="container">
				<h1> CUSTOMER RELATIONS </h1>
				<hr>
				<p>To develop a deeper and more meaningful connection with consumers, we believe design must invite them to take part in the conversation.</p>
			</div><!-- container -->
		</section><!-- section -->

		<!-- ==== TEAM MEMBERS ==== -->
		<div class="container" id="team" name="team">
		<br>
			<div class="row white centered">
				<h1 class="centered">MEET OUR AWESOME TEAM</h1>
				<hr>
				<br>
				<br>
				<div class="col-sm-3 centered">
					<img class="img img-circle" src="img/team/team01.jpg" height="120px" width="120px" alt="">
					<br>
					<h4><b>David Malago</b></h4>
					<a href="#" class="icon icon-twitter"></a>
					<a href="#" class="icon icon-facebook"></a>
					<a href="#" class="icon icon-flickr"></a>
					<p>David combines an expert technical knowledge with a real eye for design. Working with clients from a wide range of industries, he fully understands client objectives when working on a project, large or small.</p>
				</div><!-- col-lg-3 -->
				
				<div class="col-sm-3 centered">
					<img class="img img-circle" src="img/team/team02.jpg" height="120px" width="120px" alt="">
					<br>
					<h4><b>Tim Davies</b></h4>
					<a href="#" class="icon icon-twitter"></a>
					<a href="#" class="icon icon-facebook"></a>
					<a href="#" class="icon icon-flickr"></a>
					<p>Tim is an experienced marcoms practitioner and manages projects from inception to delivery. He understands the synergy between great design and commercial effectiveness which shines through on every project.</p>
				</div><!-- col-lg-3 -->
				
				<div class="col-sm-3 centered">
					<img class="img img-circle" src="img/team/team03.jpg" height="120px" width="120px" alt="">
					<br>
					<h4><b>Michele Lampa</b></h4>
					<a href="#" class="icon icon-twitter"></a>
					<a href="#" class="icon icon-facebook"></a>
					<a href="#" class="icon icon-flickr"></a>
					<p>Be a creative director is a hard task, but Michele loves what she does. Her combination of knowledge and expertise is an important pillar in our agency.</p>
				</div><!-- col-lg-3 -->
				
				<div class="col-sm-3 centered">
					<img class="img img-circle" src="img/team/team04.jpg" height="120px" width="120px" alt="">
					<br>
					<h4><b>Jaye Smith</b></h4>
					<a href="#" class="icon icon-twitter"></a>
					<a href="#" class="icon icon-facebook"></a>
					<a href="#" class="icon icon-flickr"></a>
					<p>Jaye began making websites when animated logos and scrolling text were cool, but has since found a love for simplicity, creating websites that are a pleasure to browse. Monkey Island Fan.</p>
				</div><!-- col-lg-3 -->
				
			</div><!-- row -->
		</div><!-- container -->

		<!-- ==== GREYWRAP ==== -->
		<div id="greywrap">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 centered">
						<img class="img-responsive" src="img/macbook.png" align="">
					</div>
					<div class="col-lg-4">
						<h2>We Are Hiring!</h2>
						<p>Do you want to be one of use? Sure you want, because we are an awesome team!. Here we work hard every day to craft pixel perfect sites.</p>
						<p><a href="management/login.php" class="btn btn-success">Contact Us</a></p>
					</div>					
				</div><!-- row -->
			</div>
			<br>
			<br>
		</div><!-- greywrap -->
		
		<!-- ==== SECTION DIVIDER3 -->
		<section class="section-divider textdivider divider3">
			<div class="container">
				<h1>DESIGN SOLVE PROBLEMS</h1>
				<hr>
				<p>From the purely practical to the richly philosophical, design is the solution to a host of challenges.</p>
			</div><!-- container -->
		</section><!-- section -->
		
		<!-- ==== PORTFOLIO ==== -->
		<div class="container" id="portfolio" name="portfolio">
		<br>
			<div class="row">
				<br>
				<h1 class="centered">OUR PARTNERS INCLUDE</h1>
				<hr>
				<br>
				<br>
			</div><!-- /row -->
			<div class="container">
			<div class="row">	
			
				<!-- PORTFOLIO IMAGE 1 -->
				<div class="col-md-4 ">
			    	<div class="grid mask">
						<figure>
							<img class="img-responsive" src="img/portfolio/folio01.jpg" alt="">
							<figcaption>
								<h5>DASHBOARD</h5>
								<a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Take a Look</a>
							</figcaption><!-- /figcaption -->
						</figure><!-- /figure -->
			    	</div><!-- /grid-mask -->
				</div><!-- /col -->
				
				
						 <!-- MODAL SHOW THE PORTFOLIO IMAGE. In this demo, all links point to this modal. You should create
						      a modal for each of your projects. -->
						      
						  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						    <div class="modal-dialog">
						      <div class="modal-content">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						          <h4 class="modal-title">Project Title</h4>
						        </div>
						        <div class="modal-body">
						          <p><img class="img-responsive" src="img/portfolio/folio01.jpg" alt=""></p>
						          <p>This project was crafted for Some Name corp. Detail here a little about your job requirements and the tools used. Tell about the challenges faced and what you and your team did to solve it.</p>
						          <p><b><a href="#">Visit Site</a></b></p>
						        </div>
						        <div class="modal-footer">
						          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						        </div>
						      </div><!-- /.modal-content -->
						    </div><!-- /.modal-dialog -->
						  </div><!-- /.modal -->
				
				
				<!-- PORTFOLIO IMAGE 2 -->
				<div class="col-md-4">
			    	<div class="grid mask">
						<figure>
							<img class="img-responsive" src="img/portfolio/folio02.jpg" alt="">
							<figcaption>
								<h5>UI DESIGN</h5>
								<a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Take a Look</a>
							</figcaption><!-- /figcaption -->
						</figure><!-- /figure -->
			    	</div><!-- /grid-mask -->
				</div><!-- /col -->
				
				<!-- PORTFOLIO IMAGE 3 -->
				<div class="col-md-4">
			    	<div class="grid mask">
						<figure>
							<img class="img-responsive" src="img/portfolio/folio03.jpg" alt="">
							<figcaption>
								<h5>ANDROID PAGE</h5>
								<a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Take a Look</a>
							</figcaption><!-- /figcaption -->
						</figure><!-- /figure -->
			    	</div><!-- /grid-mask -->
				</div><!-- /col -->
			</div><!-- /row -->

				<!-- PORTFOLIO IMAGE 4 -->
			<div class="row">	
				<div class="col-md-4 ">
			    	<div class="grid mask">
						<figure>
							<img class="img-responsive" src="img/portfolio/folio04.jpg" alt="">
							<figcaption>
								<h5>PROFILE</h5>
								<a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Take a Look</a>
							</figcaption><!-- /figcaption -->
						</figure><!-- /figure -->
			    	</div><!-- /grid-mask -->
				</div><!-- /col -->
				
				<!-- PORTFOLIO IMAGE 5 -->
				<div class="col-md-4">
			    	<div class="grid mask">
						<figure>
							<img class="img-responsive" src="img/portfolio/folio05.jpg" alt="">
							<figcaption>
								<h5>TWITTER STATUS</h5>
								<a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Take a Look</a>
							</figcaption><!-- /figcaption -->
						</figure><!-- /figure -->
			    	</div><!-- /grid-mask -->
				</div><!-- /col -->
				
				<!-- PORTFOLIO IMAGE 6 -->
				<div class="col-md-4">
			    	<div class="grid mask">
						<figure>
							<img class="img-responsive" src="img/portfolio/folio06.jpg" alt="">
							<figcaption>
								<h5>PHONE MOCKUP</h5>
								<a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Take a Look</a>
							</figcaption><!-- /figcaption -->
						</figure><!-- /figure -->
			    	</div><!-- /grid-mask -->
				</div><!-- /col -->
			</div><!-- /row -->
			<br>
			<br>
		</div><!-- /row -->
	</div><!-- /container -->

		<!-- ==== SECTION DIVIDER4 ==== -->
		<section class="section-divider textdivider divider4">
			<div class="container">
				<h1>DESIGN CREATES EMOTIONAL CONNECTION</h1>
				<hr>
				<p>There’s more to design than meets the eye. It’s when it meets the heart that design creates a meaningful, lasting connection with the audience.</p>
			</div><!-- container -->
		</section><!-- section -->
		
		<!-- ==== BLOG ==== -->
		<div class="container" id="blog" name="blog">
		<br>
			<div class="row">
				<br>
				<h1 class="centered">WE ARE STORYTELLERS</h1>
				<hr>
				<br>
				<br>
			</div><!-- /row -->
			
			<div class="row">
				<div class="col-lg-6 blog-bg">
					<div class="col-lg-4 centered">
					<br>
						<p><img class="img img-circle" src="img/team/team04.jpg" width="60px" height="60px"></p>
						<h4>Jaye Smith</h4>
						<h5>Published Aug 30.</h5>
					</div>
					<div class="col-lg-8 blog-content">
						<h2>We Define Success</h2>
						<p>Armed with insight, we embark on designing the right brand experience that engages the audience. It encompasses both the strategic direction and creative execution that solves a business problem and brings the brand to life.</p>
						<p>In the create phase, the big idea is unleashed to the world through different media touchpoints. This is when we watch the audience fall in love all over again with our client’s brand.</p>
						<p><a href="#" class="icon icon-link"> Read More</a></p>
						<br>
					</div>
				</div><!-- /col -->
				
				<div class="col-lg-6 blog-bg">
					<div class="col-lg-4 centered">
					<br>
						<p><img class="img img-circle" src="img/team/team03.jpg" width="60px" height="60px"></p>
						<h4>Michele Lampa</h4>
						<h5>Published Aug 28.</h5>
					</div>
					<div class="col-lg-8 blog-content">
						<h2>A Beautiful Story</h2>
						<p>Armed with insight, we embark on designing the right brand experience that engages the audience. It encompasses both the strategic direction and creative execution that solves a business problem and brings the brand to life.</p>
						<p>In the create phase, the big idea is unleashed to the world through different media touchpoints. This is when we watch the audience fall in love all over again with our client’s brand.</p>
						<p><a href="#" class="icon icon-link"> Read More</a></p>
						<br>
					</div>
				</div><!-- /col -->
			</div><!-- /row -->
			<br>
			<br>
		</div><!-- /container -->

		
		<!-- ==== SECTION DIVIDER6 ==== -->
		<section class="section-divider textdivider divider6">
			<div class="container">
				<h1>CRAFTED IN NEW YORK, USA.</h1>
				<hr>
				<p>Some Address 987,</p>
				<p>+34 9884 4893</p>
				<p><a class="icon icon-twitter" href="#"></a> | <a class="icon icon-facebook" href="#"></a></p>
			</div><!-- container -->
		</section><!-- section -->
		
		<div class="container" id="contact" name="contact">
			<div class="row">
			<br>
				<h1 class="centered">THANKS FOR VISITING US</h1>
				<hr>
				<br>
				<br>
                <div class="row" >
				<div class="col-sm-4">
                <div class="well">
					<h3>Nairobi Contact Information</h3>
					<p><span class="icon icon-home"></span>2nd Floor, Chepkorio Road opposite CMC Engineering Division, Industrial Area<br/>
						<span class="icon icon-phone"></span> +254-020-550429 <br/>
						<span class="icon icon-mobile"></span> +2547-128-88321 <br/>
						<span class="icon icon-envelop"></span> <a href="#"> insight.nairobi@insightmanagement.co.ke</a> <br/>
						<span class="icon icon-twitter"></span> <a href="#"> @insightmanagement </a> <br/>
						<span class="icon icon-facebook"></span> <a href="#"> Insight Management </a> <br/>
					</p>
				</div><!-- col -->
                </div>
                
				<div class="col-sm-4">
                <div class="well">
					<h3>Mombasa Contact Information</h3>
					<p><span class="icon icon-home"></span>Insight Management Center on Meru Road, Mombasa<br/>
						<span class="icon icon-phone"></span> +254-041-2319826 <br/>
						<span class="icon icon-envelop"></span> <a href="#"> insight.mombasa@insightmanagement.co.ke</a> <br/>
						<span class="icon icon-twitter"></span> <a href="#"> @insightmanagement </a> <br/>
						<span class="icon icon-facebook"></span> <a href="#"> Insight Management </a> <br/>
					</p>
				</div><!-- col -->
                </div>
                
				<div class="col-sm-4">
                <div class="well">
					<h3>Kisumu Contact Information</h3>
					<p><span class="icon icon-home"></span>1st Floor door number 16, Kibuye Jua Kali Association Building on Kakamega road. <br/>
						<span class="icon icon-phone"></span> +254-020-650017/8 <br/>
						<span class="icon icon-envelop"></span> <a href="#"> insight.nairobi@insightmanagement.co.ke</a> <br/>
						<span class="icon icon-twitter"></span> <a href="#"> @insightmanagement </a> <br/>
						<span class="icon icon-facebook"></span> <a href="#"> Insight Management </a> <br/>
					</p>
				</div><!-- col -->
                </div>
                    
                </div>
				<p>
				<div class="col-lg-4">
					<h3>Newsletter</h3>
					<p>Register to our newsletter and be updated with the latests information regarding our services, offers and much more.</p>
					
						<form class="form-horizontal" role="form">
						  <div class="form-group">
						    <label for="inputEmail1" class="col-lg-4 control-label"></label>
						    <div class="col-lg-10">
						      <input type="email" class="form-control" id="inputEmail1" placeholder="Email">
						    </div>
						  </div>
						  <div class="form-group">
						    <label for="text1" class="col-lg-4 control-label"></label>
						    <div class="col-lg-10">
						      <input type="text" class="form-control" id="text1" placeholder="Your Name">
						    </div>
						  </div>
						  <div class="form-group">
						    <div class="col-lg-10">
						      <button type="submit" class="btn btn-success">Sign in</button>
						    </div>
						  </div>
					   </form><!-- form -->
					</p>
				</div><!-- col -->
		
			</div><!-- row -->
		
		</div><!-- container -->

		<div id="footerwrap">
			<div class="container">
				<h4>Created by <a href="#">Clement Wekesa</a> - Copyright 2016</h4>
			</div>
		</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
		

	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/retina.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/smoothscroll.js"></script>
	<script type="text/javascript" src="js/jquery-func.js"></script>
  </body>
</html>
