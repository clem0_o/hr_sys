<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Insight Management</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../../css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../../css/font-awesome.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../../css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head><!-- Header -->
<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html"> Admin </a> 
			</div>
			<div style="color: white;
					padding: 15px 50px 5px 50px;
					float: right;
					font-size: 16px;"> Welcome Again &nbsp; <a href="../logout.php" class="btn btn-success square-btn-adjust">Logout</a> </div>
		</nav> 
           <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
			<div class="sidebar-collapse">
			    <ul class="nav" id="main-menu">
				<li class="text-center">
					<img src="../../img/users/find_user.png" class="user-image img-responsive"/>
				</li>
					
						
				<li>
					<a  href="index.php"><i class=""></i> Dashboard</a>
				</li>
				<li>
					<a  href="profile.php"><i class=""></i> Profile </a>
				</li>
				<li>
					<a  href="admin_payments.php"><i class=""></i> Payments </a>
				</li>
				<li  >
					<a  href="cv.php"><i class=""></i> Cvs </a>
				</li>	
				<li  >
					<a  href="active_jobs.php"><i class=""></i> Active Jobs </a>
				</li>
				<li  >
					<a  href="all_applications.php"><i class=""></i> All Applications </a>
				</li>			
					
			    </ul>
			   
			</div>
            
		</nav> 
        <!-- /. NAV SIDE  -->