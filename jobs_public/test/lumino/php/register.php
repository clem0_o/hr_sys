<?php require_once("../../../includes/initialize.php"); ?>
<?php include_layout_template('header_forms.php'); ?>
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
			<div class="login-panel panel panel-default">
				<div class="panel-heading"> Register </div>
				<div class="panel-body">
					<form action="" role="form">
						<fieldset>
							<div class="row">
								<div class="control-label">
									<label for="" class="control-label">General Infomation: </label>
								</div>
								<div class="col-xs-6">
									<label class="sr-only" for="first_name">First Name: </label>
									<input type="text" name="first_name" class="form-control" id="first_name" placeholder="first name" value="<?php echo isset($fname) ? $fname: false; ?>"  title="Please enter your first name." required>
									<span class="error"><?php echo isset($fname_error) ? $fname_error : false; ?></span>
								</div>
								<div class="col-xs-6">
									<label class="sr-only" for="last_name">Last Name: </label>
									<input type="text" name="last_name" class="form-control" id="last_name" placeholder="last name" value="<?php echo isset($lname) ? $lname : false; ?>" title="Please enter your last name."  required>
									<span class="error"><?php echo isset($lname_error) ? $lname_error : false; ?></span>
								</div>
							</div><!-- End of general information-->
							  
							<div class="row">
								<div class="control-label">
									<label for="" class="control-label">Login Infomation: </label>
								</div>
									<div class="col-sm-4">
										<label class="sr-only" for="username">UserName: </label>
										<input type="text" name="username" class="form-control" id="username" placeholder="username..." value="<?php echo isset($uname) ? $uname: false; ?>" title="Please enter your user name."  required>
										<span class="error"><?php echo isset($uname_error) ? $uname_error : false; ?></span>
									</div>
									<div class="col-sm-4">
										<label for="password" class="sr-only">Password: </label>
										<input type="password" name="password" class="form-control" id="password" placeholder="Password..." title="Please enter your password."  required>
										<span class="error"><?php echo isset($pword_error) ? $pword_error : false; ?></span>
									</div>
									<div class="col-sm-4">
										<label for="password" class="sr-only">Password again: </label>
										<input type="password" name="password_again" class="form-control" id="password_again" placeholder="Password again..." title="Please enter your password again."  required>
										<span class="error"><?php echo isset($pword_error) ? $pword_error : false; ?></span>
									</div>
								</div><!--End of login information -->
							  
								<div class="row">
									<div class="control-label">
										<label for="" class="control-label">Contact Infomation: </label>
									</div>
									<div class="input-group">
										<div class="col-sm-4">
											<div class="input-group">
												<label for="phone_number" class="sr-only">Phone Number: </label>
												<span class="input-group-addon">
													<select name="country_code">
													  <option value="+254">country</option>
													  <option value="+254">Kenya</option>
													  <option value="+256">Uganda</option>
													  <option value="+255">Tanzania</option>
													</select>
												</span>
												<input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="phone_number..." value="<?php echo isset($pnumber) ? $pnumber: false; ?>" min="9" maxlength="13" title="Please enter your phone number."  required>
												<span class="error"><?php echo isset($pnumber_error) ? $pnumber_error : false; ?><?php echo isset($pnumber_error2) ? $pnumber_error2 : false; ?></span>
											</div>
										</div>
										<div class="col-sm-4">
											<label class="sr-only" for="email">Email: </label>
											<input type="text" name="email" class="form-control" id="email" placeholder="email..." value="<?php echo isset($email) ? $email : false; ?>" title="Please enter your email."  required>
											<span class="error"><?php echo isset($email_error) ? $email_error : false; ?></span>
										</div>
										<div class="col-xs-4">
											<label class="sr-only" for="region">Region: </label>
											<input type="text" name="region" class="form-control" id="region" placeholder="region..." value="<?php echo isset($uregion) ? $uregion: false; ?>" title="Please enter your region."  required>
											<span class="error"><?php echo isset($uregion_error) ? $uregion_error : false; ?></span>
										</div>
									</div>
								</div><!-- End of contact infprmation-->
							   
								<div class="row">
									<label for="" class="control-label">Looking for: </label>
									<p>
										<div class="col-xs-4">
											<label class="sr-only" for="type">Looking for: </label>
											<select name="type" class="form-control centred" id="type" required>
												<option value="Employee">Job</option>
												<!--<option value="Employer">Employee</option>-->
											</select>
											<span class="error"><?php echo isset($type_error) ? $uregion_error : false; ?></span>
										</div>
									</p>
								</div>
								<br>
								<button type="submit" name="submit" class="btn btn-primary">Sign Up!</button>
								<p>If you already have an account <a class="btn btn-primary" href="login.php"> Login </a>.</p>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
		

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
