<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insight Management</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Insight</span>Management</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<!--<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>-->
		<ul class="nav menu">
			<li class="active"><a href="eprofile.php"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Profile </a></li>
			<li><a href="eapplied_jobs.php"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Applied Jobs</a></li>
			<li><a href="eavailable_jobs.php"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Available Jobs</a></li>
			
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row -Bread crumbs-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Profile</h1>
			</div>
		</div><!--/.row -Page header-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						Personal Details
					</div>
					<div class="panel-body">
						<p><label>Name: <?php?></label></p>
						<p><label>Phone No: </label></p>
						<p><label>Email: </label></p>
						<p><label>Residence: </label></p>
						<p><label>Account Status: </label></p>
					</div>
				</div>
			</div>
			<div class="clo-lg-2 col-sm-12">
				<!--The Ad goes here-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-8 col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body tabs">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">Working History</a></li>
							<li><a href="#tab2" data-toggle="tab">Education Background</a></li>
							<li><a href="#tab3" data-toggle="tab">Other Qualifications</a></li>
							<li><a href="#tab4" data-toggle="tab">Referees</a></li>
						</ul>
		
						<div class="tab-content">
							<div class="tab-pane fade in active" id="tab1">
								<h4>Working History</h4>
								<div class="table-responsive">
									<?php if (!empty($work_history)) { ?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>No</th>
												<th>Organisation Name</th>
												<th>Position Held</th>
												<th>Jobs Description</th>
												<th>Time At work</th>
												<th>Edit</th>
											</tr>
										</thead>
										<tbody>
                                  <?php 
                                  foreach ($work_history as $work) :
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td>".$work->organization_name."</td>";
                                      $output .= "<td>".$work->position_held."</td>";
                                      $output .= "<td>".$work->job_description."</td>";
                                      $output .= "<td>".$work->start_date." - ".$jobs_history->last_date()."</td>";
                                      $output .= "<td><a href=\"edit_work_profile.php?work_id=".$work->id."\" class=\"btn btn-link\">Edit</a></td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    endforeach;
                                    } else {
                                      echo "You have no entry yet";
                                      } ?>
                                    </tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="tab2">
								<h4>Education Background</h4>
								<div class="table-responsive">
									<?php if (!empty($education_history)) { ?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Institution Name</th>
												<th>Course Taken</th>
												<th>Level</th>
												<th>Major</th>
												<th>Time At the institution</th>
											</tr>
										</thead>
										<tbody>
                                  <?php foreach ($education_history as $education) {
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td>".$education->institution."</td>";
                                      $output .= "<td>".$education->course_taken."</td>";
                                      $output .= "<td>".$education->level."</td>";
                                      $output .= "<td>".$education->major."</td>";
                                      $output .= "<td>".$education->start_date." - ".$education_history_obj->last_date()."</td>";
                                      $output .= "<td><a href=\"edit_education_profile.php?edu_id=".$education->id."\" class=\"btn btn-link\">Edit</a></td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    }
                                    } else {
                                      echo "You have no entry yet";
                                      } ?>
                                    </tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="tab3">
								<h4>Other Qualifications</h4>
								<div class="table-responsive">
									<?php if (!empty($other_qualifications)) { ?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Description</th>
												<th>Type</th>
												<th>Where Attained</th>
											</tr>
										</thead>
										<tbody>
                                  <?php foreach ($other_qualifications as $qualification) {
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td>".$qualification->name."</td>";
                                      $output .= "<td>".$qualification->description."</td>";
                                      $output .= "<td>".$qualification->type."</td>";
                                      $output .= "<td>".$qualification->where_attained."</td>";
                                      $output .= "<td><a href=\"edit_otherq_profile.php?otherq_id=".$qualification->id."\" class=\"btn btn-link\">Edit</a></td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    }
                                    } else {
                                      echo "You have no entry yet";
                                      } ?>
                                    </tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="tab4">
								<h4>Referees</h4>
								<div class="table-responsive">
									<?php if (!empty($referees)) { ?>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th>#</th>
												<th>Name</th>
												<th>Title</th>
												<th>Phone No</th>
												<th>Email</th>
											</tr>
										</thead>
										<tbody>
                                  <?php foreach ($referees as $qualification) {
                                    $output = "<tr>";
                                      $output .= "<td>1.</td>";
                                      $output .= "<td>".$qualification->name."</td>";
                                      $output .= "<td>".$qualification->description."</td>";
                                      $output .= "<td>".$qualification->type."</td>";
                                      $output .= "<td>".$qualification->where_attained."</td>";
                                      $output .= "<td><a href=\"edit_otherq_profile.php?otherq_id=".$qualification->id."\" class=\"btn btn-link\">Edit</a></td>";
                                    $output .= "</tr>";
                                    echo $output;
                                    }
                                    } else {
                                      echo "You have no entry yet";
                                      } ?>
                                    </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div><!--/.panel-->
			</div>
		</div><!--/.row-->
		
		<div class="row">
			
		</div><!--/.row-->
								
		<div class="row">
			
		</div><!--/.row-->
	</div>	<!--/.main-->

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
